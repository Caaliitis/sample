<?php


namespace App\Model;

use Symfony\Component\Serializer\Annotation\Groups;

final class CountGraph
{
    /**
     * @Groups({"get-product", "get-company", "get-contact", "get-component"})
     */
    private $monthNr;

    /**
     * @Groups({"get-product", "get-company", "get-contact", "get-component"})
     */
    private $month;

    /**
     * @Groups({"get-product", "get-company", "get-contact", "get-component"})
     */
    private $year;

    /**
     * @Groups({"get-product", "get-company", "get-contact", "get-component"})
     */
    private $count;

    public function getMonthNr(): string
    {
        return $this->monthNr;
    }

    public function getMonth(): string
    {
        return $this->month;
    }

    public function getYear(): string
    {
        return $this->year;
    }

    public function getCount(): int
    {
        return $this->count;
    }


}