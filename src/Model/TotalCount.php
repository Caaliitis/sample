<?php


namespace App\Model;

use Symfony\Component\Serializer\Annotation\Groups;

final class TotalCount
{
    /**
     * @Groups({"get-product"})
     */
    private $total;

    public function getTotal(): int
    {
        return $this->total;
    }

}