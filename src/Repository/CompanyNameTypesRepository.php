<?php

namespace App\Repository;

use App\Entity\CompanyNameTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CompanyNameTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyNameTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyNameTypes[]    findAll()
 * @method CompanyNameTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyNameTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyNameTypes::class);
    }
}
