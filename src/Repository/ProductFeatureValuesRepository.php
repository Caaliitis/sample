<?php

namespace App\Repository;

use App\Entity\ProductFeatureValues;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductFeatureValues|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductFeatureValues|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductFeatureValues[]    findAll()
 * @method ProductFeatureValues[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductFeatureValuesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductFeatureValues::class);
    }
}
