<?php

namespace App\Repository;

use App\Entity\ComponentCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentCategory[]    findAll()
 * @method ComponentCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComponentCategory::class);
    }

}
