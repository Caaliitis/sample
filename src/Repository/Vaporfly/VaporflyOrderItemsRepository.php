<?php

namespace App\Repository\Vaporfly;

use App\Entity\Vaporfly\VaporflyOrderItems;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VaporflyOrderItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method VaporflyOrderItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method VaporflyOrderItems[]    findAll()
 * @method VaporflyOrderItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VaporflyOrderItemsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VaporflyOrderItems::class);
    }

}
