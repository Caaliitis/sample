<?php

namespace App\Repository;

use App\Entity\PatentDescription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentDescription|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentDescription|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentDescription[]    findAll()
 * @method PatentDescription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentDescriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentDescription::class);
    }
}
