<?php

namespace App\Repository;

use App\Entity\SampleFiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SampleFiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method SampleFiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method SampleFiles[]    findAll()
 * @method SampleFiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SampleFilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SampleFiles::class);
    }

}
