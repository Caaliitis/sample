<?php

namespace App\Repository;

use App\Entity\PatentAbstract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentAbstract|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentAbstract|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentAbstract[]    findAll()
 * @method PatentAbstract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentAbstractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentAbstract::class);
    }

}
