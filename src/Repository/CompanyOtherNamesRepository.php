<?php

namespace App\Repository;

use App\Entity\CompanyOtherNames;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CompanyOtherNames|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyOtherNames|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyOtherNames[]    findAll()
 * @method CompanyOtherNames[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyOtherNamesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyOtherNames::class);
    }

}
