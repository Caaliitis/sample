<?php

namespace App\Repository;

use App\Entity\PatentContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PatentContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentContent[]    findAll()
 * @method PatentContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentContent::class);
    }

}
