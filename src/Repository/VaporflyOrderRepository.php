<?php

namespace App\Repository;

use App\Entity\Vaporfly\VaporflyOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method VaporflyOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method VaporflyOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method VaporflyOrder[]    findAll()
 * @method VaporflyOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VaporflyOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VaporflyOrder::class);
    }
}
