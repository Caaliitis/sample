<?php

namespace App\Repository;

use App\Entity\ProductFiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductFiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductFiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductFiles[]    findAll()
 * @method ProductFiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductFilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductFiles::class);
    }

    /**
     * @param $product_id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll($product_id)
	{
		return $this->createQueryBuilder('zwpf')
			->select('count(zwpf.id)')
			->andWhere('zwpf.product = :product_id')->setParameter('product_id', $product_id)
			->andWhere('zwpf.state = 1')
			->getQuery()
			->getSingleScalarResult();
	}

    /**
     * @param int $orderColumn
     * @param string $orderDir
     * @param $product_id
     * @return \Doctrine\ORM\QueryBuilder
     */
	public function findAllQueryBuilder($orderColumn = 0, $orderDir = 'ASC', $product_id)
	{
		$qb = $this->createQueryBuilder('zwpf');

		$qb->andWhere('zwpf.product = :product_id')->setParameter('product_id', $product_id);
		$qb->andWhere('zwpf.state = 1');

		$qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

		return $qb;
	}

    /**
     * @param $orderColumn
     * @return mixed
     */
	protected function getColumnsOrder($orderColumn)
	{
		$array = array(0 => 'zwpf.id', 1 => 'zwpf.file');

		return $array[$orderColumn];
	}
}
