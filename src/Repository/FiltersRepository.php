<?php

namespace App\Repository;

use App\Entity\Users;
use App\Entity\Filters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Filters|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filters|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filters[]    findAll()
 * @method Filters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FiltersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Filters::class);
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
	public function findCountAll()
	{
		return $this->createQueryBuilder('filters')
			->select('count(filters.id)')
			->getQuery()
			->getSingleScalarResult()
			;
	}

    /**
     * @param bool $filter
     * @param int $orderColumn
     * @param string $orderDir
     * @param Users $user
     * @return QueryBuilder
     */
	public function findAllQueryBuilder($filter = false, $orderColumn = 0, $orderDir = 'ASC', Users $user){
		$qb = $this->createQueryBuilder('filters');

		if($filter){
			$qb->andWhere('LOWER(filters.name) LIKE :filter')->setParameter('filter', '%'.strtolower($filter).'%');
		}
		
		$qb->andWhere('filters.created = :user')->setParameter('user', $user);

		$qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

		return $qb;
	}

    /**
     * @param $orderColumn
     * @return mixed
     */
	protected function getColumnsOrder($orderColumn)
	{
		$array = array(0 => 'filters.id', 1 => 'filters.name');

		return $array[$orderColumn];
	}
}
