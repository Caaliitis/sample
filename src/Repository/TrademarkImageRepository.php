<?php

namespace App\Repository;

use App\Entity\TrademarkImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TrademarkImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrademarkImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrademarkImage[]    findAll()
 * @method TrademarkImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrademarkImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrademarkImage::class);
    }

}
