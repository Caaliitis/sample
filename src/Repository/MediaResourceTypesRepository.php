<?php

namespace App\Repository;

use App\Entity\MediaResourceTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MediaResourceTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method MediaResourceTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method MediaResourceTypes[]    findAll()
 * @method MediaResourceTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MediaResourceTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MediaResourceTypes::class);
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll()
    {
        return $this->createQueryBuilder('m')
            ->select('count(m.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllQueryBuilder()
    {
        $qb = $this->createQueryBuilder('m');
        return $qb;
    }
}
