<?php

namespace App\Repository;

use App\Entity\Types;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Types|null find($id, $lockMode = null, $lockVersion = null)
 * @method Types|null findOneBy(array $criteria, array $orderBy = null)
 * @method Types[]    findAll()
 * @method Types[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypesRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Types::class);
    }


    /**
     * @param string $title
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function searchByLowercaseTitle(string $title): ?Types
    {
        $qb = $this->createQueryBuilder('t');
        return $qb
            ->where($qb->expr()->like("LOWER(t.name)", "?1"))
            ->setParameter("1","?".strtolower($title)."%")
            ->getQuery()
            ->getSingleResult();
    }


    public function findAllInArray()
    {
        return $this->createQueryBuilder('t')
            ->select('t.id, t.name')
            ->getQuery()
            ->getArrayResult();
    }

}
