<?php

namespace App\Repository;

use App\Entity\Users;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use http\Client\Curl\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Users::class);
    }

    /**
     * @param UserInterface $user
     * @param string $newEncodedPassword
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof Users) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * Create a new user
     *
     * @param $data
     * @return Users
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createNewUser($data): Users
    {
        $user = new Users();
        $user->setEmail($data['email']);
        $user->setName($data['email']);
        $user->setUsername($data['email']);
        $user->setRegisterdate(new DateTime());
        $user->setNewPassword($data['password']);

        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    public function collectUsersForEmailSubscription()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.uiSettings IS NOT NULL')
            ->andWhere('u.block = false')
            ->getQuery()
            ->execute();
    }
}
