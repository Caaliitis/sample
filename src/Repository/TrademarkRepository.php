<?php

namespace App\Repository;

use App\Entity\Trademark;
use App\Traits\UbersearchTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Trademark|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trademark|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trademark[]    findAll()
 * @method Trademark[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrademarkRepository extends ServiceEntityRepository
{

	use UbersearchTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trademark::class);
    }
}
