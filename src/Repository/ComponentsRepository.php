<?php

namespace App\Repository;

use App\Entity\Components;
use App\Entity\Products;
use App\Traits\CountPerMonthTrait;
use App\Traits\UpdatedIdsAfterDateTime;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Components|null find($id, $lockMode = null, $lockVersion = null)
 * @method Components|null findOneBy(array $criteria, array $orderBy = null)
 * @method Components[]    findAll()
 * @method Components[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentsRepository extends ServiceEntityRepository
{
    use CountPerMonthTrait, UpdatedIdsAfterDateTime;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Components::class);
    }

    /**
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findCountAll()
    {
        return $this->createQueryBuilder('components')
            ->select('count(components.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int $orderColumn
     * @param string $orderDir
     *
     * @return QueryBuilder
     */
    public function findAllQueryBuilder($orderColumn = 0, $orderDir = 'ASC')
    {
        $qb = $this->createQueryBuilder('components');

        $qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

        return $qb;
    }

    /**
     * @param $orderColumn
     *
     * @return mixed
     */
    protected function getColumnsOrder($orderColumn)
    {
        $array = ['components.id', 'components.name'];

        return $array[$orderColumn];
    }


    public function findAllDuplicates(Components $component)
    {
        $parameters = array(
            'name' => $component->getName(),
            'marking' => $component->getMarking(),
            'description' => $component->getDescription(),
            'categoryId' => $component->getCategoryId(),
            'id' => $component->getId(),
        );

        return $this->createQueryBuilder('c')
            ->andWhere('LOWER(c.name)=LOWER(:name) AND LOWER(c.marking)=LOWER(:marking) AND LOWER(c.description)=LOWER(:description) AND c.category_id=:categoryId AND c.id !=:id')
            ->setParameters($parameters)
            ->getQuery()
            ->getResult();
    }


    public function totalCount(): ?int
    {
        try {
            return $this->createQueryBuilder('c')->select('count(c.id)')->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    /**
     * @param $sql
     * @return mixed[]
     * @throws DBALException
     */
    public function executePlainSQL($sql)
    {
        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

}
