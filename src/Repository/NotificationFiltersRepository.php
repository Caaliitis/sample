<?php

namespace App\Repository;

use App\Entity\NotificationFilters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NotificationFilters|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotificationFilters|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotificationFilters[]    findAll()
 * @method NotificationFilters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationFiltersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotificationFilters::class);
    }

    // /**
    //  * @return NotificationFilters[] Returns an array of NotificationFilters objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotificationFilters
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
