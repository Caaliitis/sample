<?php

namespace App\Repository;

use App\Entity\FileGroups;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FileGroups|null find($id, $lockMode = null, $lockVersion = null)
 * @method FileGroups|null findOneBy(array $criteria, array $orderBy = null)
 * @method FileGroups[]    findAll()
 * @method FileGroups[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileGroupsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FileGroups::class);
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll()
	{
		return $this->createQueryBuilder('file_groups')
			->select('count(file_groups.id)')
			->getQuery()
			->getSingleScalarResult()
			;
	}

    /**
     * @param bool $filter
     * @param int $orderColumn
     * @param string $orderDir
     * @param null $module
     * @return \Doctrine\ORM\QueryBuilder
     */
	public function findAllQueryBuilder($filter = false, $orderColumn = 0, $orderDir = 'ASC', $module = null){
		$qb = $this->createQueryBuilder('file_groups');

		if($filter){
			$qb->andWhere('LOWER(file_groups.name) LIKE :filter')->setParameter('filter', '%'.strtolower($filter).'%');
		}

		if(null !== $module){
			$qb->innerJoin('file_groups.modules', 'm');
			$qb->andWhere('m.id = :module')->setParameter('module', $module->getId());
		}

		$qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

		return $qb;
	}

    /**
     * @param $orderColumn
     * @return mixed
     */
	protected function getColumnsOrder($orderColumn)
	{
		$array = array(0 => 'file_groups.id', 1 => 'file_groups.name');

		return $array[$orderColumn];
	}
}
