<?php

namespace App\Repository;

use App\Entity\PatentAssignee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PatentAssignee|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentAssignee|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentAssignee[]    findAll()
 * @method PatentAssignee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentAssigneeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentAssignee::class);
    }
}
