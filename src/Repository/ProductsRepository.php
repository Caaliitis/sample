<?php

namespace App\Repository;

use App\Entity\Products;
use App\Traits\CountPerMonthTrait;
use App\Traits\UbersearchTrait;
use App\Traits\UpdatedIdsAfterDateTime;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * @method Products|null find($id, $lockMode = null, $lockVersion = null)
 * @method Products|null findOneBy(array $criteria, array $orderBy = null)
 * @method Products[]    findAll()
 * @method Products[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductsRepository extends ServiceEntityRepository
{
    use UbersearchTrait, CountPerMonthTrait, UpdatedIdsAfterDateTime;

    private $vaporflyTypeId;

    public function __construct(ManagerRegistry $registry, int $vaporflyTypeId)
    {
        parent::__construct($registry, Products::class);
        $this->vaporflyTypeId = $vaporflyTypeId;
    }

    /**
     * @param $sql
     * @return mixed[]
     * @throws DBALException
     */
    public function executePlainSQL($sql)
    {
        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function totalCount(bool $draft = false): ?int
    {
        try {
            $qb = $this->createQueryBuilder('p')->select('count(p.id)');

            if (false === $draft) {
                $qb->andWhere('p.draft = false');
            }

            return $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    public function vaporflyTotalCount(bool $draft = false): ?int
    {
        try {
            $qb = $this->createQueryBuilder('p')->select('count(p.id)');
            $qb->innerJoin(sprintf('%s.types', 'p'), "ty");
            $qb->andWhere("ty.id = " . $this->vaporflyTypeId);

            if (false === $draft) {
                $qb->andWhere('p.draft = false');
            }

            return $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }


}
