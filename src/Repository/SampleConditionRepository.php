<?php

namespace App\Repository;

use App\Entity\SampleConditions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SampleConditions|null find($id, $lockMode = null, $lockVersion = null)
 * @method SampleConditions|null findOneBy(array $criteria, array $orderBy = null)
 * @method SampleConditions[]    findAll()
 * @method SampleConditions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SampleConditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SampleConditions::class);
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll()
    {
        return $this->createQueryBuilder('ss')
            ->select('count(ss.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllQueryBuilder()
    {
        $qb = $this->createQueryBuilder('ss');

        $qb->orderBy($this->getColumnsOrder(0), 'ASC');

        return $qb;
    }

    /**
     * @param $orderColumn
     * @return mixed
     */
    protected function getColumnsOrder($orderColumn)
    {
        $array = [0 => 'ss.id', 1 => 'ss.name'];

        return $array[$orderColumn];
    }

}
