<?php

namespace App\Repository;

use App\Entity\ContactMediaResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ContactMediaResource|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactMediaResource|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactMediaResource[]    findAll()
 * @method ContactMediaResource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactMediaResourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactMediaResource::class);
    }
}
