<?php

namespace App\Repository;

use App\Entity\PatentClaim;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentClaim|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentClaim|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentClaim[]    findAll()
 * @method PatentClaim[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentClaimRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentClaim::class);
    }
}
