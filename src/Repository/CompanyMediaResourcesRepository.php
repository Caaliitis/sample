<?php

namespace App\Repository;

use App\Entity\CompanyMediaResources;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CompanyMediaResources|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyMediaResources|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyMediaResources[]    findAll()
 * @method CompanyMediaResources[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyMediaResourcesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyMediaResources::class);
    }

}
