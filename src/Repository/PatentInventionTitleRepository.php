<?php

namespace App\Repository;

use App\Entity\PatentInventionTitle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentInventionTitle|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentInventionTitle|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentInventionTitle[]    findAll()
 * @method PatentInventionTitle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentInventionTitleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentInventionTitle::class);
    }

}
