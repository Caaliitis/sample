<?php

namespace App\Repository;

use App\Entity\PatentMessenger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PatentMessenger|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentMessenger|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentMessenger[]    findAll()
 * @method PatentMessenger[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentMessengerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentMessenger::class);
    }
}
