<?php

namespace App\Repository;

use App\Entity\ComponentPackage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentPackage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentPackage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentPackage[]    findAll()
 * @method ComponentPackage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentPackageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComponentPackage::class);
    }
}
