<?php

namespace App\Repository;

use App\Entity\Companies;
use App\Traits\CountPerMonthTrait;
use App\Traits\UbersearchTrait;
use App\Traits\UpdatedIdsAfterDateTime;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Companies|null find($id, $lockMode = null, $lockVersion = null)
 * @method Companies|null findOneBy(array $criteria, array $orderBy = null)
 * @method Companies[]    findAll()
 * @method Companies[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompaniesRepository extends ServiceEntityRepository
{

	use UbersearchTrait, CountPerMonthTrait, UpdatedIdsAfterDateTime;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Companies::class);
    }

    /**
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findCountAll()
    {
        return $this->createQueryBuilder('companies')
            ->select('count(companies.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllNames()
    {
        return $this->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param $sql
     * @return mixed[]
     * @throws DBALException|Exception
     */
    public function executePlainSQL($sql): array
    {
        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @return int|null
     */
    public function totalCount(): ?int
    {
        try {
            return $this->createQueryBuilder('c')->select('count(c.id)')->getQuery()->getSingleScalarResult();
        } catch (NoResultException | NonUniqueResultException $e) {
            return 0;
        }
    }


    public function findUpdatedIdsAfterDateTime(DateTime $dateTime): array
    {
        return array_column($this->createQueryBuilder('c')
            ->select('c.id')
            ->andWhere('c.updatedAt > :dateTime')
            ->setParameter('dateTime', $dateTime)
            ->getQuery()
            ->getArrayResult() , 'id');
    }


    /**
     * @param string $title
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function searchByLowercaseTitle(string $title): ?Companies
    {
        $qb = $this->createQueryBuilder('c');
        return $qb
            ->where($qb->expr()->like("LOWER(c.name)", "?1"))
            ->setParameter("1","?".strtolower($title)."%")
            ->getQuery()
            ->getSingleResult();
    }
}
