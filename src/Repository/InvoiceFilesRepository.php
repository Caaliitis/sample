<?php

namespace App\Repository;

use App\Entity\InvoiceFiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method InvoiceFiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvoiceFiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvoiceFiles[]    findAll()
 * @method InvoiceFiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceFilesRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, InvoiceFiles::class);
	}

}
