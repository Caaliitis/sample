<?php

namespace App\Repository;

use App\Entity\UploadedReports;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UploadedReports|null find($id, $lockMode = null, $lockVersion = null)
 * @method UploadedReports|null findOneBy(array $criteria, array $orderBy = null)
 * @method UploadedReports[]    findAll()
 * @method UploadedReports[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UploadedReportsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UploadedReports::class);
    }

}
