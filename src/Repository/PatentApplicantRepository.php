<?php

namespace App\Repository;

use App\Entity\PatentApplicant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentApplicant|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentApplicant|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentApplicant[]    findAll()
 * @method PatentApplicant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentApplicantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentApplicant::class);
    }

}
