<?php

namespace App\Repository;

use App\Entity\CountryRegions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CountryRegions|null find($id, $lockMode = null, $lockVersion = null)
 * @method CountryRegions|null findOneBy(array $criteria, array $orderBy = null)
 * @method CountryRegions[]    findAll()
 * @method CountryRegions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountryRegionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CountryRegions::class);
    }

}
