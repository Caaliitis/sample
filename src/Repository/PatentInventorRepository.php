<?php

namespace App\Repository;

use App\Entity\PatentInventor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentInventor|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentInventor|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentInventor[]    findAll()
 * @method PatentInventor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentInventorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentInventor::class);
    }

}
