<?php

namespace App\Repository;

use App\Entity\ComponentsElement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentsElement|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentsElement|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentsElement[]    findAll()
 * @method ComponentsElement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentsElementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComponentsElement::class);
    }

    // /**
    //  * @return ComponentsElement[] Returns an array of ComponentsElement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ComponentsElement
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
