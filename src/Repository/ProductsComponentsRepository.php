<?php

namespace App\Repository;

use App\Entity\ProductsComponents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductsComponents|null find( $id, $lockMode = null, $lockVersion = null )
 * @method ProductsComponents|null findOneBy( array $criteria, array $orderBy = null )
 * @method ProductsComponents[]    findAll()
 * @method ProductsComponents[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class ProductsComponentsRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, ProductsComponents::class );
	}

}
