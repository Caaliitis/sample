<?php

namespace App\Repository;

use App\Entity\PatentApplication;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentApplication|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentApplication|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentApplication[]    findAll()
 * @method PatentApplication[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentApplication::class);
    }

}
