<?php

namespace App\Repository;

use App\Entity\ProofRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProofRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProofRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProofRequest[]    findAll()
 * @method ProofRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProofRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProofRequest::class);
    }

}
