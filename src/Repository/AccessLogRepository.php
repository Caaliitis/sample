<?php

namespace App\Repository;

use App\Entity\AccessLog;
use App\Entity\Users;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccessLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccessLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccessLog[]    findAll()
 * @method AccessLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccessLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessLog::class);
    }

    /**
     * @param Users $user
     * @param int $hours
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getRequestCountByUser(Users $user, int $hours)
    {
        $timeThreshold = (new DateTime())->modify('-' . $hours . ' hours');

        return $this->createQueryBuilder('a')
            ->select('count(a.id)')
            ->andWhere('a.user = :user')
            ->setParameter(':user', $user)
            ->andWhere('a.createdAt >= :createdAt')
            ->setParameter(':createdAt', $timeThreshold)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Users $user
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNotificationCountLastDayByUser(Users $user)
    {
        $timeThreshold = (new DateTime())->modify('-24 hours');

        return $this->createQueryBuilder('a')
            ->select('count(a.id)')
            ->andWhere('a.user = :user')
            ->setParameter(':user', $user)
            ->andWhere('a.createdAt >= :createdAt')
            ->setParameter(':createdAt', $timeThreshold)
            ->andWhere('a.recaptchaNotification = TRUE')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
