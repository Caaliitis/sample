<?php

namespace App\Repository;

use App\Entity\ProductionStatuses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductionStatuses|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductionStatuses|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductionStatuses[]    findAll()
 * @method ProductionStatuses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductionStatusesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductionStatuses::class);
    }


    /**
     * @param string $title
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function searchByLowercaseTitle(string $title): ?ProductionStatuses
    {
        $qb = $this->createQueryBuilder('s');
        return $qb
            ->where($qb->expr()->like("LOWER(s.name)", "?1"))
            ->setParameter("1","?".strtolower($title)."%")
            ->getQuery()
            ->getSingleResult();
    }
}
