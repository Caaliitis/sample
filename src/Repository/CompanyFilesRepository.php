<?php

namespace App\Repository;

use App\Entity\CompanyFiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyFiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyFiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyFiles[]    findAll()
 * @method CompanyFiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyFilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyFiles::class);
    }

    /**
     * @param $company_id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll($company_id)
	{
		return $this->createQueryBuilder('zwcf')
			->select('count(zwcf.id)')
			->andWhere('zwcf.company = :company_id')->setParameter('company_id', $company_id)
			->andWhere('zwcf.state = 1')
			->getQuery()
			->getSingleScalarResult();
	}

    /**
     * @param int $orderColumn
     * @param string $orderDir
     * @param $company_id
     * @return \Doctrine\ORM\QueryBuilder
     */
	public function findAllQueryBuilder($orderColumn = 0, $orderDir = 'ASC', $company_id)
	{
		$qb = $this->createQueryBuilder('zwcf');

		$qb->andWhere('zwcf.company = :company_id')->setParameter('company_id', $company_id);
		$qb->andWhere('zwcf.state = 1');

		$qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

		return $qb;
	}

    /**
     * @param $orderColumn
     * @return mixed
     */
	protected function getColumnsOrder($orderColumn)
	{
		$array = array(0 => 'zwcf.id', 1 => 'zwcf.file');

		return $array[$orderColumn];
	}
}
