<?php

namespace App\Repository;

use App\Entity\PatentDocument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentDocument|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentDocument|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentDocument[]    findAll()
 * @method PatentDocument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentDocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentDocument::class);
    }
}
