<?php

namespace App\Repository;

use App\Entity\Features;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Features|null find($id, $lockMode = null, $lockVersion = null)
 * @method Features|null findOneBy(array $criteria, array $orderBy = null)
 * @method Features[]    findAll()
 * @method Features[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeaturesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Features::class);
    }

    /**
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
	public function findCountAll()
	{
		return $this->createQueryBuilder('features')
			->select('count(features.id)')
			->getQuery()
			->getSingleScalarResult()
			;
	}

    /**
     * @param array $filter
     * @param int $orderColumn
     * @param string $orderDir
     * @return QueryBuilder
     */
	public function findAllQueryBuilder($filter = array(), $orderColumn = 2, $orderDir = 'ASC'): QueryBuilder
    {

    	$qb = $this->createQueryBuilder('features');

		if (array_key_exists('featureName', $filter) && !empty($filter['featureName'])){
			$qb->andWhere('LOWER(features.name) LIKE :filter')->setParameter('filter', '%'.strtolower($filter['featureName']).'%');
		}


		if(array_key_exists('selectByType', $filter) && !empty($filter['selectByType'])){
			$qb->innerJoin('features.types', 't');
			$qb->andWhere('t.id = :type_id')->setParameter('type_id', $filter['typeId']);
		}

		$qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

		return $qb;
	}

    /**
     * @param $productId
     * @return QueryBuilder
     */
	public function findAllQueryBuilderFeaturesByProduct($productId): QueryBuilder
    {
		$qb = $this->createQueryBuilder('features');

		$qb->innerJoin('features.types', 't');
		$qb->innerJoin('t.products', 'p');
		$qb->andWhere('p.id = :productId')->setParameter('productId', $productId);

		$qb->leftJoin('features.options', 'o');
		$qb->leftJoin('features.productFeatureValue', 'v');

		$qb->orderBy('features.ordering', 'ASC');

		return $qb;
	}

    /**
     * @param $orderColumn
     * @return mixed
     */
	protected function getColumnsOrder($orderColumn): string
    {
		$array = array(0 => 'features.id', 1 => 'features.name', 2 => 'features.ordering');

		return $array[$orderColumn];
	}

    /**
     * @param string $title
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function searchByLowercaseTitle(string $title): ?Features
    {
        $qb = $this->createQueryBuilder('b');
        return $qb
            ->where($qb->expr()->like("LOWER(b.name)", "?1"))
            ->setParameter("1","?".strtolower($title)."%")
            ->getQuery()
            ->getSingleResult();
    }



    public function findAllInArray()
    {
        return $this->createQueryBuilder('f')
            ->select('f.id, f.name')
            ->getQuery()
            ->getArrayResult();
    }
}
