<?php

namespace App\Repository;

use App\Entity\ProductInformationSource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductInformationSource|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductInformationSource|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductInformationSource[]    findAll()
 * @method ProductInformationSource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductInformationSourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductInformationSource::class);
    }
}
