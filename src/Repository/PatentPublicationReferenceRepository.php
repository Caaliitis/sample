<?php

namespace App\Repository;

use App\Entity\PatentPublicationReference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentPublicationReference|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentPublicationReference|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentPublicationReference[]    findAll()
 * @method PatentPublicationReference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentPublicationReferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentPublicationReference::class);
    }
}
