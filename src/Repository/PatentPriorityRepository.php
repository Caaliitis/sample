<?php

namespace App\Repository;

use App\Entity\PatentPriority;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentPriority|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentPriority|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentPriority[]    findAll()
 * @method PatentPriority[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentPriorityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentPriority::class);
    }

}
