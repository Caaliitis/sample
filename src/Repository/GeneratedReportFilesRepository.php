<?php

namespace App\Repository;

use App\Entity\Report\GeneratedReportFiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GeneratedReportFiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeneratedReportFiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeneratedReportFiles[]    findAll()
 * @method GeneratedReportFiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeneratedReportFilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeneratedReportFiles::class);
    }
}
