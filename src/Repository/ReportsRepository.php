<?php

namespace App\Repository;

use App\Entity\Report\Reports;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reports|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reports|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reports[]    findAll()
 * @method Reports[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reports::class);
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll()
    {
        return $this->createQueryBuilder('reports')
            ->select('count(reports.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param bool $filter
     * @param int $orderColumn
     * @param string $orderDir
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllQueryBuilder($filter = false, $orderColumn = 0, $orderDir = 'ASC')
    {
        $qb = $this->createQueryBuilder('reports');

        if ($filter) {
            $qb->andWhere('LOWER(reports.name) LIKE :filter')->setParameter('filter', '%' . strtolower($filter) . '%');
        }

        $qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

        return $qb;
    }

    protected function getColumnsOrder($orderColumn)
    {
        $array = [0 => 'reports.id', 1 => 'reports.name', 2 => 'reports.firstPage'];

        return $array[$orderColumn];
    }
}
