<?php

namespace App\Repository;

use App\Entity\CompanyImages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompanyImages|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyImages|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyImages[]    findAll()
 * @method CompanyImages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyImagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyImages::class);
    }

    /**
     * @param $company_id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll($company_id)
	{
		return $this->createQueryBuilder('zwci')
			->select('count(zwci.id)')
			->andWhere('zwci.company = :company_id')->setParameter('company_id', $company_id)
			->andWhere('zwci.state = 1')
			->getQuery()
			->getSingleScalarResult();
	}

    /**
     * @param int $orderColumn
     * @param string $orderDir
     * @param $company_id
     * @return \Doctrine\ORM\QueryBuilder
     */
	public function findAllQueryBuilder($orderColumn = 0, $orderDir = 'ASC', $company_id)
	{
		$qb = $this->createQueryBuilder('zwci');

		$qb->andWhere('zwci.company = :company_id')->setParameter('company_id', $company_id);
		$qb->andWhere('zwci.state = 1');

		$qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

		return $qb;
	}

    /**
     * @param $orderColumn
     * @return mixed
     */
	protected function getColumnsOrder($orderColumn)
	{
		$array = array(0 => 'zwci.id', 1 => 'zwci.source', 2 => 'zwci.defaultImg');

		return $array[$orderColumn];
	}
}
