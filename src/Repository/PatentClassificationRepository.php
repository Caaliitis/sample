<?php

namespace App\Repository;

use App\Entity\PatentClassification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentClassification|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentClassification|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentClassification[]    findAll()
 * @method PatentClassification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentClassificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentClassification::class);
    }

}
