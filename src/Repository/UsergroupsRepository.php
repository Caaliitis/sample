<?php

namespace App\Repository;

use App\Entity\Usergroups;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;


/**
 * @method Usergroups|null find($id, $lockMode = null, $lockVersion = null)
 * @method Usergroups|null findOneBy(array $criteria, array $orderBy = null)
 * @method Usergroups[]    findAll()
 * @method Usergroups[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsergroupsRepository extends NestedTreeRepository
{
	public function __construct(EntityManagerInterface $em, ClassMetadata $class)
	{
		parent::__construct($em, $class);
	}

    /**
     * @param $value
     * @return mixed
     */
    public function findByTitle($value)
    {
        return $this->createQueryBuilder('g')
            ->where('LOWER(g.title) LIKE :value')->setParameter('value', '%'.strtolower($value).'%')
            ->orderBy('g.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

}
