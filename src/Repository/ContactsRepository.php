<?php

namespace App\Repository;

use App\Entity\Contacts;
use App\Traits\CountPerMonthTrait;
use App\Traits\UbersearchTrait;
use App\Traits\UpdatedIdsAfterDateTime;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contacts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contacts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contacts[]    findAll()
 * @method Contacts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactsRepository extends ServiceEntityRepository
{

    use UbersearchTrait, CountPerMonthTrait, UpdatedIdsAfterDateTime;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contacts::class);
    }


    /**
     * @param $sql
     * @return mixed[]
     * @throws DBALException
     */
    public function executePlainSQL($sql)
    {
        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function totalCount(): ?int
    {
        try {
            return $this->createQueryBuilder('c')->select('count(c.id)')->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

}
