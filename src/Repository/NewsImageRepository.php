<?php

namespace App\Repository;

use App\Entity\NewsImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NewsImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsImage[]    findAll()
 * @method NewsImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsImage::class);
    }

}
