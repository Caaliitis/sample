<?php

namespace App\Repository;

use App\Entity\TrademarkMessenger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TrademarkMessenger|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrademarkMessenger|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrademarkMessenger[]    findAll()
 * @method TrademarkMessenger[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrademarkMessengerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrademarkMessenger::class);
    }
}
