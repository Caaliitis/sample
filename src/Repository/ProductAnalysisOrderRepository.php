<?php

namespace App\Repository;

use App\Entity\ProductAnalysisOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductAnalysisOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductAnalysisOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductAnalysisOrder[]    findAll()
 * @method ProductAnalysisOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductAnalysisOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductAnalysisOrder::class);
    }

}
