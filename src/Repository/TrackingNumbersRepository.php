<?php

namespace App\Repository;

use App\Entity\TrackingNumbers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TrackingNumbers|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrackingNumbers|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrackingNumbers[]    findAll()
 * @method TrackingNumbers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrackingNumbersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrackingNumbers::class);
    }

}
