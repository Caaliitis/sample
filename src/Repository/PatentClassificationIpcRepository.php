<?php

namespace App\Repository;

use App\Entity\PatentClassificationIpc;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatentClassificationIpc|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatentClassificationIpc|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatentClassificationIpc[]    findAll()
 * @method PatentClassificationIpc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatentClassificationIpcRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatentClassificationIpc::class);
    }

}
