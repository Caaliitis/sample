<?php

namespace App\Repository;

use App\Entity\Report\ReportSections;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReportSections|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReportSections|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReportSections[]    findAll()
 * @method ReportSections[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportSectionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReportSections::class);
    }

    /**
     * @param $reportId
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountAll($reportId)
    {
        return $this->createQueryBuilder('report_sections')
            ->select('count(report_sections.id)')
            ->andWhere('report_sections.report = :report_id')->setParameter('report_id', $reportId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int $orderColumn
     * @param string $orderDir
     * @param $reportId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllQueryBuilder($orderColumn = 0, $orderDir = 'ASC', $reportId)
    {
        $qb = $this->createQueryBuilder('report_sections');

        $qb->andWhere('report_sections.report = :report_id')->setParameter('report_id', $reportId);

        $qb->orderBy($this->getColumnsOrder($orderColumn), $orderDir);

        return $qb;
    }

    /**
     * @param $orderColumn
     * @return mixed
     */
    protected function getColumnsOrder($orderColumn)
    {
        $array = [
            0 => 'report_sections.id',
            1 => 'report_sections.name',
            2 => 'report_sections.ordering',
            3 => 'report_sections.isHidden',
        ];

        return $array[$orderColumn];
    }
}
