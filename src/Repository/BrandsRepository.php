<?php

namespace App\Repository;

use App\Entity\Brands;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use App\Traits\UbersearchTrait;

/**
 * @method Brands|null find($id, $lockMode = null, $lockVersion = null)
 * @method Brands|null findOneBy(array $criteria, array $orderBy = null)
 * @method Brands[]    findAll()
 * @method Brands[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrandsRepository extends ServiceEntityRepository
{
    use UbersearchTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Brands::class);
    }

    /**
     * @param string $title
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function searchByLowercaseTitle(string $title): ?Brands
    {
        $qb = $this->createQueryBuilder('b');
        return $qb
            ->where($qb->expr()->like("LOWER(b.name)", "?1"))
            ->setParameter("1", "?" . strtolower($title) . "%")
            ->getQuery()
            ->getSingleResult();
    }
}
