<?php

namespace App\Repository;

use App\Entity\AnalysisType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AnalysisType|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnalysisType|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnalysisType[]    findAll()
 * @method AnalysisType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnalysisTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnalysisType::class);
    }

}
