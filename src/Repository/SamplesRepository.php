<?php

namespace App\Repository;

use App\Entity\Samples;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Samples|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Samples|null findOneBy( array $criteria, array $orderBy = null )
 * @method Samples[]    findAll()
 * @method Samples[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class SamplesRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, Samples::class );
	}
}
