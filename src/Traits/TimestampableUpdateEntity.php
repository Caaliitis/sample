<?php

namespace App\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait TimestampableUpdateEntity
 *
 * @package App\Traits
 * @ORM\HasLifecycleCallbacks()
 */
trait TimestampableUpdateEntity
{
	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime",type="datetime")
	 * @Assert\Type("datetime")
	 * @SerializedName("updatedAt")
	 */
	protected $updatedAt;

    /**
     * @return $this
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
	public function setUpdatedAt(): self
	{
		$this->updatedAt = new \DateTime();
		return $this;
	}

    /**
     * @return \DateTimeInterface
     */
	public function getUpdatedAt(): ?\DateTimeInterface
	{
		return $this->updatedAt;
	}
}
