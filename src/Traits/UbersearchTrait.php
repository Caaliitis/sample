<?php


namespace App\Traits;

use Doctrine\ORM\Query\ResultSetMappingBuilder;
use App\Entity\Products;

trait UbersearchTrait
{

	/**
	 * @param string $searchString
	 * @param array $fields
	 * @param int|null $offset
	 * @param int|null $limit
	 * @return Products[]
	 */
	public function uberSearch(string $searchString, array $fields, ?int $limit = null, ?int $offset = null): array
	{
		/*$result = [
			'meta' => [
				'total' => 0
			],
			'items' => []
		];*/

		$em = $this->getEntityManager();

		$tableName = $em->getClassMetadata($this->_entityName)->getTableName();
		$schema = $em->getClassMetadata($this->_entityName)->getSchemaName();
		$tokenizedTableName = $tableName . '_tokenized';
		$tableToLowerCase = strtolower($tableName);
		$relationFieldName = (substr($tableToLowerCase, strlen($tableToLowerCase) - 3) === 'ies') ? trim($tableToLowerCase, 'ies') . 'y_id' : trim($tableToLowerCase, 's') . '_id';

		/**
		 * Data
		 */
		$rsm = new ResultSetMappingBuilder($em);
//		$rsm->addRootEntityFromClassMetadata($this->_entityName, $this->_entityName[0]);
		$alias = $this->_entityName[0];
		$rsm->addEntityResult($this->_entityName, $alias);
		$rsm->addFieldResult($alias, 'id', 'id');
		$rsm->addFieldResult($alias, 'name', 'name');

		$sql = "SELECT *
			FROM 
				$schema.$tableName
			WHERE id in (
				SELECT 
			  		$relationFieldName
				FROM 
					$schema.$tokenizedTableName
				WHERE 
				" . implode(' OR ', array_map(function ($val) {
				return $val .= ' @@ to_tsquery(:q)';
			}, $fields)) . ")";

		if ($limit !== null && $offset !== null) {
			$sql .= ' limit ' . $limit . ', ' . $offset;
		} else if ($limit !== null) {
			$sql .= ' limit ' . $limit;
		}

		$query = $em->createNativeQuery($sql, $rsm);
		$query->setParameters(['q' => $searchString]);

		return $query->getResult();


		/**
		 * Meta
		 */
		/*
				$rsm = new ResultSetMappingBuilder($em);
				$rsm->addScalarResult('qty', 'total', 'integer');

				$sql = "
						SELECT
							  count($relationFieldName) as qty
						FROM
							$schema.$tokenizedTableName
						WHERE
						" . implode(' OR ', array_map(function($val) {
						return $val .= ' @@ to_tsquery(:q)';
					}, $fields));

				$query = $em->createNativeQuery($sql, $rsm);
				$query->setParameters(['q' => $searchString]);

				$result['meta']['total'] = $query->getSingleScalarResult();

				return $result;
		*/
	}


	public function ubserSearchTokenize(array $fields)
	{
		$em = $this->getEntityManager();

		$tableName = $em->getClassMetadata($this->_entityName)->getTableName();
		$schema = $em->getClassMetadata($this->_entityName)->getSchemaName();
		$tokenizedTableName = $tableName . '_tokenized';
		$tableToLowerCase = strtolower($tableName);
		$relationFieldName = (substr($tableToLowerCase, strlen($tableToLowerCase) - 3) === 'ies') ? trim($tableToLowerCase, 'ies') . 'y_id' : trim($tableToLowerCase, 's') . '_id';

		$sql = "WITH data as (SELECT
                	" . implode('', array_map(function ($field) {
				return "to_tsvector('english', d1.$field) AS $field, ";
			}, $fields)) . "
                	d1.id AS $relationFieldName
              	FROM $schema.$tableName d1
                LEFT JOIN $schema.$tokenizedTableName d2 ON d1.id = d2.$relationFieldName
              	WHERE d2.$relationFieldName IS NULL
			)
			INSERT INTO $schema.$tokenizedTableName (" . implode(', ', array_merge($fields, [$relationFieldName])) . ")
  			SELECT * FROM data;";

		$stmt = $em->getConnection()->prepare($sql)->execute();
	}
}