<?php

namespace App\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Trait TimestampableCreateEntity
 *
 * @package App\Traits
 * @ORM\HasLifecycleCallbacks()
 */
trait TimestampableCreateEntity
{
	/**
	 * @ORM\Column(type="datetime",type="datetime")
	 * @SerializedName("createdAt")
	 * @Groups({"date"})
	 */
	protected $createdAt;


    /**
     * @return $this
     * @ORM\PrePersist()
     */
	public function setCreatedAt(): self
	{
		$this->createdAt = new \DateTime();

		return $this;
	}

    /**
     * @return \DateTimeInterface
     */
	public function getCreatedAt(): ?\DateTimeInterface
	{
		return $this->createdAt;
	}

}
