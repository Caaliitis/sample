<?php

namespace App\Traits;

use App\Entity\Users;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Trait UserCreateEntity
 *
 * @package App\Traits
 */
trait UserCreateEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Users")
	 * @ORM\JoinColumn(nullable=false)
	 * @MaxDepth(1)
	 */
	private $created;

    /**
     * @return Users
     */
	public function getCreated(): Users
	{
		return $this->created;
	}

    /**
     * @param Users $created
     */
	public function setCreated(Users $created)
    {
		$this->created = $created;
	}

}