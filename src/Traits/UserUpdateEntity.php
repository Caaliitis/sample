<?php

namespace App\Traits;

use App\Entity\Users;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Trait UserUpdateEntity
 *
 * @package App\Traits
 */
trait UserUpdateEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Users")
	 * @ORM\JoinColumn(nullable=false)
	 * @MaxDepth(1)
	 */
	private $updated;

    /**
     * @return Users
     */
	public function getUpdated(): Users
	{
		return $this->updated;
	}

    /**
     * @param Users $updated
     */
	public function setUpdated(Users $updated)
	{
		$this->updated = $updated;
	}
}