<?php


namespace App\Traits;


trait CompanyVariantGenerator
{
    private function generate(string $name): array
    {
        $array = ['\\sCo\.\,\\sLtd\.', '\\sCo\.\,\\sLtd', '\\slimited', '\\sltd', '\\sltd\.',];
        $arrayStr = [' company limited', ' Co., Ltd', ' Co., Ltd.', ' limited', ' ltd', ' ltd.'];
        $name = trim($name);

        $pattern = "/(%s)$/i";
        $result = [];

        foreach ($array as $key => $strSearch) {
            if (!empty(preg_match(sprintf($pattern, $strSearch), $name))) {
                $currentPattern = sprintf($pattern, $strSearch);
                break;
            }
        }

        if (isset($currentPattern)) {
            foreach ($arrayStr as $strSearch) {
                $result[] = preg_replace($currentPattern, $strSearch, $name);
            }
        } else {
            $result[] = $name;
        }

        return $result;
    }
}