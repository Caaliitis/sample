<?php

namespace App\Traits;

use DateTime;

trait CountPerMonthTrait
{

    public function getCountsPerMonth(): array
    {

        $em = $this->getEntityManager();

        $tableName = $em->getClassMetadata($this->_entityName)->getTableName();
        $schema = $em->getClassMetadata($this->_entityName)->getSchemaName();
        $table = strtolower($schema . '.' . $tableName);

        $currentDate = new DateTime();
        $currentDate->modify('-11 months');

        try {
            $date = new DateTime(sprintf('%s-01', $currentDate->format('Y-m')));
        } catch (\Exception $e) {
            return [];
        }

        $sql = "select to_char(created_at,'MM') as month_nr,
                       to_char(created_at,'Mon') as month,
                       extract(year from created_at) as year,
                       count(*) as count
                from " . $table . "
                where created_at >= to_timestamp(" . $date->getTimestamp() . ")
                group by 1,2,3
                ORDER BY 3,1";

        $thisYear = $this->executePlainSQL($sql);
        $sql = "select count(*) as count from " . $table . " where created_at < to_timestamp(" . $date->getTimestamp() . ")";
        $base = $this->executePlainSQL($sql);
        $sum = $base[0]["count"];

        $periodOneYear = $this->getPeriodOneYear();

        $f = function (&$value, $key, $thisYear) use (&$sum) {
            foreach ($thisYear as $item) {
                if ($item['month_nr'] === $value['month_nr'] && $item['year'] === $value['year']) {
                    $sum += $item["count"];
                }
            }
            $value['count'] = $sum;

        };

        array_walk($periodOneYear, $f, $thisYear);

        return $periodOneYear;

    }

    private function getPeriodOneYear(): array
    {
        $currentDate = new DateTime();
        $start = $currentDate->modify('-11 months');
        $currentDate = new DateTime();
        $end = $currentDate->modify('+1 month');

        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);

        $result = [];

        foreach ($period as $key => $dt) {
            $result[$key]['month_nr'] = $dt->format("m");
            $result[$key]['month'] = $dt->format("M");
            $result[$key]['year'] = $dt->format("Y");
            $result[$key]['count'] = 0;
        }

        return $result;

    }
}