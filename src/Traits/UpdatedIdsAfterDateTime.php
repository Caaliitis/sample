<?php


namespace App\Traits;


use DateTime;

trait UpdatedIdsAfterDateTime
{
    public function findUpdatedIdsAfterDateTime(DateTime $dateTime): array
    {
        return array_column($this->createQueryBuilder('e')
            ->select('e.id')
            ->andWhere('e.updatedAt > :dateTime')
            ->setParameter('dateTime', $dateTime)
            ->getQuery()
            ->getArrayResult() , 'id');
    }
}