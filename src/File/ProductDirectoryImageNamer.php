<?php

namespace App\File;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class ProductDirectoryImageNamer implements DirectoryNamerInterface
{

	public function directoryName( $object, PropertyMapping $mapping ): string
	{
		$ds     = DIRECTORY_SEPARATOR;

		$productId = $object->getProduct()->getId();

		return 'products' . $ds . $productId . $ds . 'images' . $ds;
	}
}