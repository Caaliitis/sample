<?php

namespace App\File;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class CompanyDirectoryImageNamer implements DirectoryNamerInterface
{

	public function directoryName( $object, PropertyMapping $mapping ): string
	{
		$ds     = DIRECTORY_SEPARATOR;

		$companyId = $object->getCompany()->getId();

		return 'companies' . $ds . $companyId . $ds . 'images' . $ds;
	}
}