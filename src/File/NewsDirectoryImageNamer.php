<?php

namespace App\File;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class NewsDirectoryImageNamer implements DirectoryNamerInterface
{

	public function directoryName( $object, PropertyMapping $mapping ): string
	{
		$ds     = DIRECTORY_SEPARATOR;
		$newsId = $object->getNews()->getId();

		return 'news' . $ds . $newsId . $ds . 'images' . $ds;
	}
}