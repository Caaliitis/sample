<?php

namespace App\File;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class PatentDirectoryContentNamer implements DirectoryNamerInterface
{

	public function directoryName( $object, PropertyMapping $mapping ): string
	{
		$ds     = DIRECTORY_SEPARATOR;
		$patentId = $object->getPatent()->getId();

		return 'patent' . $ds . $patentId . $ds . 'contents' . $ds;
	}
}