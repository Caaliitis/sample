<?php


namespace App\File;


interface ContentUri
{
    public function getContentUris();
}