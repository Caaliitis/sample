<?php

namespace App\File;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class TrademarkDirectoryImageNamer implements DirectoryNamerInterface
{

	public function directoryName( $object, PropertyMapping $mapping ): string
	{
		$ds          = DIRECTORY_SEPARATOR;
		$trademarkId = $object->getTrademark()->getId();

		return 'trademarks' . $ds . $trademarkId . $ds . 'images' . $ds;
	}
}