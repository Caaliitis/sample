<?php


namespace App\Command;

use App\Repository\ProductsRepository;
use App\Ubersearch\Ubersearch;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\Console\Style\SymfonyStyle;

class CommandUbersearchTokenize extends Command
{
	protected static $defaultName = 'wims:ubersearch:tokenize';

	private $io;
	private $container;

	private $productsRepository;

	protected function configure()
	{
		$this->setDescription('get content')
			->setHelp('Run this to to tokenize data');
	}

	/**
	 * CommandUbersearch constructor.
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	public function __construct(ContainerInterface $container)
	{
		parent::__construct(null);

		$this->container = $container;
	}


	public function execute(InputInterface $input, OutputInterface $output)
	{

		$this->io = new SymfonyStyle($input, $output);

		$entities = $this->container->getParameter('ubersearch');

		foreach ($entities as $entity) {
			$result = $this->container->get('doctrine')->getRepository($entity['class'])->ubserSearchTokenize($entity['fields']);
		}

		return 0;
	}
}