<?php

namespace App\Command;

use App\Entity\OrderStatus;
use App\Entity\ProductAnalysisOrder;
use App\Mailer\MailGunMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Symfony\Component\Validator\Constraints as Assert;

class AnalysisOrderCommand extends Command
{
    protected static $defaultName = 'wims:analysis-order';

    private $entityManager;

    private $mailer;

    private $parameterBag;

    private $validator;

    public function __construct(
        EntityManagerInterface $entityManager,
        MailGunMailer $mailer,
        ParameterBagInterface $parameterBag,
        ValidatorInterface $validator,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->validator = $validator;
    }


    protected function configure()
    {
        $this
            ->setDescription('Check a new analysis order');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $analysisOrders = $this->entityManager->getRepository(ProductAnalysisOrder::class)->findBy(['status' => NULL]);

        $statusList = $this->entityManager->getRepository(OrderStatus::class)->findBy(['useInAnalysisOrder' => true], ['ordering' => 'ASC']);
        $firstStatus = $this->entityManager->getRepository(OrderStatus::class)->findOneBy(['id' => 1]);

        $managerEmail = $this->parameterBag->get('manager_email');
        $violations = $this->validator->validate($managerEmail, new Assert\Email());

        if ($violations->count() !== 0) {
            $io->note('The command has failed. You need to set manager email param before execution');
            return 0;
        }


        if (empty($analysisOrders)) {
            $io->success('Order List is empty');
            return 0;
        }


        $isSent = $this->mailer->sendEmail(
            $managerEmail,
            'Analysis Order',
            "analysis-order.html.twig",
            [
                "orders" => $analysisOrders,
                "statuses" => $statusList
            ]
        );

        if (false === $isSent) {
            $io->error('Send email error');
            return 0;
        }

        foreach ($analysisOrders as $order) {
            $order->setStatus($firstStatus);
            $this->entityManager->persist($order);
            $this->entityManager->flush();
        }

        $io->success('Done');

        return 0;
    }
}
