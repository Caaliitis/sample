<?php

namespace App\Command;

use App\Entity\Companies;
use App\Traits\CompanyVariantGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DataUpdateTmCompanyCommand extends Command
{
    use CompanyVariantGenerator;

    protected static $defaultName = 'data:update-tm-company';
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(string $name = null, EntityManagerInterface $manager)
    {
        parent::__construct($name);
        $this->manager = $manager;
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $companies = $this->manager->getRepository(Companies::class)->findAllNames();

        $conn = $this->manager->getConnection();

        foreach ($companies as $company) {

            $companiesArray = $this->generate($company["name"]);

            $io->note($company["name"]);

            foreach ($companiesArray as $companyItem) {
                $io->note($companyItem);

	    $sql = '
UPDATE trademark.trademark
SET company_id = '.$company["id"].'
WHERE trademark.trademark.company_id IS NULL AND lower(trademark.trademark.holder) = lower(\''.$companyItem.'\');';

                $io->note($sql);

                    try {
                        $stmt = $conn->prepare( $sql );
                        $stmt->execute();
                    }
                    catch ( \Exception $e ) {
                        $io->error('DB error');
                    }
                }

        }


        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
