<?php

namespace App\Command;

use App\Entity\Report\GeneratedReportFiles;
use App\File\FileUploadedHelper;
use App\Mailer\MailGunHelper;
use App\Mailer\MailGunMailer;
use App\Pdf\PdfHelper;
use App\Report\PdfReport;
use App\Report\PptxReport;
use App\Report\ReportData;
use App\Report\Twig\TwigTemplate;
use App\Repository\ProductsRepository;
use App\Zip\ZipArchive;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\FileExistsException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class WimsReportGenerateCommand extends Command
{
    protected static $defaultName = 'wims:report:generate';

    private $manager;

    private $mailer;

    private $logger;

    private $pdfReport;

    private $parameterBag;

    private $zipArchive;

    private $pptxReport;

    private $productsRepository;

    private $twigTemplate;

    private $temporaryReportFilesystem;

    private $standingPptxReport;


    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $manager,
        MailGunMailer $mailer,
        PdfReport $pdfReport,
        ParameterBagInterface $parameterBag,
        ZipArchive $zipArchive,
        PptxReport $pptxReport,
        ProductsRepository $productsRepository,
        TwigTemplate $twigTemplate,
        FilesystemInterface $temporaryReportFilesystem,
        \App\Report\Standing\PptxReport $standingPptxReport
    )
    {
        $this->manager = $manager;
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->pdfReport = $pdfReport;
        $this->parameterBag = $parameterBag;
        $this->zipArchive = $zipArchive;

        $this->pptxReport = $pptxReport;
        $this->productsRepository = $productsRepository;

        $this->twigTemplate = $twigTemplate;
        $this->temporaryReportFilesystem = $temporaryReportFilesystem;
        $this->standingPptxReport = $standingPptxReport;

        parent::__construct();

    }


    protected function configure()
    {
        $this
            ->setDescription('Generate a report, host it on the Google Bucket and send email for author');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //get records from Report table
        /** @var GeneratedReportFiles $reportFile */
        $reportFile = $this->manager->getRepository(GeneratedReportFiles::class)->findOneBy(['completed' => false], ['id' => 'ASC']);

        if (null === $reportFile) {
            $io->warning('Not found any report for generation');

            return 0;
        }

        $reportFilePath = $this->parameterBag->get('kernel.project_dir') . '/var/reports/' . $reportFile->getId();
        $filesystem = new Filesystem();

        //check whether the report was finished
        if ($reportFile->getPagesAmount() <= $reportFile->getCurrentPage() && $reportFile->getPagesAmount() > 0) {
            $zipPath = $this->zipArchive->create($reportFilePath);

            $fileContent = file_get_contents($zipPath);

            $fileName = \str_replace('.', '', \uniqid('', true)) . ".zip";
            $filePath = '/files/' . $fileName;

            try {
                $this->temporaryReportFilesystem->write($filePath, $fileContent);
            } catch (FileExistsException $e) {
                $io->error("Can't write file");
            }

            $reportFile->setFile($fileName);
            $reportFile->setCompleted(true);
            $reportFile->setCurrentPage(0);
            $reportFile->setPagesAmount(0);
            $this->manager->persist($reportFile);
            $this->manager->flush();

            //remove temp file and all parts of reports
            $filesystem->remove($reportFilePath);
            $filesystem->remove($zipPath);

            //send email with a link
            $this->mailer->sendEmail(
                $reportFile->getCreated()->getEmail(),
                sprintf('Report: %s', $reportFile->getReport()->getName()),
                'report.html.twig',
                [
                    'reportLink' => $this->parameterBag->get('temporary_report_base_url') . $filePath
                ]
            );

            $io->note('The report was done');

            return 0;
        }

        //count products amount
        $count = $reportFile->getPagesAmount();

        if ($count === 0) {
            foreach ($reportFile->getReport()->getSections() as $section) {
                $count += $section->getProductQuantity();
            }

            $reportFile->setPagesAmount($count);
            $this->manager->persist($reportFile);
            $this->manager->flush();

            $io->note(sprintf('The report contains %d products', $reportFile->getPagesAmount()));
        }

        //generated file and relocate them to Google Bucket
        $reportFileFolder = $filesystem->exists($reportFilePath);

        if (false === $reportFileFolder) {
            $filesystem->mkdir($reportFilePath, 0770);
        }

        $successful = $this->generateReport($reportFile);

        $io->note($successful);

        if ($successful) {
            $reportFile->setCurrentPage($reportFile->getCurrentPage() + $reportFile->getStep());
            $this->manager->persist($reportFile);
            $this->manager->flush();
        }


        $io->success('Report Generator Command. Pass --help to see your options.');

        return 0;
    }

    private function generateReport(GeneratedReportFiles $reportFile): bool
    {

        if ($reportFile->getFormat() === 'pdf') {
            $report = new PdfReport();
            $reportData = new ReportData($this->productsRepository);
            $reportData->setEntity('products');
            $reportData->setTemplate(0);
            $reportData->setFormat($reportFile->getFormat());


            $report->setData($reportData);
            $report->setPath($this->parameterBag->get('kernel.project_dir'));
            $report->setTwig($this->twigTemplate);
            $report->setGeneratedReport($reportFile);

            $fileContent = $report->generate(true);

            if ($fileContent === null) {
                return false;
            }

            $part = floor($reportFile->getCurrentPage() / $reportFile->getStep() + 1);
            $isSaved = file_put_contents(sprintf('%s/var/reports/%s/report_part%s.pdf', $this->parameterBag->get('kernel.project_dir'), $reportFile->getId(), $part), $fileContent);


            if ($isSaved) {
                return true;
            }
        }


		if ($reportFile->getFormat() === 'pptx')
		{
			return $this->standingPptxReport->generateReport(
                $reportFile,
				$this->parameterBag->get('kernel.project_dir')
			);
		}


        return false;
    }
}
