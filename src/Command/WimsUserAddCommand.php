<?php

namespace App\Command;

use App\Entity\Usergroups;
use App\Entity\Users;
use App\Form\UserType;
use App\Mailer\MailGunMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WimsUserAddCommand extends Command
{
    protected static $defaultName = 'wims:user:add';

    private $encoder;

    private $form;

    private $entity;

    private $mailer;

    private $validator;

    public function __construct(UserPasswordEncoderInterface $encoder, FormFactoryInterface $form, EntityManagerInterface $entity, MailGunMailer $mailer, ValidatorInterface $validator)
    {
        $this->encoder = $encoder;
        $this->form = $form;
        $this->entity = $entity;
        $this->mailer = $mailer;
        $this->validator = $validator;

        parent::__construct();

    }


    protected function configure()
    {
        $this
            ->addOption('filter', null, InputOption::VALUE_REQUIRED, 'Filter roles by name')
            ->setDescription('Allow you create a new user');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $filter     = $input->getOption('filter');

        $output->writeln([
            'Create a new user',
            '=================',
            '',
        ]);

        $helper = $this->getHelper('question');

        $question = new Question('Please enter the email of the user: ');
        $question->setValidator(function ($answer) {
            //validate email address
            $violations = $this->validator->validate($answer, new Email());

            if ($violations->count() > 0) {
                /** @var ConstraintViolation $firstViolation */
                $firstViolation = $violations[0];
                throw new \RuntimeException(
                    $firstViolation->getMessage()
                );
            }

            //Check whether email isn't used
            $emailExist = $this->entity->getRepository(Users::class)->count(['email' => $answer]);

            if ($emailExist > 0) {
                throw new \RuntimeException(
                    sprintf('A user with current email "%s" already exists', $answer)
                );
            }

            return $answer;
        });
        $question->setMaxAttempts(2);

        $email = $helper->ask($input, $output, $question);

        $name = $io->ask('Please enter user full name: ');


        $manuallyPasswordStr = $io->choice('Do you want to set password manually', ['yes', 'no'], 'no');

        $manuallyPassword = filter_var($manuallyPasswordStr, FILTER_VALIDATE_BOOLEAN);

        $genPassword = $this->generatePassword(12);

        if ($manuallyPassword) {
            $password = $io->ask('Set a new password for user: ');
        }
        $io->note($genPassword);

        $sendEmailStr = $io->choice('Do you want to send for user information by email', ['yes', 'no'], 'no');

        $sendEmail = filter_var($sendEmailStr, FILTER_VALIDATE_BOOLEAN);


        $user = new Users();

        $password = $this->encoder->encodePassword($user, $genPassword);

        $data = [
            'name' => $name,
            'username' => $email,
            'email' => $email,
            'password' => $password
        ];


        $form = $this->form->create(UserType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            foreach ($form->getErrors() as $error) {

                $io->error('Invalid user data: ' . $error->getMessage());
            }

            return 0;
        }

        if ($sendEmail) {
            try {
                $this->mailer->sendEmail($email, 'Welcome to WIMS. Getting started!', 'welcome.html.twig',
                    [
                        'username'=> $email,
                        'password'=> $genPassword
                    ]
                );
                $io->note('The registration information was sent successfully');
            } catch (\Exception $exception) {
                $io->error("An email with registration information wasn't sent");
            }
        }

        $this->entity->persist($user);
        $this->entity->flush();


        $output->writeln([
            'Created a new user',
            '=================',
            '',
        ]);

        $output->writeln('Full Name: ' . $name);
        $output->writeln('Username: ' . $email);
        $output->writeln('Email: ' . $email);
        $output->writeln('Password: ' . $genPassword);

        $setRoles = $io->choice('Do you want to set roles for a new user?', ['yes', 'no'], 'no');


        if ($setRoles)
        {
            $this->addRoles($user, $io, $filter);

            return 0;
        }


        $io->success('A new user was created successfully. Pass --help to see your options.');

        return 0;
    }


    private function generatePassword(int $length): string
    {
        $array = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
        shuffle($array);

        $string = implode('', $array);

        return substr(str_shuffle($string), 0, $length);
    }

    private function addRoles(Users $user, SymfonyStyle $io, $filter)
    {
        if($filter){
            $roles = $this->entity->getRepository(Usergroups::class)->findByTitle($filter);
        }
        else{
            $roles = $this->entity->getRepository(Usergroups::class)->findAll();
        }

        $notExists = $user->getNotExistGroup($roles);

        foreach ($notExists as $role)
        {
            $answer = $io->choice(sprintf('Do you want to add the role "%s"', $role->getTitle()), ['yes', 'no', 'stop'], 'no');

            if ($answer === 'stop')
            {
                break;
            }

            $addRole = filter_var($answer, FILTER_VALIDATE_BOOLEAN);
            if ($addRole)
            {
                $user->addGroup($role);
            }

        }

        $this->entity->persist($user);
        $this->entity->flush();

        $this->showList($user, $io);
    }

    private function showList(Users $user, SymfonyStyle $io)
    {
        $rolesArray = array_map(function ($role) {
            return [$role];
        }, $user->getRoles());

        $io->table(
            ['User Roles'],
            $rolesArray
        );

        $io->success('Roles list has been showed successfully. Pass --help to see other options.');
    }

}
