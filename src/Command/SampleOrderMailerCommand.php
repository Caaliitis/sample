<?php

namespace App\Command;

use App\Entity\OrderStatus;
use App\Entity\ProductOrder;
use App\Mailer\MailGunMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SampleOrderMailerCommand extends Command
{
	protected static $defaultName = 'wims:sample-order-mailer';

	private $em;

	private $validator;

	private $mailer;

    private $parameterBag;

    public function __construct(
	    EntityManagerInterface $em,
        ValidatorInterface $validator,
        MailGunMailer $mailer,
        ParameterBagInterface $parameterBag
    )
	{
		$this->em        = $em;
		$this->validator = $validator;
		$this->mailer    = $mailer;

		parent::__construct();
        $this->parameterBag = $parameterBag;
    }

	protected function configure()
	{
		$this
			->setDescription('Check orders and send them by email');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{

		$unsentOrders = $this->em->getRepository(ProductOrder::class)->findBy(['isSent' => false]);

        $statusList = $this->em->getRepository(OrderStatus::class)->findBy(['useInProductOrder' => true], ['ordering' => 'ASC']);

		if (empty($unsentOrders))
		{
			return 0;
		}

		try
		{
            $email = $this->parameterBag->get('manager_email');

            $this->mailer->sendEmail($email, "Product SAMPLE order", "product-order-bunch.html.twig", [
                "orders" => $unsentOrders,
                "statuses" => $statusList
            ]);
		}
		catch (\Exception $exception)
		{
			return 0;
		}


		/** @var ProductOrder $order */
		foreach ($unsentOrders as $order)
		{
			$order->setIsSent(true);
			$this->em->persist($order);
			$this->em->flush();
		}


		return 0;
	}
}
