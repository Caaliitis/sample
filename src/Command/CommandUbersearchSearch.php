<?php

namespace App\Command;

use App\Service\Ubersearch;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\Console\Style\SymfonyStyle;

class CommandUbersearchSearch extends Command
{
	protected static $defaultName = 'wims:ubersearch:search';

	private $io;
	private $container;
	private $ubersearch;

	protected function configure()
	{
		$this->setDescription('get content')
			->setHelp('Run this to search data');

		$this->addArgument('text', InputArgument::REQUIRED, 'Text to search');
	}

	/**
	 * CommandUbersearch constructor.
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	public function __construct(ContainerInterface $container, Ubersearch $ubersearch)
	{
		parent::__construct(null);

		$this->container = $container;
		$this->ubersearch = $ubersearch;
	}


	public function execute(InputInterface $input, OutputInterface $output)
	{

		$this->io = new SymfonyStyle($input, $output);

		$text = $input->getArgument('text');

		$parsedText = Ubersearch::parseSearchString($text);

		$this->io->writeln('Parsed string: ' . $parsedText);


		$result = $this->ubersearch->unionSearch($parsedText, ['products', 'brands'], 100, 0, 1);

		$entities = $this->container->getParameter('ubersearch');

		foreach ($entities as $entity) {
			$this->io->section($entity['class']);

			$result = $this->container->get('doctrine')->getRepository($entity['class'])->uberSearch($parsedText, $entity['fields']);

		//	$this->io->writeln('Count: ' . $result['meta']['total']);

			foreach ($result as $res) {
				$this->io->writeln($res->getId() . ': ' . $res->getName());
			}
		}

		return 0;
	}
}