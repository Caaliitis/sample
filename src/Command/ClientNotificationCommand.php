<?php

namespace App\Command;

use App\Connector\WimsConnector;
use App\Entity\Companies;
use App\Entity\Components;
use App\Entity\Contacts;
use App\Entity\NotificationFilters;
use App\Entity\NotificationLog;
use App\Entity\Products;
use App\Entity\Users;
use App\Mailer\MailGunMailer;
use App\Traits\UpdatedIdsAfterDateTime;
use DateTime;
use Doctrine\Inflector\Inflector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ClientNotificationCommand
 * @package App\Command
 */
class ClientNotificationCommand extends Command
{
    const STATUS_ADDED = 'Added';
    const STATUS_REMOVED = 'Removed';
    const STATUS_UPDATED = 'Updated';

    const ENTITIES = [
        ['name' => 'companies', 'class' => Companies::class],
        ['name' => 'products', 'class' => Products::class],
        ['name' => 'contacts', 'class' => Contacts::class],
        ['name' => 'components', 'class' => Components::class],
    ];

    private $entityManager;

    private $mailer;

    private $wimsConnector;

    private $runLog;

    private $inflector;

    protected static $defaultName = 'app:notify-clients';

    public function __construct(
        EntityManagerInterface $entityManager,
        MailGunMailer $mailer,
        WimsConnector $wimsConnector
    )
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->wimsConnector = $wimsConnector;
        $this->inflector = new Inflector();
    }

    protected function configure()
    {
        $this->setDescription('Checks for the last changes in client favorites and notifies clients');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userMessages = [];

        $output->writeln('Collecting users with filled favorites');
        $users = $this->collectUsers();
        $output->writeln(count($users) . ' users collected');

        $lastLog = $this->entityManager->getRepository(NotificationLog::class)
            ->findOneBy([], ['id' => 'DESC']);

        if ($lastLog) {
            $lastDateTime = $lastLog->getCreatedAt();
        } else {
            $lastDateTime = (new DateTime())->modify('-1 day');
        }

        $output->writeln('Last run datetime is ' . $lastDateTime->format('Y-m-d H:i:s'));

        $this->runLog = new NotificationLog();
        $this->log('Starting run');

        /**
         * Collecting updated entities
         */
        $updatedEntityIds = [];
        foreach (self::ENTITIES as $entity) {
            $output->writeln(sprintf('Collecting recently updated %s', $entity['name']));
            $repository = $this->entityManager->getRepository($entity['class']);

            try {

                $hasTrait = in_array(
                    UpdatedIdsAfterDateTime::class,
                    array_keys((new \ReflectionClass($repository))->getTraits())
                );

                if (!$hasTrait) {
                    $output->writeln('Class doesnt have Trait');
                    continue;
                }

            } catch (\ReflectionException $e) {
                $output->writeln('Reflection error');
                continue;
            }

            $updatedEntityIds[$entity['name']] = $repository->findUpdatedIdsAfterDateTime($lastDateTime);
            $output->writeln(sprintf('%d %s found that were updated since last run datetime', count($updatedEntityIds[$entity['name']]), $entity['name']));
            $this->log(sprintf('LOG: %d  updated %s found', count($updatedEntityIds[$entity['name']]), $entity['name']));
        }


        /**
         * Loop through each client with favorites
         */
        $output->writeln('Looping through collected users');
        foreach ($users as $user) {

            $output->writeln('Handling client ' . $user->getName());

            //Bookmarks
            foreach (json_decode($user->getUiSettings(), true)['bookmarks'] as $bookmark) {

                /**
                 * Handling entities updates
                 */
                if (is_array($bookmark) &&
                    array_key_exists('category', $bookmark) &&
                    in_array($bookmark['id'], $updatedEntityIds[$bookmark['category']])) {

                    if (null === $class = $this->getClassByName($bookmark['category'])) {
                        continue;
                    }

                    $object = $this->entityManager->getRepository($class)
                        ->find($bookmark['id']);

                    $singularityWord = $this->inflector->tableize($this->inflector->singularize($bookmark['category']));

                    if ($object === null) {
                        $this->log(sprintf('ERROR: %s id %d was not found', $singularityWord, $bookmark['id']));
                        break;
                    }

                    $userMessages[$user->getId()][$bookmark['category']][] = [
                        'id' => $bookmark['id'],
                        'title' => $bookmark['title'],
                    ];

                    $this->log(
                        sprintf(
                            'LOG: type "%s", Client Id %d, %s Id %d, %s Name "%s"',
                            $bookmark['category'],
                            $user->getId(),
                            $singularityWord,
                            $bookmark['id'],
                            $singularityWord,
                            $bookmark['title']
                        )
                    );
                }
            }

            //Filters
            foreach (json_decode($user->getUiSettings(), true)['filters'] as $filter) {

                $filter = json_decode($filter, true);

                $query = $this->createFromFEData($filter['conditions']);
                $query .= '&properties[]=id';
                $query .= '&searchRule=' . $filter['searchRule'];

                $previousFilterRun = $this->entityManager->getRepository(NotificationFilters::class)
                    ->findOneBy([
                        'users' => $user,
                        'label' => $filter['label'],
                    ]);

                /**
                 * Handling items filters
                 */
                $updatedEntities = [];

                $currentItemIds = $this->getAllFromApi($filter['category'], $query);
                $previousItemIds = [];

                $currentClass = $this->getClassByName($filter['category']);

                if ($previousFilterRun) {
                    $previousItemIds = $previousFilterRun->getItems($filter['category']);
                }

                /**
                 * Compare filter results with previous run
                 */
                $itemsAdded = array_diff($currentItemIds, $previousItemIds);
                $itemsRemoved = array_diff($previousItemIds, $currentItemIds);

                if (!empty($itemsAdded) || !empty($itemsRemoved)) {
                    $notificationFilter = new NotificationFilters();
                    $notificationFilter->setUsers($user);
                    $notificationFilter->setLabel($filter['label']);
                    $notificationFilter->setItems($filter['category'], $currentItemIds);

                    $this->entityManager->persist($notificationFilter);
                    $this->entityManager->flush();
                }

                /**
                 * Generate message lines for added items
                 */
                if (!empty($itemsAdded)) {

                    foreach ($itemsAdded as $itemId) {

                        $item = $this->entityManager->getRepository($currentClass)->find($itemId);

                        $updatedEntities[] = [
                            'id' => $item->getId(),
                            'title' => $item->getName(), //?CONTACTS
                            'status' => self::STATUS_ADDED,
                        ];
                    }
                }

                /**
                 * Generate message lines for removed items
                 */
                if (!empty($itemsRemoved)) {

                    foreach ($itemsRemoved as $itemId) {

                        $item = $this->entityManager->getRepository($currentClass)->find($itemId);

                        $updatedEntities[] = [
                            'id' => $item->getId(),
                            'title' => $item->getName(),
                            'status' => self::STATUS_REMOVED,
                        ];
                    }
                }


                /**
                 * Check all filter items for updates
                 */
                foreach ($currentItemIds as $itemId) {

                    if (in_array($itemId, $updatedEntityIds[$filter['category']]) &&
                        !in_array($itemId, $itemsAdded)) {

                        $item = $this->entityManager->getRepository($currentClass)
                            ->find($itemId);

                        $updatedEntities[] = [
                            'id' => $item->getId(),
                            'title' => $item->getName(),
                            'status' => self::STATUS_UPDATED,
                        ];
                    }
                }

                if (!empty($updatedEntities)) {

                    $messageToLog = '';
                    foreach ($updatedEntities as $updatedEntity) {

                        $messageToLog .= implode(', ', $updatedEntity) . '; ';
                    }

                    $this->log(
                        'LOG: type "company filter", Client Id ' . $user->getId() .
                        ', Filter Label "' . $filter['label'] . '"' .
                        ', message "' . $messageToLog . '"'
                    );

                    $userMessages[$user->getId()]['filters'][] = [
                        'label' => $filter['label'],
                        'category' => $filter['category'],
                        'entities' => $this->sortByTitleAsc($updatedEntities),
                    ];
                }

            }

            if (array_key_exists($user->getId(), $userMessages)) {

                $userMessages[$user->getId()]['user'] = $user;
            }
        }

        $this->log('LOG: ' . count($userMessages) . ' message will be sent');

        /**
         * Send prepared notifications
         */
        $output->writeln('Sending ' . count($userMessages) . ' prepared messages');
        $this->sendNotifications($userMessages);
        $output->writeln('All sent');

        exit(1);
    }

    /**
     * @return Users[]
     */
    private function collectUsers(): array
    {
        $result = [];

        $users = $this->entityManager->getRepository(Users::class)->collectUsersForEmailSubscription();

        /** @var Users $user */
        foreach ($users as $user) {

            $uiSettings = json_decode($user->getUiSettings(), true);

            if (empty($uiSettings)) {
                continue;
            }

            if (array_key_exists('bookmarks', $uiSettings) && !empty($uiSettings['bookmarks'])) {
                $result[] = $user;
            }

            if (array_key_exists('filters', $uiSettings) && !empty($uiSettings['filters'])) {
                $result[] = $user;
            }
        }

        return $result;
    }


    private function log(string $newString): void
    {
        if ($this->runLog->getLog()) {

            $this->runLog->setLog($this->runLog->getLog() . PHP_EOL . $newString);

        } else {

            $this->runLog->setLog($newString);
        }

        $this->entityManager->persist($this->runLog);
        $this->entityManager->flush();
    }


    private function createFromFEData(?array $conditions): string
    {
        $data = [];
        $features = [];

        foreach ($conditions as $key => $condition) {

            if (in_array($key, ['id', 'name', 'marking'])) {

                $data[] = $key . '=' . $condition['single'];

            } elseif (in_array($key, ['location', 'category', 'types', 'types.id'])) {

                foreach ($condition['list'] as $item) {

                    $data[] = $key . '[]=' . $item;
                }

            } elseif (in_array($key, ['created'])) {

                $data[] = 'createdAt[after]=' . $condition['from'];
                $data[] = 'createdAt[before]=' . $condition['to'];

            } elseif (strpos($key, 'feature') !== false) {

                $featureId = (int)filter_var($key, FILTER_SANITIZE_NUMBER_INT);

                if (array_key_exists('from', $condition) || array_key_exists('to', $condition)) {

                    $features[] = '{"' . $featureId . '":"' . $condition['from'] . '...' . $condition['to'] . '"}';
                }
            }
        }

        if (!empty($features)) {

            $data[] = 'features[' . implode(',', $features) . ']';
        }

        return implode('&', $data);
    }


    private function getAllFromApi(string $objectKey, string $query): array
    {
        $result = [];
        $paginatedCollection = [];
        $itemsPerPage = 100;

        //make response
        $objectCount = $this->wimsConnector->getTotalCount(sprintf('/api/%s/total_items', $objectKey));

        $pageCount = (int)ceil($objectCount / $itemsPerPage);

        // aggregate paginated request in one big array
        for ($i = 0; $i < $pageCount; $i++) {

            $page = $i + 1;
            $extendedQuery = 'itemsPerPage=' . $itemsPerPage . '&' . 'page=' . $page . '&' . $query;

            $paginatedCollection = $this->wimsConnector->requestData(sprintf('/api/%s', $objectKey), $extendedQuery)->getResponse();

            $result = array_merge($result, $paginatedCollection);

            if (count($paginatedCollection) !== $itemsPerPage) {

                break;
            }
        }

        return array_column($result, 'id');
    }


    private function sendNotifications(array $userMessages)
    {
        foreach ($userMessages as $email) {

            /** @var Users $user */
            $user = $email['user'];

            $this->mailer->sendEmail(
                $user->getEmail(),
                'Update notification from supersecretproject.com',
                'notification.html.twig',
                [
                    'data' => $email,
                ]
            );
        }
    }


    private function sortByTitleAsc(array $updatedEntities): array
    {
        usort($updatedEntities, function ($a, $b) {
            return $a['title'] <=> $b['title'];
        });

        return $updatedEntities;
    }

    private function getClassByName(string $name): ?string
    {
        $key = array_search($name, array_column(self::ENTITIES, 'name'));

        if ($key === false) {
            return null;
        }

        return self::ENTITIES[$key]['class'];
    }
}