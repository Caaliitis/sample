<?php

namespace App\Doctrine;

use App\Entity\OrderStatus;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;


class SetOrderStatusSubscriber implements EventSubscriber
{

    public function getSubscribedEvents(): array
    {
        return ['prePersist'];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $order = $args->getEntity();

        if (!$order instanceof OrderStatusInterface) {
            return;
        }

        if (null !== $order->getStatus()) {
            return;
        }

        $entityManager = $args->getObjectManager();
        $status = $entityManager->getRepository(OrderStatus::class)->find(1);
        $order->setStatus($status);

    }

}
