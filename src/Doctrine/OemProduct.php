<?php

namespace App\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Products;
use App\Entity\Users;
use App\Repository\TypesRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

class OemProduct implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $security;

    private $vaporflyTypeId;

    private $repository;

    public function __construct(Security $security, TypesRepository $repository, int $vaporflyTypeId)
    {
        $this->security = $security;
        $this->vaporflyTypeId = $vaporflyTypeId;
        $this->repository = $repository;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null): void
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = []): void
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (Products::class !== $resourceClass) {
            return;
        }


        $user = $this->security->getUser();

        if ($user instanceof Users && !in_array('ROLE_SHOP', $this->security->getUser()->getRoles())) {

            return;
        }

        $tradingType = $this->repository->find($this->vaporflyTypeId);

        $rootAlias = $queryBuilder->getRootAliases()[0];

        $queryBuilder->andWhere(sprintf(":tradingType MEMBER OF %s.types", $rootAlias));
        $queryBuilder->setParameter('tradingType', $tradingType);

    }
}