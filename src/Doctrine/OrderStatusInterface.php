<?php


namespace App\Doctrine;


use App\Entity\OrderStatus;

interface OrderStatusInterface
{
    public function getStatus();

    public function setStatus(?OrderStatus $status);
}