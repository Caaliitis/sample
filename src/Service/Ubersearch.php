<?php


namespace App\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use \Psr\Container\ContainerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;

use Doctrine\ORM\Query\Expr\Join;

/**
 * Class Ubersearch
 * @package App\Ubersearch
 *
 * @property ContainerInterface $container
 * @property EntityManager $em
 */
class Ubersearch
{

	protected $container;
	protected $em;

	/**
	 * Parses the quoted text in the search string
	 *
	 * @param $head
	 * @param array $tail
	 * @return array
	 */
	private static function walkQuote($head, array &$tail): array
	{
		if ($head === '"') return [];

		if (strpos($head, '"')) {
			return [substr($head, 0, strpos($head, '"'))];
		}

		$result = [$head];

		return array_merge($result, self::walkQuote(array_shift($tail), $tail));
	}

	/**
	 * Tokenizes the query string
	 *
	 * @param string $text
	 * @return string
	 */
	public static function parseSearchString(string $text): string
	{

		//replace single quotes to double in case some1 entered single quote
		$text = str_replace('\'', '"', $text);

		//tokenize the string
		$tokens = explode(' ', $text);

		//remove empty values (double spaces)
		foreach ($tokens as $i => $token) {
			if ($token === '') {
				unset($tokens[$i]);
			}
		}

		$tokens = array_values($tokens);

		$search = [];

		while (count($tokens) > 0) {

			$token = array_shift($tokens);

			//start quote parsing
			if (strpos($token, '"') !== false) {
				//remove quote from token otherwise walkQuote ends on first step
				$token = str_replace('"', '', $token);
				$search[] = '(' . implode(' <-> ', self::walkQuote($token, $tokens)) . ')';
			} else if (strpos($token, '-') === 0) {

				//if token is just "-" skip it
				if ($token === '-') {
					$token = array_shift($tokens);
				}

				$search[] = ltrim('-', $token);
			} else {
				$search[] = $token;
			}
		}

		return implode(' & ', $search);
	}

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->em = $this->container->get('doctrine.orm.entity_manager');
	}

	/**
	 * Returns the relation name between classes
	 *
	 * @param $class
	 * @param $relatedClass
	 * @return int|string
	 * @throws \Exception
	 */
	private function getRelationName($class, $relatedClass)
	{
		$metaData = $this->em->getClassMetadata($class);

		foreach ($metaData->associationMappings as $key => $relation) {
			if ($relation['targetEntity'] === $relatedClass) {
				return $key;
			}
		}

		throw new \Exception('Relation between ' . $class . ' and ' . $relatedClass . ' not found');
	}

	/**
	 * Combines queries
	 *
	 * @param array $queryBuilders
	 * @return mixed
	 */
	private function unionQueryBuilders(array $queryBuilders): string
	{
		//all named params are being removed from the query so we have to get them back
		//Doctrine breaks aliases, so i relace sclr_2 with name_2
		return str_replace(['?', 'sclr_2'], [':q', 'name_2'], '(' . implode(') UNION ALL (', array_map(function (QueryBuilder $q) {
				return $q->getQuery()->getSQL();
			}, $queryBuilders)) . ')');
	}

	/**
	 * Builds where for the inner queries
	 *
	 * @param array $fields
	 * @return string
	 */
	private function makeWhere(array $fields): string
	{
		return implode(' OR ', array_map(function ($val) {
			return $val = 'TS_MATCH_OP(u.' . $val . ', to_tsquery(:q)) = true';
		}, $fields));
	}


	private function parseLabel(array $label, string $alias = 'label'): string
	{
		//DQL CONCAT() fails if there is only 1 member
		if (count($label) == 1) {
			return $label[0] . ' as ' . $alias;
		}

		//Doctrine replaces concat with ||, which breaks if one member is null
		return 'CONCAT(' . implode(",', ',", array_map(function ($item) {
				return "COALESCE($item, '')";
			}, $label)) . ') as ' . $alias;
	}

	/**
	 * Main search func
	 *
	 * @param string $q
	 * @param array $entities
	 * @param int|null $limit
	 * @param int|null $offset
	 * @param int $extendedFieldsSearch
	 * @param int $depth
	 * @return array
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function unionSearch(string $q, array $entities, ?int $limit = null, ?int $offset = null, int $extendedFieldsSearch = 0, int $depth = 0)
	{
		$config = $this->container->getParameter('ubersearch');

		$sql = [];
		$sql_count = [];
		$searchedEntities = [];

		foreach (array_intersect_key($config, array_flip($entities)) as $entity => $params) {

			$entity = ucfirst($entity);
			$searchedEntities[] = $entity;

			$tokenizedClass = 'App\Entity\Ubersearch\\' . $entity;

			if ($entity == 'Contacts') {
				$where = 'TS_MATCH_OP(TS_CONCAT_OP(' . implode(',', array_map(function ($item) {
						return 'u.' . $item;
					}, $params['fields'])) . '), to_tsquery(:q)) = true';
			} else if ($extendedFieldsSearch) {
				$where = $this->makeWhere($params['fields']);
			} else {
				$where = $this->makeWhere(['name']);
			}

			//actually aliases are being ignored by DQL
			$qb = $this->em->createQueryBuilder();

			$relationTable = [];

			foreach ($params['label'] as &$l) {
				if (strpos($l, '.')) {
					list($relation, $column) = explode('.', $l);

					if (!isset($relationTable[$relation])) {
						$relationTable[$relation] = 'r' . count($relationTable);
					}

					$l = $relationTable[$relation] . '.' . $column;
				}
			}

			$sql[] = $qb
				->select([
					"'$entity' as entity",
					'c.id as entity_id',
					$this->parseLabel(array_map(function ($item) {
						return strpos($item, '.') ? $item : 'c.' . $item;
					}, $params['label']), 'label')
				])
				->from($params['class'], 'c')
				->innerJoin('c.ubersearchIndex', 'u');

			foreach ($relationTable as $relation => $alias) {
				$qb->innerJoin('c.' . $relation, $alias);
			}

			$qb->where($where);

			$sql_count[] = $this->em->createQueryBuilder()
				->select(['count(u.id) as qty'])
				->from($tokenizedClass, 'u')
				->where($where);

			if ($depth > 0 && isset($params['related']) && count($params['related'])) {

				$subquery = $this->em->createQueryBuilder()
					->select([
						'c.id',
					])
					->from($params['class'], 'c')
					->innerJoin('c.ubersearchIndex', 'u')
					->where($where)->getDQL();

				foreach ($params['related'] as $relatedClass) {

					$entity2 = explode('\\', $relatedClass);
					$entity2 = end($entity2);
					$searchedEntities[] = $entity2;

					$query = $this->em->createQueryBuilder();

					$sql[] = $query->select([
						"'$entity2' as entity",
						'r.id as entity_id',
						'r.name as label'
					])
						->from($relatedClass, 'r')
						->innerJoin('r.' . $this->getRelationName($relatedClass, $params['class']), 'q')
						->andWhere($query->expr()->in('q.id', $subquery));

					$sql_count[] = $this->em->createQueryBuilder()
						->select(['count(r.id) as qty'])
						->from($relatedClass, 'r')
						->innerJoin('r.' . strtolower($entity), 'q')
						->andWhere($query->expr()->in('q.id', $subquery));
				}
			}
		}

		//for some reasons doctrine renames fields. deep inside name_2 is nanmed sclr_2
		$sql = 'SELECT DISTINCT ON (sclr_0, id_1) sclr_0, id_1, name_2 FROM (' . $this->unionQueryBuilders($sql) . ') s ';

		//can't be only offset
		if ($limit !== null && $offset !== null) {
			$sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;
		} else if ($limit !== null) {
			$sql .= ' LIMIT ' . $limit;
		}

		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('sclr_0', 'entity');
		$rsm->addScalarResult('id_1', 'id');
		$rsm->addScalarResult('name_2', 'label');

		$query = $this->em->createNativeQuery($sql, $rsm);
		$query->setParameters(['q' => $q]);

		$sql_count = "SELECT sum(sclr_0) AS qty FROM (" . $this->unionQueryBuilders($sql_count) . ") as a";

		$rsm_count = new ResultSetMapping();
		$rsm_count->addScalarResult('qty', 'total', 'integer');

		$query_count = $this->em->createNativeQuery($sql_count, $rsm_count);
		$query_count->setParameters(['q' => $q]);

		return [
			'sql' => $sql,
			'searchedEntities' => $searchedEntities,
			'depth' => $depth,
			'items' => $query->getResult(),
			'meta' => [
				'total' => $query_count->getSingleScalarResult()
			]
		];
	}
}
