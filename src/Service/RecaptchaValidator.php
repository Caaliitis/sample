<?php


namespace App\Service;


use App\Entity\AccessLog;
use App\Entity\Users;
use App\Mailer\MailGunMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RecaptchaValidator
{
    private const HOUR_THRESHOLD = 50;
    private const DAY_THRESHOLD = 300;

    /** @var HttpClientInterface */
    private $httpClient;
    /** @var MailGunMailer */
    private $mailer;
    /** Security */
    private $security;
    /** @var AccessLog */
    private $log;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        HttpClientInterface $httpClient,
        MailGunMailer $mailer,
        Security $security,
        EntityManagerInterface $entityManager
    )
    {
        $this->httpClient = $httpClient;
        $this->mailer = $mailer;
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    /**
     * @return Users
     */
    private function getAuthenticatedUser(): ?Users
    {
        /** @var Users $authenticatedUser */
        $authenticatedUser = $this->security->getUser();

        return $authenticatedUser;
    }

    /**
     * @param Request $request
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function validateAndLog(Request $request)
    {
        $authenticatedUser = $this->getAuthenticatedUser();

        /** Create log and set some params from request */
        $this->log = new AccessLog();
        $this->log->setUser($authenticatedUser);
        $this->log->setHost($request->getHost());
        $this->log->setIp($request->getClientIp());
        $this->log->setMethod($request->getMethod());
        $this->log->setPath($request->getPathInfo());
        $this->log->setQueryString($request->getQueryString());
        $this->log->setPayload($request->getContent());

        /** On login save username only to keep plain password out of database */
        if ($request->getPathInfo() === '/login_check') {
            $payload = json_decode($request->getContent(), true);
            $userName = 'unknown';
            if (array_key_exists('username', $payload)) {
                $userName = $payload['username'];
            }
            $this->log->setPayload($userName);
        }

        $this->validateRecaptcha($request);

        $this->saveLog();
    }

    /**
     * @param Request $request
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function validateAndLogOnRegistration(Request $request)
    {
        /** Create log and set some params from request */
        $this->log = new AccessLog();
        $this->log->setHost($request->getHost());
        $this->log->setIp($request->getClientIp());
        $this->log->setMethod($request->getMethod());
        $this->log->setPath($request->getPathInfo());
        $this->log->setQueryString($request->getQueryString());
        $this->log->setPayload($request->getContent());
        $this->saveLog();

        /** Validate recaptcha and update log */
        $this->validateRecaptcha($request);
        $this->saveLog();
    }

    /**
     * Will notify admin on 50th, 100th etc count of requests
     */
    public function notifyIfCountLimitReached()
    {
        $requestCounts = $this->getUserRequestCounts();

        if (($requestCounts['hour'] >= self::HOUR_THRESHOLD && $requestCounts['hour'] % self::HOUR_THRESHOLD === 0) ||
            ($requestCounts['day'] >= self::DAY_THRESHOLD && $requestCounts['day'] % self::DAY_THRESHOLD === 0)
        ) {
            $this->sendRequestCountNotification($requestCounts);
        }
    }

    /**
     * @param Request $request
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function validateRecaptcha(Request $request)
    {
        /** Skip if disabled */
        if ($_ENV['RECAPTCHA_ENABLED'] === 'false') {
            $this->log->setError('RECAPTCHA IS DISABLED IN CONFIG');
            return;
        }

        /** Ensure that recaptcha param is present */
        if (!array_key_exists('recaptcha', $request->headers->all()) ||
            empty($request->headers->get('recaptcha'))
        ) {
            $this->log->setError('RECAPTCHA IS MISSING');
            $this->saveLog();
            throw new BadRequestHttpException("Recaptcha param is missing");
        }

        /** This param can have value that helps to skip recaptcha protection. Is used for scraper bot on dev server */
        if ($request->headers->get('recaptcha') === $_ENV['RECAPTCHA_WHITELIST_TOKEN']) {
            $this->log->setError('FRIENDLY BOT DETECTED');
            return;
        }

        /** Validate token via Google API request */
        $startTime = microtime(true);
        $response = $this->httpClient->request(
            'POST',
            $_ENV['RECAPTCHA_API_URL'],
            [
                'query' => [
                    'secret' => $_ENV['RECAPTCHA_SECRET_KEY'],
                    'response' => $request->headers->get('recaptcha'),
                    'remoteip' => $request->getClientIp(),
                ]
            ]
        );
        $time = microtime(true) - $startTime;

        if ($response) {
            $response = json_decode($response->getContent(), true);
            $response['time'] = $time;
            $this->log->setRecaptchaResponse($response);

            if (array_key_exists('success', $response) &&
                array_key_exists('score', $response)
            ) {
                /** Notify about cases with score < 0.8 */
                if ($response['success'] === true && $response['score'] < 0.8) {
                    $this->sendScoreNotification($response['score']);
                }

                /** Handle human case */
                if ($response['success'] === true && $response['score'] >= 0.5) {
                    return;
                }
            }
        }

        /** Handle bot case */
        $this->log->setError('BOT DETECTED');
        $this->saveLog();
        throw new BadRequestHttpException("Go home, nasty bot.");
    }

    private function saveLog()
    {
        $this->entityManager->persist($this->log);
        $this->entityManager->flush();
    }


    private function getUserRequestCounts(): array
    {
        $authenticatedUser = $this->getAuthenticatedUser();

        if ($authenticatedUser === null) {
            return ['hour' => 0, 'day' => 0];
        }

        return [
            'hour' => $this->entityManager->getRepository(AccessLog::class)->getRequestCountByUser($authenticatedUser, 1),
            'day' => $this->entityManager->getRepository(AccessLog::class)->getRequestCountByUser($authenticatedUser, 24),
        ];
    }

    /**
     * @param array $requestCounts
     */
    private function sendRequestCountNotification(array $requestCounts)
    {
        $authenticatedUser = $this->getAuthenticatedUser();

        $this->mailer->sendEmail(
            $_ENV['ADMIN_EMAIL'],
            'User "' . $authenticatedUser->getUsername() .
            '"; Last hour request count: ' . $requestCounts['hour'] .
            '; Last 24 hours request count: ' . $requestCounts['day'],
            'recaptcha-notification.html.twig'
        );
    }

    /**
     * @param float $score
     */
    private function sendScoreNotification(float $score)
    {
        $authenticatedUser = $this->getAuthenticatedUser();

        /**
         * always send notification for non-authenticated users
         */
        if ($authenticatedUser === null) {
            $this->mailer->sendEmail(
                $_ENV['ADMIN_EMAIL'],
                'Recaptcha score is ' . $score . ' for non-authenticated user. See access_log.id ' .
                $this->log->getId() . ' for details',
                'recaptcha-notification.html.twig'
            );

            return;
        }

        /**
         * send 1 notification per user per day for authenticated users
         */
        $notificationCountLastDay = $this->entityManager->getRepository(AccessLog::class)
            ->getNotificationCountLastDayByUser($authenticatedUser);

        if ($notificationCountLastDay > 0) {
            return;
        }

        $this->log->setRecaptchaNotification(true);
        $this->mailer->sendEmail(
            $_ENV['ADMIN_EMAIL'],
            'Recaptcha score is ' . $score . ' for user "' . $authenticatedUser->getUsername() . '"',
            'recaptcha-notification.html.twig'
        );
    }
}
