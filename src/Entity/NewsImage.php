<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use App\Controller\CreateNewsMediaObject;

/**
 * @ORM\Table(schema="api")
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={
 *         "groups"={"media_object_read"}
 *     },
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreateNewsMediaObject::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                "parameters" = {
 *                      {
 *                          "name" = "news_id",
 *                          "in" = "query",
 *                          "description" = "News ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "intro",
 *                          "in" = "query",
 *                          "description" = "Intro Image",
 *                          "type" : "boolean"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get"
 *     },
 *     itemOperations={
 *         "get",
 *          "delete"={
 *              "normalization_context"={"groups"={"get-news", "date"}}
 *          }
 *     }
 * )
 * @Vich\Uploadable()
 * @ORM\Entity(repositoryClass="App\Repository\NewsImageRepository")
 * @ApiFilter(SearchFilter::class, properties={
 *     "news.id":"exact",
 * })
 */
class NewsImage
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 * @Groups({"media_object_read", "get-news","get-news-item"})
	 */
	private $id;

	/**
	 * @var string|null
	 *
	 * @ApiProperty(iri="http://schema.org/contentUrl")
	 * @Groups({"media_object_read", "get-news","get-news-item"})
	 */
	public $contentUrl;

	/**
	 * @var File|null
	 *
	 * @Assert\NotNull(groups={"media_object_create"})
	 * @Vich\UploadableField(mapping="wims_news_image", fileNameProperty="filePath")
	 */
	public $file;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(nullable=true)
	 */
	public $filePath;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\News", inversedBy="images")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $news;

	/**
	 * @ORM\Column(type="boolean", options={"default" = false})
     * @Groups({"media_object_read", "get-news","get-news-item"})
	 */
	private $intro = false;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getNews(): ?News
	{
		return $this->news;
	}

	public function setNews( ?News $news ): self
	{
		$this->news = $news;

		return $this;
	}

	public function getIntro(): ?bool
	{
		return $this->intro;
	}

	public function setIntro( bool $intro ): self
	{
		$this->intro = $intro;

		return $this;
	}
}
