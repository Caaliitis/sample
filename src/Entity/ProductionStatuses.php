<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="production_statuses")
 * @ORM\Entity(repositoryClass="App\Repository\ProductionStatusesRepository")
 * @ApiResource(
 *      collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-status"}}
 *          },
 *         "get_names"={
 *              "normalization_context"={"groups"={"get-production-status-name"}},
 *               "path"="/production_statuses/name",
 *               "method"="GET"
 *         },
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-status"}}
 *          }
 *     }
 * )

 */
class ProductionStatuses
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-product", "get-products", "get-company", "get-status","get-production-status-name"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(
     *      message="The field name cannot be empty"
     * )
     * @Groups({"get-product", "get-products", "get-company", "get-status","get-production-status-name"})
     */
    private $name;

    /**
     * @ORM\Column(type="smallint",  options={"default" = "1"})
     * @Groups({"get-status"})
     */
    private $ordering;

	/**
	 * @ORM\Column(type="string", length=7, options={"default" = "#000000"})
	 * @Assert\NotBlank(
	 *      message="The field name cannot be empty"
	 * )
	 */
	private $color;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function setOrdering(int $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }

	public function getColor()
	{
		return $this->color;
	}

	public function setColor($color)
	{
		$this->color = $color;
	}
}
