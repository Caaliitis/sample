<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="patent", name="classification")
 * @ORM\Entity(repositoryClass="App\Repository\PatentClassificationRepository")
 */
class PatentClassification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get-patent"})
     */
    private $sequence;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patent"})
     */
    private $office;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patent"})
     */
    private $scheme;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patent"})
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"get-patent"})
     */
    private $section;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"get-patent"})
     */
    private $class;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"get-patent"})
     */
    private $subclass;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"get-patent"})
     */
    private $mainGroup;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"get-patent"})
     */
    private $subgroup;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"get-patent"})
     */
    private $classificationValue;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"get-patent"})
     */
    private $generatingOffice;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="classifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getOffice(): ?string
    {
        return $this->office;
    }

    public function setOffice(?string $office): self
    {
        $this->office = $office;

        return $this;
    }

    public function getScheme(): ?string
    {
        return $this->scheme;
    }

    public function setScheme(?string $scheme): self
    {
        $this->scheme = $scheme;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(?string $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(?string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getSubclass(): ?string
    {
        return $this->subclass;
    }

    public function setSubclass(?string $subclass): self
    {
        $this->subclass = $subclass;

        return $this;
    }

    public function getMainGroup(): ?string
    {
        return $this->mainGroup;
    }

    public function setMainGroup(?string $mainGroup): self
    {
        $this->mainGroup = $mainGroup;

        return $this;
    }

    public function getSubgroup(): ?string
    {
        return $this->subgroup;
    }

    public function setSubgroup(?string $subgroup): self
    {
        $this->subgroup = $subgroup;

        return $this;
    }

    public function getClassificationValue(): ?string
    {
        return $this->classificationValue;
    }

    public function setClassificationValue(?string $classificationValue): self
    {
        $this->classificationValue = $classificationValue;

        return $this;
    }

    public function getGeneratingOffice(): ?string
    {
        return $this->generatingOffice;
    }

    public function setGeneratingOffice(?string $generatingOffice): self
    {
        $this->generatingOffice = $generatingOffice;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }
}
