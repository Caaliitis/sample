<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="patent", name="abstract")
 * @ORM\Entity(repositoryClass="App\Repository\PatentAbstractRepository")
 */
class PatentAbstract
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups({"get-patent"})
     */
    private $lang;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get-patent"})
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="abstracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }
}
