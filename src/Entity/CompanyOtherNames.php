<?php

namespace App\Entity;

use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="api", name="company_other_names")
 * @ORM\Entity(repositoryClass="App\Repository\CompanyOtherNamesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CompanyOtherNames
{
	use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-company", "edit-company"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CompanyNameTypes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-company", "edit-company"})
     */
    private $nameType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies", inversedBy="otherNames")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameType(): ?CompanyNameTypes
    {
        return $this->nameType;
    }

    public function setNameType(?CompanyNameTypes $nameType): self
    {
        $this->nameType = $nameType;

        return $this;
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany(?Companies $company): self
    {
        $this->company = $company;

        return $this;
    }
}
