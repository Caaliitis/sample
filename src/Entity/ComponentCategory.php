<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ComponentCategoryRepository")
 * @ORM\Table(schema="components", name="category")
 * @ApiResource()
 */
class ComponentCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     * @Groups({"get-component", "get-components"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"get-component", "get-components"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentCategory")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Components", mappedBy="category")
     */
    private $components;

    public function __construct()
    {
        $this->components = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Components[]
     */
    public function getComponents(): Collection
    {
        return $this->components;
    }

    public function addComponent(Components $component): self
    {
        if (!$this->components->contains($component)) {
            $this->components[] = $component;
            $component->setCategory($this);
        }

        return $this;
    }

    public function removeComponent(Components $component): self
    {
        if ($this->components->contains($component)) {
            $this->components->removeElement($component);
            // set the owning side to null (unless already changed)
            if ($component->getCategory() === $this) {
                $component->setCategory(null);
            }
        }

        return $this;
    }
}
