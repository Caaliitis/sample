<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Table(schema="api", name="types")
 * @ORM\Entity(repositoryClass="App\Repository\TypesRepository")
 * @UniqueEntity("name", message="This type name is already exists. Specify unique type name.")
 * @ApiResource(
 *     collectionOperations={
 *     "gettypes"={
 *          "path"="/producttypes",
 *          "method"="GET",
 *          "normalization_context"={"groups"={"get-types"}}
 *      },
 *      "get"={
 *          "path"="/types",
 *          "method"="GET",
 *          "normalization_context"={"groups"={"get-types"}}
 *      },
 *         "get_names"={
 *              "normalization_context"={"groups"={"get-type-name"}},
 *               "path"="/types/name",
 *               "method"="GET"
 *         },
 *     },
 *     itemOperations={"get"={"method"="GET"}},
 *     attributes={"pagination_enabled"=false}
 * )
 * @ApiFilter(SearchFilter::class, properties={"modules":"exact"})
 * @ORM\HasLifecycleCallbacks()
 */
class Types
{

    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     *
     * @Groups({"get-types", "get-products", "get-product", "get-company", "get-contact", "media_object_read","get-type-name", "get-report"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"get-types", "get-products", "get-product", "get-company", "get-contact", "media_object_read","get-type-name", "get-report"})
     * @Assert\NotBlank(
     *     message="The type name cannot be empty"
     * )
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(name="svg", type="string", length=255, nullable=false, options={"default"=""})
     */
    private $svg;

    /**
     * @ORM\Column(name="order_field", type="integer", length=3)
     */
    private $orderField;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Modules", inversedBy="types")
     * @ORM\JoinTable(schema="api", name="modules_has_types",
     *      joinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="module_id", referencedColumnName="id")}
     *
     * )
     */
    private $modules;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Products", mappedBy="types")
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductImages", mappedBy="types")
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Features", inversedBy="types")
     * @ORM\JoinTable(schema="api", name="types_has_features",
     *      joinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="feature_id", referencedColumnName="id")}
     * )
     */
    private $features;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $onlyForStaff;

    /**
     * @ORM\ManyToMany(targetEntity=Companies::class, mappedBy="types")
     */
    private $companies;

    public function __construct()
    {
        $this->modules = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->companies = new ArrayCollection();
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Collection|Modules[]
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @return Collection|Products[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Modules $module
     * @return $this
     */
    public function setModules(Modules $module): Types
    {
        $this->modules[] = $module;
        if ($module->getTypes()->contains($this)) {
            return $this;
        }

        return $this;
    }

    public function removeModules(Modules $module)
    {
        $this->modules->removeElement($module);
    }

    /**
     * @return Collection|Features[]
     */
    public function getFeatures()
    {
        return $this->features;
    }

    public function setFeatures(Features $feature): Types
    {
        $this->features[] = $feature;
        if ($feature->getTypes()->contains($this)) {
            return $this;
        }

        return $this;
    }

    public function removeFeatures(Features $feature)
    {
        $this->features->removeElement($feature);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSvg()
    {
        return $this->svg;
    }

    public function setSvg($svg)
    {
        $this->svg = $svg;
    }

    public function getOrderField()
    {
        return $this->orderField;
    }

    public function setOrderField($orderField)
    {
        $this->orderField = $orderField;
    }

    public function getOnlyForStaff(): ?bool
    {
        return $this->onlyForStaff;
    }

    public function setOnlyForStaff(?bool $onlyForStaff): self
    {
        $this->onlyForStaff = $onlyForStaff;

        return $this;
    }

    /**
     * @return Collection|Companies[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(Companies $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->addType($this);
        }

        return $this;
    }

    public function removeCompany(Companies $company): self
    {
        if ($this->companies->removeElement($company)) {
            $company->removeType($this);
        }

        return $this;
    }

}
