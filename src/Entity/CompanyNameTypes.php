<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="api", name="company_name_types")
 * @ORM\Entity(repositoryClass="App\Repository\CompanyNameTypesRepository")
 * @ApiResource(
 *     collectionOperations={
 *         "get"={}
 *     },
 *     itemOperations={
 *         "get"={}
 *     }
 * )
 */
class CompanyNameTypes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-company"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"get-company"})
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
