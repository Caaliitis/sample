<?php

namespace App\Entity;

use App\File\FileBase;
use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use App\Validator\Constraints\FileMimeType;
use App\Validator\Constraints\FileSize;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @ORM\Table(schema="api", name="company_files")
 * @ORM\Entity(repositoryClass="App\Repository\CompanyFilesRepository")
 */

class CompanyFiles
{
	use TimestampableCreateEntity, UserCreateEntity;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Companies", inversedBy="file")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $company;

	/**
	 * @ORM\Column(name="file", type="string", length=255, nullable=true)
	 * @SerializedName("name")
	 */
	private $file;

	/**
	 * @Assert\NotBlank(
	 *     message="The file name cannot be empty"
	 * )
	 * @Assert\Type("string")
	 * @ORM\Column(name="original_name", type="string", length=255, nullable=true)
	 */
	private $originalName;

	/**
	 * @ORM\Column(name="file_size", type="integer", nullable=true)
	 */
	private $fileSize;

	/**
	 * @ORM\Column(name="mime_type", type="string", length=255, nullable=true)
	 */
	private $mimeType;

	/**
	 * @Assert\Type("string")
	 * @ORM\Column(type="string", length=255)
	 */
	private $description;

	/**
	 * @ORM\Column(name="state", type="integer", nullable=true)
	 */
	private $state;


	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\FileGroups")
	 * @ORM\JoinTable(schema="api", name="company_files_has_file_groups",
	 *     joinColumns={@ORM\JoinColumn(name="company_file_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="file_group_id", referencedColumnName="id")}
	 * )
	 * @SerializedName("fileGroup")
	 */
	private $fileGroup;

	private $uploadsBaseUrl;


	public function __construct()
	{
		$this->fileGroup = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCompany(): ?Companies
	{
		return $this->company;
	}

	public function setCompany(Companies $company): self
	{
		$this->company = $company;

		return $this;
	}

	public function getOriginalName()
	{
		return $this->originalName;
	}


	public function setOriginalName($originalName)
	{
		$this->originalName = $originalName;
	}

	public function getFile(): ?string
	{
		return $this->file;
	}

	public function setFile(?string $file): self
	{
		$this->file = $file;

		return $this;
	}

	public function getFileSize(): ?int
	{
		return $this->fileSize;
	}

	public function setFileSize(int $fileSize): self
	{
		$this->fileSize = $fileSize;

		return $this;
	}

	public function getMimeType(): ?string
	{
		return $this->mimeType;
	}

	public function setMimeType(?string $mimeType): self
	{
		$this->mimeType = $mimeType;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(string $description): self
	{
		$this->description = $description;

		return $this;
	}

	public function getState()
	{
		return $this->state;
	}

	public function setState($state)
	{
		$this->state = $state;
	}

	/**
	 * @return Collection|FileGroups[]
	 */
	public function getFileGroup(): Collection
	{
		return $this->fileGroup;
	}

	public function addFileGroup(FileGroups $fileGroup): self
	{
		if (!$this->fileGroup->contains($fileGroup))
		{
			$this->fileGroup[] = $fileGroup;
		}

		return $this;
	}

	public function removeFileGroup(FileGroups $fileGroup): self
	{
		if ($this->fileGroup->contains($fileGroup))
		{
			$this->fileGroup->removeElement($fileGroup);
		}

		return $this;
	}

	public function getFilePath(): string
	{
		return '/files/companies/' . $this->getCompany()->getId() . '/files/';
	}


	public function getUploadsBaseUrl(): string
	{
		return $this->uploadsBaseUrl;
	}


	public function setUploadsBaseUrl(string $uploadsBaseUrl)
	{
		$this->uploadsBaseUrl = $uploadsBaseUrl;
	}
}
