<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Doctrine\OrderStatusInterface;
use App\Security\OwnerSecurity;
use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="product_orders")
 * @ORM\Entity(repositoryClass="App\Repository\ProductOrderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-order", "date"}},
 *              "openapi_context"={
 *                "summary"= "Retrieves the collection of ProductOrder resources. (Sample Order for WIMS)",
 *              }
 *          }
 *      },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-order", "date"}},
 *              "openapi_context"={
 *                "summary"= "Retrieves a ProductOrder resource. (Sample Order for WIMS)",
 *              }
 *          }
 *      }
 *)
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "comment", "product.name", "quantity", "status.ordering" }, arguments={"orderParameterName"="order"})
 */
class ProductOrder implements OwnerSecurity, OrderStatusInterface
{
	use UserCreateEntity, TimestampableCreateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-order"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Products")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-order"})
     */
    private $product;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\GreaterThan(value="0")
     * @Groups({"get-order"})
     */
    private $quantity;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $isSent = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-order"})
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderStatus")
     * @Groups({"get-order"})
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getIsSent(): ?bool
    {
        return $this->isSent;
    }

    public function setIsSent(bool $isSent): self
    {
        $this->isSent = $isSent;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getStatus(): ?OrderStatus
    {
        return $this->status;
    }

    public function setStatus(?OrderStatus $status): self
    {
        $this->status = $status;

        return $this;
    }
}
