<?php

namespace App\Entity\Report;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-reports", "date"}},
 *              "security"="is_granted('ROLE_STAFF')"
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-report"}},
 *              "normalization_context"={"groups"={"get-report", "date"}},
 *              "security"="is_granted('ROLE_STAFF')"
 *          },
 *         "post_report_section"={
 *             "route_name"="post_report_section",
 *             "path"="/api/reports/{id}/sections",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="POST",
 *             "output"=ReportSections::class,
 *             "normalization_context"={"groups"={"get-report"}},
 *             "openapi_context"={
 *                "summary"= "Creates a ReportSections resource for current report",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "application/json"={
 *                             "schema"={
 *                                 "properties"={
 *                                     "name"={
 *                                         "type"="string",
 *                                          "required" = "true"
 *                                     },
 *                                     "isHidden"={
 *                                         "type"="boolean",
 *                                          "required" = "false"
 *                                     },
 *                                      "ordering"={
 *                                         "type"="integer"
 *                                     },
 *                                     "type"={
 *                                         "type"="integer"
 *                                     },
 *                                 }
 *                             }
 *                         }
 *                     }
 *                  }
 *             }
 *          },
 *         "clone_report"={
 *             "route_name"="clone_report",
 *             "path"="/api/reports/{id}/clone",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="POST",
 *             "output"=Reports::class,
 *             "input"=false,
 *             "normalization_context"={"groups"={"get-report"}},
 *             "openapi_context"={
 *                "summary"= "Clone sceleton ( the report and the sections settings) of current report by ID. ",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  }
 *             }
 *          },
 *         "clean_report"={
 *             "route_name"="clean_report",
 *             "path"="/api/reports/{id}/sections/clean",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="POST",
 *             "output"=false,
 *             "input"=false,
 *             "read"=false,
 *             "normalization_context"={"groups"={"get-report"}},
 *             "openapi_context"={
 *                "summary"= "Clean all products inside report's sections. ",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  }
 *             }
 *          },
 *         "generate_report"={
 *             "route_name"="generate_report",
 *             "path"="/api/reports/{id}/generate",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="POST",
 *             "output"=false,
 *             "openapi_context"={
 *                "summary"= "Add the report in a queue for generation",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "application/json"={
 *                             "schema"={
 *                                 "properties"={
 *                                     "format"={
 *                                         "type"="string",
 *                                          "required"="true",
 *                                          "example"="pdf",
 *                                          "pattern"="^(pdf|pptx)$"
 *                                     },
 *                                     "step"={
 *                                         "type"="integer",
 *                                          "required"="false",
 *                                          "example"=100
 *                                     },
 *                                 }
 *                             }
 *                         }
 *                     }
 *                  }
 *             }
 *          },
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-report", "date"}},
 *              "security"="is_granted('ROLE_STAFF')"
 *          },
 *         "put_report_section"={
 *             "route_name"="put_report_section",
 *             "path"="/api/reports/{id}/sections/{section_id}",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="PUT",
 *             "output"=ReportSections::class,
 *             "normalization_context"={"groups"={"get-report"}},
 *             "openapi_context"={
 *                "summary"= "Updates the ReportSections resource for current report",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "section_id",
 *                          "in" = "path",
 *                          "description" = "Section ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "application/json"={
 *                             "schema"={
 *                                 "properties"={
 *                                     "name"={
 *                                         "type"="string",
 *                                          "required" = "true"
 *                                     },
 *                                     "isHidden"={
 *                                         "type"="boolean",
 *                                          "required" = "false"
 *                                     },
 *                                      "ordering"={
 *                                         "type"="integer"
 *                                     },
 *                                     "type"={
 *                                         "type"="integer"
 *                                     },
 *                                 }
 *                             }
 *                         }
 *                     }
 *                  }
 *             }
 *          },
 *         "put"={
 *              "denormalization_context"={"groups"={"edit-report"}},
 *              "normalization_context"={"groups"={"get-report", "date"}},
 *               "security"="is_granted('ROLE_STAFF')"
 *          },
 *         "patch"={
 *              "denormalization_context"={"groups"={"edit-report"}},
 *              "normalization_context"={"groups"={"get-report", "date"}},
 *              "security"="is_granted('ROLE_STAFF')"
 *          },
 *         "delete"={
 *              "security"="is_granted('ROLE_STAFF')"
 *          },
 *         "delete_report_section"={
 *             "route_name"="delete_report_section",
 *             "path"="/api/reports/{id}/sections/{section_id}",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="delete",
 *             "output"=false,
 *             "openapi_context"={
 *                "summary"= "Removes the ReportSections resource for current report",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "section_id",
 *                          "in" = "path",
 *                          "description" = "Section ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  }
 *              }
 *          },
 *         "delete_section_products"={
 *             "route_name"="delete_section_products",
 *             "path"="/api/reports/{id}/sections/{section_id}/delete",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="PUT",
 *             "output"=ReportSections::class,
 *             "normalization_context"={"groups"={"get-section-products"}},
 *             "openapi_context"={
 *                "summary"= "Delete products from ReportSections",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "section_id",
 *                          "in" = "path",
 *                          "description" = "Section ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "application/json"={
 *                             "schema"={
 *                                 "properties"={
 *                                     "products"={
 *                                         "type"="array",
 *                                          "required" = "true",
 *                                          "items"={
 *                                              "type" = "integer"
 *                                          }
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                  }
 *                }
 *             },
 *         "post_section_products"={
 *             "route_name"="post_section_products",
 *             "path"="/api/reports/{id}/sections/{section_id}/add",
 *             "security"="is_granted('ROLE_STAFF')",
 *             "method"="PUT",
 *             "output"=ReportSections::class,
 *             "normalization_context"={"groups"={"get-section-products"}},
 *             "openapi_context"={
 *                "summary"= "Add products to ReportSections",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Report ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "section_id",
 *                          "in" = "path",
 *                          "description" = "Section ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "application/json"={
 *                             "schema"={
 *                                 "properties"={
 *                                     "products"={
 *                                         "type"="array",
 *                                          "required" = "true",
 *                                          "items"={
 *                                              "type" = "integer"
 *                                          }
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                  }
 *             }
 *          }
 *     }
 * )
 * @ORM\Table(schema="api_report", name="product_reports")
 * @ORM\Entity(repositoryClass="App\Repository\ReportsRepository")
 * @UniqueEntity("name", message="Report name must be unique")
 * @ORM\HasLifecycleCallbacks()
 */
class Reports
{

    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    const PRODUCT_ORDER_COLUMN = [
        1 => ["id" => 1, "name" => "productionStatus"],
        2 => ["id" => 2, "name" => "appeared"],
        3 => ["id" => 3, "name" => "createdAt"]
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get-reports", "get-report"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Assert\NotBlank(message="The name cannot be empty")
     * @Groups({"get-reports", "get-report", "add-report", "edit-report"})
     */
    private $name;

    /**
     * @ORM\Column(type="smallint", options={"default" = "1"})
     * @Groups({"get-reports", "get-report", "add-report", "edit-report"})
     */
    private $firstPage;

    /**
     * @ORM\Column(type="smallint", options={"default" = "1"})
     * @Groups({"get-reports", "get-report", "add-report", "edit-report"})
     * @Assert\NotNull()
     * @Assert\Range(
     *      min = 1,
     *      max = 3,
     *      notInRangeMessage = "You must set the next value for ordering: 1 by productionStatus, 2 by appeared  or 3 by createdAt",
     * )
     */
    private $productOrderColumn = 1;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     * @Groups({"get-reports", "get-report", "add-report", "edit-report"})
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $hideIcons;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     * @Groups({"get-reports", "get-report", "add-report", "edit-report"})
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $showReleasePeriod;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Report\ReportSections", mappedBy="report", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"ordering" = "ASC"})
     * @Groups({"get-report"})
     */
    private $sections;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     * @Groups({"get-reports", "get-report", "add-report", "edit-report"})
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $containsUniqueProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Report\GeneratedReportFiles", mappedBy="report", orphanRemoval=true)
     */
    private $generatedReportFiles;

    public function __construct()
    {
        $this->sections = new ArrayCollection();
        $this->generatedReportFiles = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }


    public function setId(?int $id)
    {
        $this->id = $id;
    }


    public function getName(): ?string
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getFirstPage(): ?int
    {
        return $this->firstPage;
    }


    public function setFirstPage(int $firstPage)
    {
        $this->firstPage = $firstPage;
    }


    public function getProductOrderColumn(): ?array
    {
        return self::PRODUCT_ORDER_COLUMN[$this->productOrderColumn];
    }


    public function setProductOrderColumn(int $productOrderColumn)
    {
        $this->productOrderColumn = $productOrderColumn;
    }


    public function getHideIcons(): ?bool
    {
        return $this->hideIcons;
    }


    public function setHideIcons(bool $hideIcons)
    {
        $this->hideIcons = $hideIcons;
    }


    public function getShowReleasePeriod(): ?bool
    {
        return $this->showReleasePeriod;
    }

    public function setShowReleasePeriod(bool $showReleasePeriod)
    {
        $this->showReleasePeriod = $showReleasePeriod;
    }

    /**
     * @return Collection|ReportSections[]
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    public function addSection(ReportSections $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setReport($this);
        }

        return $this;
    }

    public function addSections(Collection $sections): self
    {
        /** @var ReportSections $section */
        foreach ($sections as $section) {
            /** @var ReportSections $cloneSection */
            $newSection = new ReportSections();

            $newSection->setName($section->getName());
            $newSection->setIsHidden($section->getIsHidden());
            $newSection->setOrdering($section->getOrdering());
            $newSection->setType($section->getType());

            $this->addSection($newSection);

            $newSection->addProducts($section->getProducts());
        }

        return $this;
    }

    public function removeSection(ReportSections $section): self
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
            // set the owning side to null (unless already changed)
            if ($section->getReport() === $this) {
                $section->setReport(null);
            }
        }

        return $this;
    }

    public function cleanSections(): self
    {

        /** @var ReportSections $section */
        foreach ($this->sections as $section) {
            $section->removeProducts();
        }

        return $this;
    }

    public function getContainsUniqueProducts(): ?bool
    {
        return $this->containsUniqueProducts;
    }

    public function setContainsUniqueProducts(bool $containsUniqueProducts): self
    {
        $this->containsUniqueProducts = $containsUniqueProducts;

        return $this;
    }

    /**
     * @return Collection|generatedReportFiles[]
     */
    public function getGeneratedReportFiles(): Collection
    {
        return $this->generatedReportFiles;
    }

    public function addGeneratedReportFile(GeneratedReportFiles $generatedReportFile): self
    {
        if (!$this->generatedReportFiles->contains($generatedReportFile)) {
            $this->generatedReportFiles[] = $generatedReportFile;
            $generatedReportFile->setReport($this);
        }

        return $this;
    }

    public function removeGeneratedReportFile(GeneratedReportFiles $generatedReportFile): self
    {
        if ($this->generatedReportFiles->contains($generatedReportFile)) {
            $this->generatedReportFiles->removeElement($generatedReportFile);
            // set the owning side to null (unless already changed)
            if ($generatedReportFile->getReport() === $this) {
                $generatedReportFile->setReport(null);
            }
        }

        return $this;
    }

}
