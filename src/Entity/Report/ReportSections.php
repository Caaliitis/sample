<?php

namespace App\Entity\Report;
;
use App\Entity\Products;
use App\Entity\Types;
use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api_report", name="product_report_sections")
 * @ORM\Entity(repositoryClass="App\Repository\ReportSectionsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ReportSections
{
    use TimestampableCreateEntity, UserCreateEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get-report"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="The name cannot be empty")
     * @Groups({"get-report", "add-report", "edit-report"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     * @Groups({"get-report", "add-report", "edit-report"})
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $isHidden;

    /**
     * @ORM\Column(type="smallint", options={"default" = "1"})
     * @Groups({"get-report", "add-report", "edit-report"})
     * @Assert\PositiveOrZero()
     */
    private $ordering;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Types")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * @Groups({"get-report", "add-report", "edit-report"})
     * @MaxDepth(2)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Report\Reports", inversedBy="sections")
     * @ORM\JoinColumn(nullable=false)
     */
    private $report;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Products", inversedBy="sections", cascade={"persist"})
     * @ORM\JoinTable(schema="api_report", name="product_report_sections_has_products",
     *    joinColumns={@ORM\JoinColumn(name="section_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     * )
     * @MaxDepth(2)
     * @ORM\OrderBy({"productionStatus" = "ASC"})
     * @Groups({"get-section-products"})
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getName(): ?string
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getIsHidden(): ?bool
    {
        return $this->isHidden;
    }


    public function setIsHidden(bool $isHidden)
    {
        $this->isHidden = $isHidden;
    }


    public function getOrdering(): ?int
    {
        return $this->ordering;
    }


    public function setOrdering(int $ordering)
    {
        $this->ordering = $ordering;
    }


    public function getType(): ?Types
    {
        return $this->type;
    }


    public function setType(?Types $type)
    {
        $this->type = $type;
    }

    /**
     * @SerializedName("quantity")
     */
    public function getProductQuantity(): ?int
    {
        return count($this->products);
    }


    public function getProducts()
    {
        return $this->products;
    }

    public function getReport(): ?Reports
    {
        return $this->report;
    }

    public function setReport(?Reports $report): self
    {
        $this->report = $report;

        return $this;
    }

    public function addProduct(Products $product): self
    {

        if ($this->getReport()->getContainsUniqueProducts() === false) {
            $this->products[] = $product;

            return $this;

        }

        foreach ($this->getReport()->getSections() as $section) {
            if ($section->getProducts()->contains($product)) {
                return $this;
            }
        }

        $this->products[] = $product;

        return $this;
    }

    public function addProducts(Collection $products): self
    {
        foreach ($products as $product) {
            $this->addProduct($product);
        }

        return $this;
    }

    public function removeProduct(Products $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

    public function removeProducts(): self
    {
        foreach ($this->getProducts() as $product) {
            $this->removeProduct($product);
        }

        return $this;
    }

}
