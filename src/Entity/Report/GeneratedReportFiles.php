<?php

namespace App\Entity\Report;

use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api_report", name="generated_report_files")
 * @ORM\Entity(repositoryClass="App\Repository\GeneratedReportFilesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class GeneratedReportFiles
{
	use TimestampableCreateEntity, UserCreateEntity;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Report\Reports", inversedBy="generatedReportFiles")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $report;

	/**
	 * @ORM\Column(name="file", type="string", length=255, nullable=true)
	 */
	private $file;

	/**
	 * @ORM\Column(type="smallint", options={"default" = "100"})
	 * @Assert\GreaterThan(value="10")
     * @Assert\LessThanOrEqual(value="200")
	 */
	private $step = 100;

	/**
	 * @ORM\Column(type="smallint", options={"default" = "0"})
	 * @Assert\GreaterThanOrEqual(value="0")
	 */
	private $currentPage = 0;

	/**
	 * @ORM\Column(type="smallint", options={"default" = "0"})
	 * @Assert\GreaterThanOrEqual(value="0")
	 *
	 */
	private $pagesAmount = 0;

	/**
	 * @ORM\Column(type="boolean", options={"default" = "true"})
     * @Assert\Type("boolean")
	 */
	private $completed = false;

	/**
	 * @ORM\Column(type="string", length=4)
     * @Assert\ExpressionLanguageSyntax(
     *     allowedVariables={"pdf", "pptx"},
     *     message="Allowed to set only the next values: pdf, pptx"
     * )
	 */
	private $format;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getReport(): ?Reports
	{
		return $this->report;
	}

	public function setReport(?Reports $report): self
	{
		$this->report = $report;

		return $this;
	}

	public function getFilePath(): ?string
	{
		return '/reports/products/generated/' . $this->getReport()->getId() . '/';
	}

	public function getRelativeUrl(): ?string
	{
		return '/reports/products/generated/' . $this->getReport()->getId() . '/' . $this->getFile();
	}

	public function getFile(): ?string
	{
		return $this->file;
	}

	public function setFile(?string $file): self
	{
		$this->file = $file;

		return $this;
	}

	public function getStep(): ?int
	{
		return $this->step;
	}

	public function setStep(int $step): self
	{
		$this->step = $step;

		return $this;
	}

	public function getCurrentPage(): ?int
	{
		return $this->currentPage;
	}

	public function setCurrentPage(int $currentPage): self
	{
		$this->currentPage = $currentPage;

		return $this;
	}

	public function getPagesAmount(): ?int
	{
		return $this->pagesAmount;
	}

	public function setPagesAmount(int $pagesAmount): self
	{
		$this->pagesAmount = $pagesAmount;

		return $this;
	}

	public function getCompleted(): ?bool
	{
		return $this->completed;
	}

	public function setCompleted(bool $completed): self
	{
		$this->completed = $completed;

		return $this;
	}

	public function getFormat(): ?string
	{
		return $this->format;
	}

	public function setFormat(string $format): self
	{
		$this->format = $format;

		return $this;
	}
}
