<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="modules")
 * @ORM\Entity(repositoryClass="App\Repository\ModulesRepository")
 * @ApiResource(
 *      collectionOperations={"get"},
 *      itemOperations={"get"},
 * )
 */
class Modules
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 * @Assert\NotBlank(message="The module name cannot be empty")
	 * @Assert\Type("string")
	 */
	private $name;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\Types", mappedBy="modules")
	 */
	private $types;

	public function __construct()
	{
		$this->types = new ArrayCollection();
	}

	/**
	 * @return Collection|Types[]
	 */
	public function getTypes()
	{
		return $this->types;
	}

	public function setTypes( Types $type )
	{
		$this->types[] = $type;
		if ( ! $type->getModules()->contains( $this ) ) {
			$type->setModules( $this );
		}

		return $this;
	}

	public function removeTypes( Types $type )
	{
		$this->types->removeElement( $type );
		$type->removeModules( $this );
	}


	public function getId()
	{
		return $this->id;
	}


	public function setId( $id )
	{
		$this->id = $id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName( $name )
	{
		$this->name = $name;
	}

}
