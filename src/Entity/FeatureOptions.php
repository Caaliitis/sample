<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="feature_options")
 * @ORM\Entity(repositoryClass="App\Repository\FeatureOptionsRepository")
 * @UniqueEntity(
 *     fields={"features", "value"},
 *     message="This list already has such an option"
 *  )
 */
class FeatureOptions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Features", inversedBy="options")
	 * @ORM\JoinColumn(name="feature_id", referencedColumnName="id")
	 */
    private $features;

	/**
	 * @Assert\NotBlank(
	 *     message="List options cannot be empty"
	 * )
	 * @ORM\Column(type="string")
     * @Groups({"get-features", "get-feature"})
	 */
    private $value;

	public function getId()
	{
		return $this->id;
	}

	public function getFeatures(): Features
	{
		return $this->features;
	}


	public function setFeatures(Features $features)
	{
		$this->features = $features;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function setValue($value)
	{
		$this->value = $value;
	}
}
