<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\File\ContentUri;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-trademarks", "date"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-trademark"}},
 *              "security"="is_granted('ROLE_BRAND_EDIT')"
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-trademark", "date"}}
 *          },
 *          "put"={
 *              "denormalization_context"={"groups"={"edit-trademark"}},
 *               "security"="is_granted('ROLE_BRAND_EDIT')"
 *          },
 *          "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *     }
 *
 * )
 * @ORM\Table(schema="trademark")
 * @ORM\Entity(repositoryClass="App\Repository\TrademarkRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiFilter(OrderFilter::class, properties={"id", "name", "holder", "company.name", "holderCountry"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact","name": "ipartial","company.id": "exact","company.name": "ipartial","source": "ipartial","applicationNumber": "ipartial","sourceId": "exact"})
 * @ApiFilter(PropertyFilter::class)
 */
class Trademark implements ContentUri
{
	use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-trademarks","get-trademark","get-company"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get-trademarks","get-trademark","add-trademark","get-company"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $origin;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $holderCountry;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $holder;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $applicationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $expirationDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $sourceId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $applicationNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TrademarkImage", mappedBy="trademark", orphanRemoval=true)
     * @Groups({"get-trademarks","get-trademark"})
     * @SerializedName("images")
     * @MaxDepth(1)
     */
    private $trademarkImages;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark"})
     */
    private $niceClass;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Companies", inversedBy="trademarks")
	 * @Groups({"get-trademarks","get-trademark"})
	 * @MaxDepth(1)
	 */
	private $company;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Ubersearch\Trademarks", mappedBy="trademark", orphanRemoval=true)
	 */
	private $ubersearchIndex;

	public function __construct()
    {
        $this->trademarkImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getHolderCountry(): ?string
    {
        return $this->holderCountry;
    }

    public function setHolderCountry(?string $holderCountry): self
    {
        $this->holderCountry = $holderCountry;

        return $this;
    }

    public function getHolder(): ?string
    {
        return $this->holder;
    }

    public function setHolder(?string $holder): self
    {
        $this->holder = $holder;

        return $this;
    }

    public function getApplicationDate(): ?\DateTimeInterface
    {
        return $this->applicationDate;
    }

    public function setApplicationDate(\DateTimeInterface $applicationDate): self
    {
        $this->applicationDate = $applicationDate;

        return $this;
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?\DateTimeInterface $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getSourceId(): ?string
    {
        return $this->sourceId;
    }

    public function setSourceId(?string $sourceId): self
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    public function getApplicationNumber(): ?string
    {
        return $this->applicationNumber;
    }

    public function setApplicationNumber(?string $applicationNumber): self
    {
        $this->applicationNumber = $applicationNumber;

        return $this;
    }

    /**
     * @return Collection|TrademarkImage[]
     */
    public function getTrademarkImages(): Collection
    {
        return $this->trademarkImages;
    }

    public function addTrademarkImage(TrademarkImage $trademarkImage): self
    {
        if (!$this->trademarkImages->contains($trademarkImage)) {
            $this->trademarkImages[] = $trademarkImage;
            $trademarkImage->setTrademark($this);
        }

        return $this;
    }

    public function removeTrademarkImage(TrademarkImage $trademarkImage): self
    {
        if ($this->trademarkImages->contains($trademarkImage)) {
            $this->trademarkImages->removeElement($trademarkImage);
            // set the owning side to null (unless already changed)
            if ($trademarkImage->getTrademark() === $this) {
                $trademarkImage->setTrademark(null);
            }
        }

        return $this;
    }

    public function getNiceClass(): ?string
    {
        return $this->niceClass;
    }

    public function setNiceClass(?string $niceClass): self
    {
        $this->niceClass = $niceClass;

        return $this;
    }

	public function getCompany(): ?Companies
	{
		return $this->company;
	}

	public function setCompany(?Companies $company): self
	{
		$this->company = $company;

		return $this;
	}

    public function getContentUris(): ?Collection
    {
        return $this->trademarkImages;
    }
}
