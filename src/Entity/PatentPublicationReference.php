<?php

namespace App\Entity;

use App\Repository\PatentPublicationReferenceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="patent", name="publication_reference")
 * @ORM\Entity(repositoryClass=PatentPublicationReferenceRepository::class)
 */
class PatentPublicationReference
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @Groups({"get-patent"})
	 */
	private $docId;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"get-patent"})
     */
    private $documentIdType;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     * @Groups({"get-patent"})
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patent"})
     */
    private $docNumber;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"get-patent"})
     */
    private $kind;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-patent"})
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="publicationReferences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumentIdType(): ?string
    {
        return $this->documentIdType;
    }

    public function setDocumentIdType(?string $documentIdType): self
    {
        $this->documentIdType = $documentIdType;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDocNumber(): ?string
    {
        return $this->docNumber;
    }

    public function setDocNumber(?string $docNumber): self
    {
        $this->docNumber = $docNumber;

        return $this;
    }

    public function getKind(): ?string
    {
        return $this->kind;
    }

    public function setKind(?string $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }

	public function getDocId(): ?int
	{
		return $this->docId;
	}

	public function setDocId(?int $docId): self
	{
		$this->docId = $docId;

		return $this;
	}
}
