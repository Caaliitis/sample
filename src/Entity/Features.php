<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use http\Exception\RuntimeException;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Filter\SearchNotConditionFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Table(schema="api", name="features")
 * @ORM\Entity(repositoryClass="App\Repository\FeaturesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiFilter(SearchNotConditionFilter::class, properties={"id"})
 * @UniqueEntity("name", message="This field name is already exists. Specify unique field name.")
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-features", "date"}}
 *         },
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-feature", "get-product", "date"}}
 *          }
 *     },
 * )
 * @ApiFilter(OrderFilter::class, properties={"id", "ordering"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "id": "exact",
 *     "types.id": "exact",
 *  }
 * )
 */
class Features
{
    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    const TEXT_TYPE = 'text';
    const NUMBER_TYPE = 'number';
    const LIST_TYPE = 'list';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get-features", "get-feature"})
     */
    private $id;

    /**
     * @Assert\NotBlank(
     *     message="The field name cannot be empty"
     * )
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Groups({"get-features", "get-feature","get-product", "get-products"})
     */
    private $name;

    /**
     * @Assert\NotBlank(
     *     message="The field type cannot be empty"
     * )
     * @ORM\Column(type="string", length=10)
     * @Groups({"get-features", "get-feature","get-product", "get-products"})
     */
    private $fieldType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FeatureOptions", mappedBy="features", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"value": "ASC"})
     */
    private $options;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Types", mappedBy="features")
     */
    private $types;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductFeatureValues", mappedBy="feature")
     */
    private $productFeatureValue;


    /**
     * @ORM\Column(type="json", nullable=true)
     * @Groups({"get-features", "get-feature","get-product", "get-products"})
     */
    private $params;

    /**
     * @ORM\Column(type="integer", options={"default"="0"})
     */
    private $ordering;


    public function __construct()
    {
        $this->types = new ArrayCollection();
        $this->options = new ArrayCollection();
        $this->productFeatureValue = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getFieldType()
    {
        return $this->fieldType;
    }


    public function setFieldType($fieldType)
    {
        if (!in_array($fieldType, [self::TEXT_TYPE, self::NUMBER_TYPE, self::LIST_TYPE])) {


            throw new RuntimeException("Huh>");
        }

        $this->fieldType = $fieldType;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Collection|Types[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function setTypes(Types $type)
    {

        $this->types[] = $type;
        if ($type->getFeatures()->contains($this)) {
            return;
        }
        $type->setFeatures($this);

        return $this;
    }

    public function removeTypes(Types $type)
    {
        $this->types->removeElement($type);
        $type->removeFeatures($this);
    }

    /**
     * @return Collection|FeatureOptions
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(FeatureOptions $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
        }

        return $this;
    }

    public function removeOption(FeatureOptions $option): self
    {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
        }

        return $this;
    }


    public function getParams()
    {
        return $this->params;
    }


    public function setParams($params)
    {
        $this->params = $params;
    }


    public function getOrdering()
    {
        return $this->ordering;
    }


    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;
    }

    /**
     * @Groups({"get-features", "get-feature","get-product", "get-products"})
     */
    public function getValues()
    {
        $values = [];

        /** @var FeatureOptions $option */
        foreach ($this->options as $option) {
            $values[$option->getId()] = $option->getValue();
        }

        return $values;

    }
}
