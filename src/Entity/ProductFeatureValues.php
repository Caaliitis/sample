<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="product_feature_values")
 * @ORM\Entity(repositoryClass="App\Repository\ProductFeatureValuesRepository")
 * @UniqueEntity(
 *     fields={"product", "feature", "value"},
 *     message="Not unique data"
 * )
 */
class ProductFeatureValues
{

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;


	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="productFeatureValue")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
	 */
	private $product;


	/**
     * @Assert\NotNull()
     * @Groups({"get-product", "get-products", "product-write"})
	 * @ORM\ManyToOne(targetEntity="App\Entity\Features", inversedBy="productFeatureValue")
	 * @ORM\JoinColumn(name="feature_id", referencedColumnName="id", nullable=false)
	 * @ORM\OrderBy({"ordering" = "ASC"})
	 */
	private $feature;


	/**
     * @Assert\NotBlank()
     * @Groups({"get-product", "get-products", "product-write"})
	 * @ORM\Column(type="float")
	 */
	private $value;


	public function getId()
	{
		return $this->id;
	}

	public function getProduct()
	{
		return $this->product;
	}

	public function setProduct($product)
	{
		$this->product = $product;
	}

	public function getFeature()
	{
		return $this->feature;
	}

	public function setFeature($feature)
	{
		$this->feature = $feature;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function setValue($value)
	{
		$this->value = $value;
	}

	/**
	 * @SerializedName("id")
	 * @Groups({"get-product", "get-products"})
	 */
	public function getFeatureId(){
		return $this->getFeature()->getId();
	}


	/**
	 * @SerializedName("name")
	 * @Groups({"get-product", "get-products"})
	 */
	public function getFeatureName(){
		return $this->getFeature()->getName();
	}

	/**
	 * @SerializedName("value")
	 * @Groups({"get-product", "get-products"})
	 */
	public function getTextValue(): string
	{
		/** @var Features $feature */
		$feature = $this->getFeature();

		if (count($feature->getOptions()) !== 0)
		{
			/** @var FeatureOptions $option */
			foreach ($feature->getOptions() as $option)
			{
				if ($option->getId() === (int)$this->getValue())
				{
					return $option->getValue();
				}
			}

		}

		return $this->value;
	}

}
