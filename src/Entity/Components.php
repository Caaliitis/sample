<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use App\Controller\CountController;
use App\Filter\ProductsDateFilter;
use App\Model\TotalCount;
use App\Model\CountGraph;

/**
 * @ORM\Table(schema="components", name="elements")
 * @ORM\Entity(repositoryClass="App\Repository\ComponentsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     denormalizationContext={"groups"={"user", "user:write"}},
 *     collectionOperations={
 *         "get"={
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "normalization_context"={"groups"={"get-components", "date"}}
 *         },
 *         "post"={"security"="is_granted('ROLE_PRODUCTS_ADD')"},
 *         "get_count_graph"={
 *              "route_name"="components_get_count_graph",
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "read"=false,
 *              "method"="GET",
 *              "filters"={},
 *              "pagination_enabled"=false,
 *              "output"=CountGraph::class,
 *              "normalization_context"={"groups"={"get-component"}},
 *              "openapi_context"={
 *                "summary"="Contact Graph Data",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *         },
 *     },
 *     itemOperations={
 *         "get"={
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "normalization_context"={"groups"={"get-component", "date"}}
 *          },
 *         "components_total_count"={
 *              "path"="/components/total_items",
 *              "controller"=CountController::class,
 *              "method"="GET",
 *              "input"=false,
 *              "output"=TotalCount::class,
 *              "openapi_context"={
 *                "summary"="Total products count",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *          },
 *         "put"={"security"="is_granted('ROLE_PRODUCTS_EDIT')"},
 *         "patch"={"security"="is_granted('ROLE_PRODUCTS_EDIT')"},
 *         "delete"={"security"="is_granted('ROLE_PRODUCTS_EDIT')"},
 *     },
 * )
 * @ApiFilter(OrderFilter::class, properties={"id", "name", "company.name", "category.name", "marking", "productsAmount"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(ProductsDateFilter::class, properties={"createdAt"})
 * @ApiFilter(RangeFilter::class, properties={"productsAmount"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "name": "ipartial",
 *     "marking": "ipartial",
 *     "productWithComponents.product.id": "exact",
 *     "id": "exact"
 * })
 * @ApiFilter(PropertyFilter::class)
 */
class Components
{

    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     * @Groups({"get-component", "get-components", "get-product", "get-products"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-component", "get-components"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get-component", "get-components"})
     */
    private $marking;

    /**
     * @Groups({"get-component", "get-components"})
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Groups({"get-component", "get-components"})
     * @ORM\OneToMany(targetEntity="App\Entity\ProductsComponents", mappedBy="component", orphanRemoval=true)
     * @SerializedName("products")
     */
    private $productWithComponents;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentCategory", inversedBy="components")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-component", "get-components"})
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies", inversedBy="components")
     * @Groups({"get-component", "get-components"})
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     * @SerializedName("productsAmount")
     * @Groups({"get-component", "get-components"})
     */
    private $productsAmount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentPackage")
     */
    private $package;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentLocation")
     */
    private $location;


    public function __construct()
    {
        $this->productWithComponents = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getMarking(): ?string
    {
        return $this->marking;
    }

    public function setMarking(string $marking): self
    {
        $this->marking = $marking;

        return $this;
    }

    /**
     * @return Collection|ProductsComponents[]
     */
    public function getProductWithComponents(): Collection
    {
        return $this->productWithComponents;
    }

    public function addProductWithComponent(Products $product): self
    {
        if (!$this->productWithComponents->contains($product)) {
            $this->productWithComponents[] = $product;
            $product->addComponentInProduct($this);
        }

        return $this;
    }

    public function removeProductWithComponent(Products $product): self
    {
        if ($this->productWithComponents->contains($product)) {

            $this->productWithComponents->removeElement($product);
            $product->removeComponentInProduct($this);
        }

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }


    public function setQuantity(?int $quantity)
    {
        $this->quantity = $quantity;
    }

    public function getCategory(): ?ComponentCategory
    {
        return $this->category;
    }

    public function setCategory(?ComponentCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany(?Companies $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getProductsAmount(): int
    {
        return $this->productsAmount;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function setProductsAmount(): self
    {
        $this->productsAmount = $this->getProductWithComponents()->count();

        return $this;
    }

    public function getPackage(): ?ComponentPackage
    {
        return $this->package;
    }

    public function setPackage(?ComponentPackage $package): self
    {
        $this->package = $package;

        return $this;
    }

    public function getLocation(): ?ComponentLocation
    {
        return $this->location;
    }

    public function setLocation(?ComponentLocation $location): self
    {
        $this->location = $location;

        return $this;
    }
}
