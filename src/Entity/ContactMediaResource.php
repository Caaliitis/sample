<?php

namespace App\Entity;

use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="contact_media_resources")
 * @ORM\Entity(repositoryClass="App\Repository\ContactMediaResourceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContactMediaResource
{
	use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-contact"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaResourceTypes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-contact", "add-contact"})
     */
    private $mediaResourceType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contacts", inversedBy="contactMediaResources")
     * @ORM\JoinColumn(nullable=false)
     */
    private $contact;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Groups({"get-contact", "add-contact"})
     */
    private $link;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMediaResourceType(): ?MediaResourceTypes
    {
        return $this->mediaResourceType;
    }

    public function setMediaResourceType(?MediaResourceTypes $mediaResourceType): self
    {
        $this->mediaResourceType = $mediaResourceType;

        return $this;
    }

    public function getContact(): ?Contacts
    {
        return $this->contact;
    }

    public function setContact(?Contacts $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }
}
