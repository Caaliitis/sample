<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PatentClaimRepository;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(
 *     schema="patent", name="claim",
 *     uniqueConstraints={
 *        @UniqueConstraint(
 *            name="patent_claims_unique",
 *            columns={"lang", "patent_id"})
 *    }
 * )
 * @ORM\Entity(repositoryClass=PatentClaimRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-patent", "date"}}
 *          }
 *     },
 *     itemOperations={
 *          "get"
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "patent.id":"exact"
 * })
 */
class PatentClaim
{
	use TimestampableCreateEntity, TimestampableUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     * @Groups({"get-patent"})
     */
    private $lang;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get-patent"})
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="claims")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }

}
