<?php

namespace App\Entity;

use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="samples")
 * @ORM\Entity(repositoryClass="App\Repository\SamplesRepository")
 */
class Samples {
	use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="samples")
	 * @Assert\NotNull(message="The product must be set")
	 * @MaxDepth(2)
	 */
	private $product;

	/**
	 * @ORM\Column(type="smallint")
	 * @Assert\GreaterThan(value="0")
	 * @Assert\LessThan(value="100")
	 */
	private $shelving;

	/**
	 * @ORM\Column(type="smallint")
	 * @Assert\GreaterThan(value="0")
	 * @Assert\LessThan(value="100")
	 */
	private $cell;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $testing;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $note;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\SampleConditions")
	 * @Assert\NotNull(message="The condition must be set")
	 */
	private $condition;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Invoices", inversedBy="samples")
	 */
	private $invoice;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @Assert\GreaterThan(value="0", message="The quantity value must be greater than 0")
	 */
	private $quantity;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\SampleFiles", mappedBy="sample", orphanRemoval=true)
	 */
	private $files;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 * @SerializedName("withoutBox")
	 */
	private $withoutBox;

	public function __construct() {
		$this->files = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getProduct(): ?Products {
		return $this->product;
	}

	public function setProduct( ?Products $product ): self {
		$this->product = $product;

		return $this;
	}

	public function getShelving(): ?int {
		return $this->shelving;
	}

	public function setShelving( int $shelving ): self {
		$this->shelving = $shelving;

		return $this;
	}

	public function getCell(): ?int {
		return $this->cell;
	}

	public function setCell( int $cell ): self {
		$this->cell = $cell;

		return $this;
	}

	public function getTesting(): ?string {
		return $this->testing;
	}

	public function setTesting( ?string $testing ): self {
		$this->testing = $testing;

		return $this;
	}

	public function getNote(): ?string {
		return $this->note;
	}

	public function setNote( ?string $note ): self {
		$this->note = $note;

		return $this;
	}

	/**
	 * @SerializedName("name")
	 */
	public function getName(): ?string {
		return ( $this->getProduct() ) ? $this->getProduct()->getName() : $this->getNote();
	}

	/**
	 * @SerializedName("boundWithProduct")
	 *
	 */
	public function getBoundWithProduct(): bool {
		return ( $this->getProduct() ) ? true : false;
	}

	/**
	 * @SerializedName("barcode")
	 */
	public function getBarcode(): ?string {
		$shelvingNumber = ( $this->getShelving() < 10 ) ? '0' . $this->getShelving() : $this->getShelving();
		$cellNumber     = ( $this->getCell() < 10 ) ? '0' . $this->getCell() : $this->getCell();

		return $shelvingNumber . $cellNumber . 'u' . $this->getId();
	}

	public function getCondition(): ?SampleConditions {
		return $this->condition;
	}

	public function setCondition( ?SampleConditions $condition ): self {
		$this->condition = $condition;

		return $this;
	}

	public function getInvoice(): ?Invoices {
		return $this->invoice;
	}

	public function setInvoice( ?Invoices $invoice ): self {
		$this->invoice = $invoice;

		return $this;
	}

	public function getQuantity(): ?int {
		return $this->quantity;
	}

	public function setQuantity( ?int $quantity ): self {
		$this->quantity = $quantity;

		return $this;
	}

	/**
	 * @return Collection|SampleFiles[]
	 */
	public function getFiles(): Collection {
		return $this->files;
	}

	public function addFile( SampleFiles $file ): self {
		if ( ! $this->files->contains( $file ) ) {
			$this->files[] = $file;
			$file->setSample( $this );
		}

		return $this;
	}

	public function removeFile( SampleFiles $file ): self {
		if ( $this->files->contains( $file ) ) {
			$this->files->removeElement( $file );
			// set the owning side to null (unless already changed)
			if ( $file->getSample() === $this ) {
				$file->setSample( null );
			}
		}

		return $this;
	}

	public function getWithoutBox(): ?bool {
		return $this->withoutBox;
	}

	public function setWithoutBox( ?bool $withoutBox ): self {
		$this->withoutBox = $withoutBox;

		return $this;
	}
}
