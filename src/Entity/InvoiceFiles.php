<?php

namespace App\Entity;

use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="invoice_files")
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceFilesRepository")
 */
class InvoiceFiles
{
	use TimestampableCreateEntity, UserCreateEntity;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;


	/**
	 * @ORM\Column(name="file", type="string", length=255, nullable=true)
	 * @SerializedName("name")
	 */
	private $file;

	/**
	 * @Assert\NotBlank(
	 *     message="The file name cannot be empty"
	 * )
	 * @Assert\Type("string")
	 * @ORM\Column(name="original_name", type="string", length=255, nullable=true)
	 * @SerializedName("originalName")
	 */
	private $originalName;

	/**
	 * @ORM\Column(name="file_size", type="integer", nullable=true)
	 */
	private $fileSize;

	/**
	 * @ORM\Column(name="mime_type", type="string", length=255, nullable=true)
	 */
	private $mimeType;

	/**
	 * @Assert\Type("string")
	 * @ORM\Column(type="string", length=255)
	 */
	private $description;

	/**
	 * @ORM\Column(name="state", type="integer", nullable=true)
	 */
	private $state;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\FileGroups")
	 * @ORM\JoinColumn(name="file_group", referencedColumnName="id")
	 * @SerializedName("fileGroup")
	 */
	private $fileGroup;

	private $uploadsBaseUrl;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Invoices", inversedBy="files")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $invoice;

	public function getId(): ?int
	{
		return $this->id;
	}


	public function getOriginalName()
	{
		return $this->originalName;
	}


	public function setOriginalName($originalName)
	{
		$this->originalName = $originalName;
	}

	public function getFile(): ?string
	{
		return $this->file;
	}

	public function setFile(?string $file): self
	{
		$this->file = $file;

		return $this;
	}

	public function getFileSize(): ?int
	{
		return $this->fileSize;
	}

	public function setFileSize(int $fileSize): self
	{
		$this->fileSize = $fileSize;

		return $this;
	}

	public function getMimeType(): ?string
	{
		return $this->mimeType;
	}

	public function setMimeType(?string $mimeType): self
	{
		$this->mimeType = $mimeType;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(string $description): self
	{
		$this->description = $description;

		return $this;
	}

	public function getState()
	{
		return $this->state;
	}

	public function setState($state)
	{
		$this->state = $state;
	}

	public function getFileGroup()
	{
		return $this->fileGroup;
	}

	public function setFileGroup($fileGroup)
	{
		$this->fileGroup = $fileGroup;
	}

	public function getFilePath(): string
	{
		return '/files/invoices/' . $this->getInvoice()->getId() . '/files/';
	}


	public function getUploadsBaseUrl(): string
	{
		return $this->uploadsBaseUrl;
	}


	public function setUploadsBaseUrl(string $uploadsBaseUrl)
	{
		$this->uploadsBaseUrl = $uploadsBaseUrl;
	}

	public function getInvoice(): ?Invoices
	{
		return $this->invoice;
	}

	public function setInvoice(?Invoices $invoice): self
	{
		$this->invoice = $invoice;

		return $this;
	}
}
