<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="company_media_resources")
 * @ORM\Entity(repositoryClass="App\Repository\CompanyMediaResourcesRepository")
 * @ApiResource()
 * @ApiFilter(PropertyFilter::class)
 */
class CompanyMediaResources
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-company"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaResourceTypes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-company", "edit-company"})
     */
    private $mediaResourceType;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get-company", "edit-company"})
     * @Assert\NotBlank()
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies", inversedBy="mediaResources")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMediaResourceType(): ?MediaResourceTypes
    {
        return $this->mediaResourceType;
    }

    public function setMediaResourceType(?MediaResourceTypes $mediaResourceType): self
    {
        $this->mediaResourceType = $mediaResourceType;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany(?Companies $company): self
    {
        $this->company = $company;

        return $this;
    }

}
