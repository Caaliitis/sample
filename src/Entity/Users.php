<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Table(name="users",schema="api",indexes={@ORM\Index(name="idx_block", columns={"block"}), @ORM\Index(name="username", columns={"username"}), @ORM\Index(name="email", columns={"email"}), @ORM\Index(name="idx_name", columns={"name"})})
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN')"},
 *         "post_registration"={
 *              "route_name"="user_registration",
 *              "method"="POST"
 *          },
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *         "put"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email", message="The current email exists")
 * @UniqueEntity("username", message="The current username exists")
 */
class Users implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"get-comment","get-comments"})
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Groups({"get-comment","get-comments"})
     */
    private $name;

    /**
     * @ORM\Column(name="username", type="string", length=150, nullable=false, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=false, unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(name="password", type="string", length=100, nullable=false)
     */
    private $password;

    private $newPassword;

    /**
     * @ORM\Column(name="block", type="boolean", nullable=false)
     */
    private $block = false;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="`registerDate`", type="datetime", nullable=false)
     */
    private $registerdate;

    /**
     * @ORM\Column(name="`lastvisitDate`", type="datetime", nullable=true)
     */
    private $lastvisitdate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $visitDate;

    /**
     * @ORM\Column(name="`lastResetTime`", type="datetime", nullable=true, options={"comment"="Date of last password reset"})
     */
    private $lastresettime;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Usergroups", inversedBy="users")
     * @ORM\JoinTable(
     *     schema="api",
     *     name="user_usergroup_map",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *  )
     * @ApiSubresource(maxDepth=1)
     */
    private $groups;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiryDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $activeLicense;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(type="text", nullable=true, name="ui_settings")
     */
    private $uiSettings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductOrderLead", mappedBy="users")
     */
    private $productOrderLeads;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotificationFilters", mappedBy="users")
     */
    private $notificationFilters;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductOrder", mappedBy="user")
     */
    private $productOrders;

    public function getUiSettings(): ?string
    {
        return $this->uiSettings;
    }

    public function setUi(?string $uiSettings)
    {
        $this->uiSettings = $uiSettings;

        return $this;
    }

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->productOrderLeads = new ArrayCollection();
        $this->notificationFilters = new ArrayCollection();
        $this->productOrders = new ArrayCollection();
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }


    public function getPassword(): ?string
    {
        return $this->password;
    }


    public function getRoles()
    {
        $rolesArray = array();

        foreach ($this->groups as $role) {
            $rolesArray[] = $role->getTitle();
        }

        return $rolesArray;

    }

    public function eraseCredentials()
    {
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        [
            $this->id,
            $this->username,
            $this->password,
        ] = unserialize($serialized);
    }

    public function getSalt()
    {
        return null;
    }

    public function isBlock(): bool
    {
        return $this->block;
    }

    public function setBlock(bool $block)
    {
        $this->block = $block;
    }


    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Collection|Usergroups[]
     */
    public function getGroups()
    {
        return $this->groups;
    }


    public function getGroupsArray()
    {

        $result = [];
        $groups = $this->getGroups();

        foreach ($groups as $group) {
            $result[] = ['id' => $group->getId(), 'name' => $group->getTitle()];
        }

        return $result;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }


    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }


    public function getRegisterdate(): \DateTimeInterface
    {
        return $this->registerdate;
    }


    public function setRegisterdate(\DateTimeInterface $registerdate)
    {
        $this->registerdate = $registerdate;
    }


    public function getLastvisitdate(): ?\DateTimeInterface
    {
        return $this->lastvisitdate;
    }


    public function setLastvisitdate(\DateTimeInterface $lastvisitdate)
    {
        $this->lastvisitdate = $lastvisitdate;
    }


    public function getLastresettime(): ?\DateTimeInterface
    {
        return $this->lastresettime;
    }


    public function setLastresettime(\DateTimeInterface $lastresettime)
    {
        $this->lastresettime = $lastresettime;
    }


    public function getName(): ?string
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getNewPassword(): ?string
    {
        return $this->newPassword;
    }

    public function setNewPassword(string $newPassword)
    {
        $this->newPassword = $newPassword;
    }

    public function getNotExistGroup(?array $allGroups): ArrayCollection
    {

        $arrayWithNonExistGroups = new ArrayCollection();

        if (!$allGroups) {
            return $arrayWithNonExistGroups;
        }

        foreach ($allGroups as $group) {
            if (!$this->getGroups()->contains($group)) {
                $arrayWithNonExistGroups[] = $group;
            }
        }


        return $arrayWithNonExistGroups;
    }

    public function addGroup(Usergroups $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(Usergroups $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }

        return $this;
    }

    public function getExpiryDate(): ?\DateTimeInterface
    {
        return $this->expiryDate;
    }

    public function setExpiryDate(?\DateTimeInterface $expiryDate): self
    {
        $this->expiryDate = $expiryDate;

        return $this;
    }

    public function getActiveLicense(): ?string
    {
        return $this->activeLicense;
    }

    public function setActiveLicense(?string $activeLicense): self
    {
        $this->activeLicense = $activeLicense;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|ProductOrderLead[]
     */
    public function getProductOrderLeads(): Collection
    {
        return $this->productOrderLeads;
    }

    public function addProductOrderLead(ProductOrderLead $productOrderLead): self
    {
        if (!$this->productOrderLeads->contains($productOrderLead)) {
            $this->productOrderLeads[] = $productOrderLead;
            $productOrderLead->setUsers($this);
        }

        return $this;
    }

    public function removeProductOrderLead(ProductOrderLead $productOrderLead): self
    {
        if ($this->productOrderLeads->contains($productOrderLead)) {
            $this->productOrderLeads->removeElement($productOrderLead);
            // set the owning side to null (unless already changed)
            if ($productOrderLead->getUsers() === $this) {
                $productOrderLead->setUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NotificationFilters[]
     */
    public function getNotificationFilters(): Collection
    {
        return $this->notificationFilters;
    }

    public function addNotificationFilter(NotificationFilters $notificationFilter): self
    {
        if (!$this->notificationFilters->contains($notificationFilter)) {
            $this->notificationFilters[] = $notificationFilter;
            $notificationFilter->setUsers($this);
        }

        return $this;
    }

    public function removeNotificationFilter(NotificationFilters $notificationFilter): self
    {
        if ($this->notificationFilters->contains($notificationFilter)) {
            $this->notificationFilters->removeElement($notificationFilter);
            // set the owning side to null (unless already changed)
            if ($notificationFilter->getUsers() === $this) {
                $notificationFilter->setUsers(null);
            }
        }

        return $this;
    }

    public function getVisitDate(): ?\DateTimeInterface
    {
        return $this->visitDate;
    }

    public function setVisitDate(): self
    {
        $this->visitDate = new \DateTime();

        return $this;
    }

    /**
     * @return Collection|ProductOrder[]
     */
    public function getProductOrders(): Collection
    {
        return $this->productOrders;
    }

    public function addProductOrder(ProductOrder $productOrder): self
    {
        if (!$this->productOrders->contains($productOrder)) {
            $this->productOrders[] = $productOrder;
            $productOrder->setUser($this);
        }

        return $this;
    }

    public function removeProductOrder(ProductOrder $productOrder): self
    {
        if ($this->productOrders->contains($productOrder)) {
            $this->productOrders->removeElement($productOrder);
            // set the owning side to null (unless already changed)
            if ($productOrder->getUser() === $this) {
                $productOrder->setUser(null);
            }
        }

        return $this;
    }

}