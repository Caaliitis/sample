<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="patent", name="applicant")
 * @ORM\Entity(repositoryClass="App\Repository\PatentApplicantRepository")
 */
class PatentApplicant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get-patent"})
     */
    private $sequence;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patent"})
     */
    private $dataFormat;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-patent"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="applicants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getDataFormat(): ?string
    {
        return $this->dataFormat;
    }

    public function setDataFormat(?string $dataFormat): self
    {
        $this->dataFormat = $dataFormat;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }
}
