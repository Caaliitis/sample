<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="api", name="order_status")
 * @ApiResource(
 *     collectionOperations={
 *         "get"={}
 *     },
 *     itemOperations={
 *         "get"={}
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"id", "ordering"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "useInProductOrder": "exact",
 *     "useInAnalysisOrder": "exact"
 *  })
 * @ORM\Entity(repositoryClass="App\Repository\OrderStatusRepository")
 * @ApiFilter(PropertyFilter::class)
 */
class OrderStatus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-analysis","get-order","get-vaporfly-order"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"get-analysis", "get-order","get-vaporfly-order"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordering;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $useInProductOrder = false;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $useInAnalysisOrder = false;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $useInVaporflyOrder;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function setOrdering(int $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function getUseInProductOrder(): ?bool
    {
        return $this->useInProductOrder;
    }

    public function setUseInProductOrder(bool $useInProductOrder): self
    {
        $this->useInProductOrder = $useInProductOrder;

        return $this;
    }

    public function getUseInAnalysisOrder(): ?bool
    {
        return $this->useInAnalysisOrder;
    }

    public function setUseInAnalysisOrder(bool $useInAnalysisOrder): self
    {
        $this->useInAnalysisOrder = $useInAnalysisOrder;

        return $this;
    }

    public function getUseInVaporflyOrder(): ?bool
    {
        return $this->useInVaporflyOrder;
    }

    public function setUseInVaporflyOrder(bool $useInVaporflyOrder): self
    {
        $this->useInVaporflyOrder = $useInVaporflyOrder;

        return $this;
    }
}
