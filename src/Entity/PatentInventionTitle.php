<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="patent", name="invention_title")
 * @ORM\Entity(repositoryClass="App\Repository\PatentInventionTitleRepository")
 */
class PatentInventionTitle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     * @Groups({"get-patent","get-patents"})
     */
    private $lang;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get-patent","get-patents"})
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="inventionTitles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }
}
