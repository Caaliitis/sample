<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductsComponentsRepository")
 * @ORM\Table(schema="components", name="products_has_components")
 */
class ProductsComponents {

	/**
     * @Groups({"get-product", "get-products","user", "user:write","get-component", "get-components"})
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="smallint", nullable=true)
	 */
	private $quantity;

	/**
     * @Groups({"get-component", "get-components"})
	 * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="componentsInProduct")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $product;

	/**
     * @Groups({"get-product", "get-products","user", "user:write"})
	 * @ORM\ManyToOne(targetEntity="App\Entity\Components", inversedBy="productWithComponents")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $component;

	public function getId(): ?int {
		return $this->id;
	}

	public function getQuantity(): ?int {
		return $this->quantity;
	}

	public function setQuantity( ?int $quantity ): self {
		$this->quantity = $quantity;

		return $this;
	}

	public function getProduct(): ?Products {
		return $this->product;
	}

	public function setProduct( ?Products $product ): self {
		$this->product = $product;

		return $this;
	}

	public function getComponent(): ?Components {
		return $this->component;
	}

	public function setComponent( ?Components $component ): self {
		$this->component = $component;

		return $this;
	}
}
