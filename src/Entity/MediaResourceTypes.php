<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="api", name="media_resource_types")
 * @ORM\Entity(repositoryClass="App\Repository\MediaResourceTypesRepository")
 * @ApiResource()
 */
class MediaResourceTypes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-company", "edit-company","get-contact"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"get-company","get-contact"})
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
