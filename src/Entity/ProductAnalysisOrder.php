<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Doctrine\OrderStatusInterface;
use App\Security\OwnerSecurity;
use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-analysis", "date"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-analysis"}},
 *              "normalization_context"={"groups"={"get-analysis", "date"}}
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-analysis", "date"}},
 *              "security"="is_granted('ROLE_STAFF') or object.created == user"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProductAnalysisOrderRepository")
 * @ORM\Table(schema="api", name="product_analysis_order")
 * @ORM\HasLifecycleCallbacks()
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "product.name", "quantity", "status.ordering", "analysisType.name" }, arguments={"orderParameterName"="order"})
 */
class ProductAnalysisOrder implements OwnerSecurity, OrderStatusInterface
{
    use TimestampableCreateEntity, UserCreateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-analysis"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Products")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-analysis","add-analysis"})
     * @MaxDepth(1)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AnalysisType")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-analysis","add-analysis"})
     */
    private $analysisType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderStatus")
     * @Groups({"get-analysis"})
     */
    private $status;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getAnalysisType(): ?AnalysisType
    {
        return $this->analysisType;
    }

    public function setAnalysisType(?AnalysisType $analysisType): self
    {
        $this->analysisType = $analysisType;

        return $this;
    }

    public function getStatus(): ?OrderStatus
    {
        return $this->status;
    }

    public function setStatus(?OrderStatus $status): self
    {
        $this->status = $status;

        return $this;
    }
}
