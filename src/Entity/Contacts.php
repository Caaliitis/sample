<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use App\Controller\CountController;
use App\Filter\ProductsDateFilter;
use App\Model\TotalCount;
use App\Model\CountGraph;

/**
 * @ORM\Table(schema="api", name="contacts")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\ContactsRepository")
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"date","get-contacts"}}
 *          },
 *         "contacts_get_count_graph"={
 *              "route_name"="contacts_get_count_graph",
 *              "read"=false,
 *              "method"="GET",
 *              "filters"={},
 *              "pagination_enabled"=false,
 *              "output"=CountGraph::class,
 *              "normalization_context"={"groups"={"get-contact"}},
 *              "openapi_context"={
 *                "summary"="Contact Graph Data",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *         },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-contact"}},
 *              "normalization_context"={"groups"={"date","get-contact"}},
 *              "security"="is_granted('ROLE_CONTACT_EDIT')"
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"method"="GET","groups"={"get-contact", "date"}}
 *          },
 *         "put"={
 *              "security"="is_granted('ROLE_CONTACT_EDIT')",
 *              "normalization_context"={"groups"={"date","get-contact"}},
 *              "denormalization_context"={"groups"={"add-contact"}}
 *          },
 *         "patch"={
 *              "security"="is_granted('ROLE_CONTACT_EDIT')",
 *              "normalization_context"={"groups"={"date","get-contact"}},
 *              "denormalization_context"={"groups"={"add-contact"}}
 *          },
 *          "contacts_total_count"={
 *              "path"="/contacts/total_items",
 *              "controller"=CountController::class,
 *              "method"="GET",
 *              "input"=false,
 *              "output"=TotalCount::class,
 *              "openapi_context"={
 *                "summary"="Total contacts count",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *          },
 *     },
 * )
 * @ApiFilter(ProductsDateFilter::class, properties={"createdAt"})
 * @ApiFilter(OrderFilter::class, properties={"id", "name", "surname", "company.name"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "id": "exact",
 *     "name": "ipartial",
 *     "surname": "ipartial",
 *     "email": "ipartial",
 *     "work_phone": "ipartial",
 *     "private_phone": "ipartial",
 *     "company.id": "exact",
 *     "company.name": "ipartial",
 *     "position": "ipartial",
 * })
 * @ApiFilter(PropertyFilter::class)
 */
class Contacts
{

    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @Groups({"get-contact", "get-contacts"})
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"get-contact", "get-contacts", "add-contact"})
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Name cannot be empty")
     * @Assert\NotNull(message="Name cannot be empty")
     */
    private $name;

    /**
     * @Groups({"get-contact", "get-contacts", "add-contact"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="surname cannot be empty")
     */
    private $surname;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies")
     * @ORM\JoinColumn(name="company",referencedColumnName="id")
     * @Groups({"add-contact","get-contacts","get-contact"})
     * @MaxDepth(2)
     */
    private $company;

    /**
     * @Groups({"add-contact", "get-contact", "get-contacts"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $position;

    /**
     * @Groups({"add-contact", "get-contact", "get-contacts"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("positionOld")
     */
    private $positionOld;

    /**
     * @Groups({"add-contact", "get-contact", "get-contacts"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("positionOldTwo")
     */
    private $positionOldTwo;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Ubersearch\Contacts", mappedBy="contact", orphanRemoval=true)
     */
    private $ubersearchIndex;

    /**
     * @ORM\ManyToMany(targetEntity="Contacts", mappedBy="partners")
     */
    private $partnersWithMe;

    /**
     * @Groups({"add-contact", "get-contact"})
     * @ORM\ManyToMany(targetEntity="Contacts", inversedBy="partnersWithMe")
     * @MaxDepth(1)
     * @ORM\JoinTable(schema="api", name="partners",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="partner_user_id", referencedColumnName="id")}
     *      )
     */
    private $partners;

    /**
     * @ORM\ManyToMany(targetEntity="Contacts", mappedBy="social")
     */
    private $socialWithMe;

    /**
     * @Groups({"add-contact", "get-contact"})
     * @ORM\ManyToMany(targetEntity="Contacts", inversedBy="socialWithMe")
     * @MaxDepth(1)
     * @ORM\JoinTable(schema="api", name="social",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="social_user_id", referencedColumnName="id")}
     *      )
     */
    private $social;

    /**
     * @Groups({"add-contact", "get-contact"})
     * @ORM\OneToMany(targetEntity="App\Entity\ContactMediaResource", mappedBy="contact", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $contactMediaResources;

    /**
     * @Assert\Regex(pattern="/(M|F)/", match=true, message="Available only M or F values")
     * @Groups({"add-contact", "get-contact"})
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $sex;

    /**
     * @Groups({"add-contact", "get-contact"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @Groups({"add-contact", "get-contact"})
     * @ORM\ManyToMany(targetEntity="App\Entity\Types")
     * @ORM\JoinTable(schema="api", name="contacts_has_types",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")}
     * )
     */
    private $types;


    /**
     * @Groups({"add-contact", "get-contact"})
     * @ORM\ManyToMany(targetEntity="App\Entity\Countries")
     * @ORM\JoinTable(schema="api", name="contacts_has_countries",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")}
     * )
     */
    private $locations;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $originalName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $originalPosition;

    public function __construct()
    {
        $this->partners = new ArrayCollection();
        $this->partnersWithMe = new ArrayCollection();
        $this->social = new ArrayCollection();
        $this->socialWithMe = new ArrayCollection();
        $this->contactMediaResources = new ArrayCollection();
        $this->types = new ArrayCollection();
        $this->locations = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getName(): ?string
    {
        return $this->name;
    }


    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getSurname(): ?string
    {
        return $this->surname;
    }


    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }


    public function getCompany(): ?Companies
    {
        return $this->company;
    }


    public function setCompany(Companies $company): self
    {
        $this->company = $company;

        return $this;
    }


    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPartnersWithMe(): Collection
    {
        return $this->partnersWithMe;
    }

    public function getPartners(): Collection
    {
        return $this->partners;
    }


//	public function getAllPartners(): Collection {
//		$this->getPartnersWithMe()->map( function ( $partner ) {
//			if ( ! $this->partners->contains( $partner ) ) {
//				$this->partners[] = $partner;
//			}
//		} );
//
//		return $this->partners;
//	}

    public function addPartners(self $partner): self
    {
        if (!$this->partners->contains($partner) && !$partner->social->contains($this)) {
            $this->partners[] = $partner;
        }

        return $this;
    }

    public function removePartners(self $partner): self
    {
        if ($this->partners->contains($partner)) {
            $this->partners->removeElement($partner);
        }

        if ($partner->social->contains($this)) {
            $partner->social->removeElement($this);
        }

        return $this;
    }

    public function getSocialWithMe(): Collection
    {
        return $this->socialWithMe;
    }


    public function getSocial(): Collection
    {
        return $this->social;
    }


//	public function getAllSocial(): Collection {
//		$this->getSocialWithMe()->map( function ( $person ) {
//			if ( ! $this->social->contains( $person ) ) {
//				$this->social[] = $person;
//			}
//		} );
//
//		return $this->social;
//	}

    public function addSocial(self $socialRelation): self
    {
        if (!$this->social->contains($socialRelation) && !$socialRelation->social->contains($this)) {
            $this->social[] = $socialRelation;
        }

        return $this;
    }

    public function removeSocial(self $socialRelation): self
    {
        if ($this->social->contains($socialRelation)) {
            $this->social->removeElement($socialRelation);
        }

        if ($socialRelation->social->contains($this)) {
            $socialRelation->social->removeElement($this);
        }

        return $this;
    }


    public function getPositionOld(): ?string
    {
        return $this->positionOld;
    }


    public function getPositionOldTwo(): ?string
    {
        return $this->positionOldTwo;
    }


    public function setPositionOld(string $positionOld): self
    {
        $this->positionOld = $positionOld;

        return $this;
    }


    public function setPositionOldTwo(string $positionOldTwo): self
    {
        $this->positionOldTwo = $positionOldTwo;

        return $this;
    }

    /**
     * @return Collection|ContactMediaResource[]
     */
    public function getContactMediaResources(): Collection
    {
        return $this->contactMediaResources;
    }

    public function addContactMediaResource(ContactMediaResource $contactMediaResource): self
    {
        if (!$this->contactMediaResources->contains($contactMediaResource)) {
            $this->contactMediaResources[] = $contactMediaResource;
            $contactMediaResource->setContact($this);
        }

        return $this;
    }

    public function removeContactMediaResource(ContactMediaResource $contactMediaResource): self
    {
        if ($this->contactMediaResources->contains($contactMediaResource)) {
            $this->contactMediaResources->removeElement($contactMediaResource);
            // set the owning side to null (unless already changed)
            if ($contactMediaResource->getContact() === $this) {
                $contactMediaResource->setContact(null);
            }
        }

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(?string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return Collection|Countries[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Countries $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
        }

        return $this;
    }

    public function removeLocation(Countries $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
        }

        return $this;
    }

    /**
     * @return Collection|Types[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Types $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(Types $type): self
    {
        if ($this->types->contains($type)) {
            $this->types->removeElement($type);
        }

        return $this;
    }

    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    public function setOriginalName(?string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getOriginalPosition(): ?string
    {
        return $this->originalPosition;
    }

    public function setOriginalPosition(?string $originalPosition): self
    {
        $this->originalPosition = $originalPosition;

        return $this;
    }
}
