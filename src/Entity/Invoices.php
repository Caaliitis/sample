<?php

namespace App\Entity;

use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Table(schema="api", name="invoices")
 * @ORM\Entity(repositoryClass="App\Repository\InvoicesRepository")
 */
class Invoices
{
	use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Samples", mappedBy="invoice")
     */
    private $samples;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InvoiceFiles", mappedBy="invoice", orphanRemoval=true)
     */
    private $files;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies", inversedBy="invoices")
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TrackingNumbers", mappedBy="invoice", orphanRemoval=true, cascade={"persist"})
     * @SerializedName("trackingNumbers")
     */
    private $trackingNumbers;

    public function __construct()
    {
        $this->samples = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->trackingNumbers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Samples[]
     */
    public function getSamples(): Collection
    {
        return $this->samples;
    }

    public function addSample(Samples $sample): self
    {
        if (!$this->samples->contains($sample)) {
            $this->samples[] = $sample;
            $sample->setInvoice($this);
        }

        return $this;
    }

    public function removeSample(Samples $sample): self
    {
        if ($this->samples->contains($sample)) {
            $this->samples->removeElement($sample);
            // set the owning side to null (unless already changed)
            if ($sample->getInvoice() === $this) {
                $sample->setInvoice(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InvoiceFiles[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(InvoiceFiles $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setInvoice($this);
        }

        return $this;
    }

    public function removeFile(InvoiceFiles $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            // set the owning side to null (unless already changed)
            if ($file->getInvoice() === $this) {
                $file->setInvoice(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany(?Companies $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|TrackingNumbers[]
     */
    public function getTrackingNumbers(): Collection
    {
        return $this->trackingNumbers;
    }

    public function addTrackingNumber(TrackingNumbers $trackingNumber): self
    {
        if (!$this->trackingNumbers->contains($trackingNumber)) {
            $this->trackingNumbers[] = $trackingNumber;
            $trackingNumber->setInvoice($this);
        }

        return $this;
    }

    public function removeTrackingNumber(TrackingNumbers $trackingNumber): self
    {
        if ($this->trackingNumbers->contains($trackingNumber)) {
            $this->trackingNumbers->removeElement($trackingNumber);
            // set the owning side to null (unless already changed)
            if ($trackingNumber->getInvoice() === $this) {
                $trackingNumber->setInvoice(null);
            }
        }

        return $this;
    }
}
