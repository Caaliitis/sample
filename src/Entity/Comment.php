<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Security\OwnerSecurity;
use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @ORM\Table(schema="api", name="comment")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-comments", "date"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-comment"}},
 *              "normalization_context"={"groups"={"get-comment", "date"}}
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-comment", "date"}}
 *          },
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "id": "exact",
 *     "product.id":"exact",
 *     "company.id":"exact",
 *     "component.id":"exact",
 *     "trademark.id":"exact",
 *     "brand.id":"exact",
 *     "contact.id":"exact",
 *     "patent.id":"exact"
 * })
 */
class Comment implements OwnerSecurity
{

	use TimestampableCreateEntity, UserCreateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-comment","get-comments"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Groups({"get-comment","get-comments", "add-comment"})
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Products")
     * @Groups({"add-comment"})
     *
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies")
     * @Groups({"add-comment"})
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Components")
     * @Groups({"add-comment"})
     */
    private $component;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Brands")
     * @Groups({"add-comment"})
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trademark")
     * @Groups({"add-comment"})
     */
    private $trademark;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contacts")
     * @Groups({"add-comment"})
     */
    private $contact;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patent")
     * @Groups({"add-comment"})
     */
    private $patent;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany(?Companies $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getComponent(): ?Components
    {
        return $this->component;
    }

    public function setComponent(?Components $component): self
    {
        $this->component = $component;

        return $this;
    }

    public function getBrand(): ?Brands
    {
        return $this->brand;
    }

    public function setBrand(?Brands $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getTrademark(): ?Trademark
    {
        return $this->trademark;
    }

    public function setTrademark(?Trademark $trademark): self
    {
        $this->trademark = $trademark;

        return $this;
    }

	/**
	 * @SerializedName("created")
	 * @Groups({"get-comment","get-comments"})
	 */
    public function getCreatedUser(): Users
    {
    	return $this->getCreated();
    }

    public function getContact(): ?Contacts
    {
        return $this->contact;
    }

    public function setContact(?Contacts $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }


}
