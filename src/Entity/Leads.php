<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LeadsRepository;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="leads")
 * @ApiResource(
 *     denormalizationContext={"groups"={"lead-post"}},
 *     collectionOperations={
 *         "get" = {
 *              "security"="is_granted('ROLE_STAFF')",
 *              "normalization_context"={"groups"={"lead-get", "date"}}
 *     },
 *         "post"
 *     },
 *     itemOperations={
 *         "get" = {
 *              "security"="is_granted('ROLE_STAFF')",
 *              "normalization_context"={"groups"={"lead-get", "date"}}
 *          },
 *     },
 * )
 * @ORM\Entity(repositoryClass=LeadsRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Leads
{
    use TimestampableUpdateEntity, TimestampableCreateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"lead-post","lead-get"})
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Groups({"lead-post","lead-get"})
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $email;

    /**
     * @Groups({"lead-post","lead-get"})
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @Groups({"lead-post","lead-get"})
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $company;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }
}
