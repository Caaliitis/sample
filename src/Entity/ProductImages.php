<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\CreateProductMediaObject;
use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Table(schema="api", name="product_images")
 * @ORM\Entity(repositoryClass="App\Repository\ProductImagesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={
 *         "groups"={"media_object_read", "date"}
 *     },
 *     collectionOperations={
 *         "get"={
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "normalization_context"={"groups"={"media_object_read", "date"}}
 *         },
 *         "post"={
 *             "controller"=CreateProductMediaObject::class,
 *             "deserialize"=false,
 *             "security"="is_granted('ROLE_STAFF')",
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                "parameters" = {
 *                      {
 *                          "name" = "product_id",
 *                          "in" = "query",
 *                          "description" = "Product ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "is_default",
 *                          "in" = "query",
 *                          "description" = "Whether this image is default for product",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *     },
 *     itemOperations={
 *         "get"={
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "normalization_context"={"groups"={"media_object_read", "date"}}
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_STAFF')",
 *              "denormalization_context"={"groups"={"media_object_edit"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_STAFF')",
 *          }
 *     },
 * )
 * @Vich\Uploadable()
 * @ApiFilter(SearchFilter::class, properties={
 *     "product.id":"exact",
 * })
 */
class ProductImages
{

	use TimestampableCreateEntity, UserCreateEntity;

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
     * @Groups({"media_object_read"})
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="media")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $product;

	/**
     * @var string|null
     *
	 * @ORM\Column(name="source", type="string", length=255, nullable=true)
	 * @SerializedName("name")
	 */
    private $source;

    /**
     * @var File|null
     *
     * @Assert\NotNull(groups={"media_object_create"})
     * @Vich\UploadableField(mapping="wims_product_image", fileNameProperty="source")
     */
    public $file;

	/**
	 * @Groups({"get-products", "get-product","no-auth","media_object_read","media_object_edit"})
	 * @ORM\Column(name="default_img", type="boolean", nullable=false, options={"default" = false})
	 * @SerializedName("default")
	 */
	private $defaultImg = false;

	/**
	 * @ORM\Column(name="state", type="integer", options={"default" = 1})
	 */
	private $state;

	/**
	 * @ORM\Column(name="mime_type", type="string", length=100, nullable=true)
	 */
	private $mimeType;

	/**
	 * @ORM\Column(name="file_size", type="integer", nullable=true)
	 */
	private $fileSize = 0;

	/**
	 * @ORM\Column(name="description", type="string", length=100, nullable=true)
	 */
	private $description;


	private $thumbImage;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\Types", inversedBy="images")
     * @Groups({"media_object_read","media_object_edit"})
	 * @ORM\JoinTable(
	 *     schema="api",
	 *     name="product_images_has_types",
	 *     joinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")}
	 *  )
	 * @MaxDepth(2)
	 */
	private $types;

	private $absolutePath;

	private $cachePath;

	public function __construct()
	{
		$this->types = new ArrayCollection();
	}

	public function getId()
	{
		return $this->id;
	}

	public function getSource(): ?string
	{
		return $this->source;
	}

    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

	public function getDefaultImg(): bool
	{
		return $this->defaultImg;
	}


	public function setDefaultImg(?bool $defaultImg)
	{
		$this->defaultImg = $defaultImg;
	}

	public function getProduct(): Products
	{
		return $this->product;
	}


	public function setProduct(Products $product)
	{
		$this->product = $product;
	}


	public function getState(): int
	{
		return $this->state;
	}


	public function setState(int $state)
	{
		$this->state = $state;
	}


	public function getMimeType(): string
	{
		return $this->mimeType;
	}


	public function setMimeType(string $mimeType)
	{
		$this->mimeType = $mimeType;
	}


	public function getFileSize(): int
	{
		return $this->fileSize;
	}


	public function setFileSize(int $fileSize)
	{
		$this->fileSize = $fileSize;
	}


	public function getDescription(): ?string
	{
		return $this->description;
	}


	public function setDescription(?string $description)
	{
		$this->description = $description;
	}


	public function getFilePath(): string
	{
		return '/products/' . $this->getProduct()->getId() . '/images/';
	}


	public function getThumbImage(): ?string
	{
		return $this->thumbImage;
	}

	public function setThumbImage(?string $thumbImage)
	{
		$this->thumbImage = $thumbImage;
	}

	/**
	 * @return Collection|Types[]
	 */
	public function getTypes(): Collection
	{
		return $this->types;
	}

	public function addType(Types $type): self
	{
		//Also check whether product contains current type
		if (!$this->types->contains($type) && $this->getProduct()->getTypes()->contains($type))
		{
			$this->types[] = $type;
		}

		return $this;
	}

	public function removeType(Types $type): self
	{
		if ($this->types->contains($type))
		{
			$this->types->removeElement($type);
		}

		return $this;
	}

	/**
	 * @Groups({"get-products", "get-product","no-auth","media_object_read"})
	 */
	public function getUrl(): ?string
	{
		return $this->getAbsoluteUrl().$this->getFilePath() . $this->getSource();
	}

	/**
	 * @Groups({"get-products", "get-product","no-auth","media_object_read"})
	 */
	public function getThumbnailUrl(): ?string
	{
		return $this->getAbsoluteUrl().$this->getCachePath().$this->getFilePath() . $this->getSource();
	}


	public function setAbsoluteUrl(string $absoluteUrl)
	{
		$this->absolutePath = $absoluteUrl;
	}

	public function getAbsoluteUrl(): ?string
	{
		return $this->absolutePath;
	}

	public function setCachePath(string $cachePath)
	{
		$this->cachePath = $cachePath;
	}

	public function getCachePath(): ?string
	{
		return $this->cachePath;
	}

}
