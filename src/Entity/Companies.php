<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\CountController;
use App\Filter\ProductsDateFilter;
use App\Model\TotalCount;
use App\Model\CountGraph;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "security"="is_granted('ROLE_COMPANIES')",
 *              "normalization_context"={"groups"={"get-companies", "date"}}
 *         },
 *         "get_names"={
 *              "normalization_context"={"groups"={"get-company-name"}},
 *               "path"="/company/name",
 *               "method"="GET"
 *         },
 *         "get_count_graph"={
 *              "route_name"="companies_get_count_graph",
 *              "security"="is_granted('ROLE_COMPANIES')",
 *              "read"=false,
 *              "method"="GET",
 *              "filters"={},
 *              "pagination_enabled"=false,
 *              "output"=CountGraph::class,
 *              "normalization_context"={"groups"={"get-company"}},
 *              "openapi_context"={
 *                "summary"="Company Graph Data",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *         },
 *          "post"={
 *              "denormalization_context"={"groups"={"edit-company"}},
 *              "normalization_context"={"groups"={"date","get-company"}},
 *              "security"="is_granted('ROLE_COMPANIES_EDIT')"
 *          }
 *     },
 *     itemOperations={
 *          "companies_total_count"={
 *              "path"="/companies/total_items",
 *              "controller"=CountController::class,
 *              "method"="GET",
 *              "input"=false,
 *              "output"=TotalCount::class,
 *              "openapi_context"={
 *                "summary"="Total products count",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *          },
 *         "get"={
 *              "security"="is_granted('ROLE_COMPANIES')",
 *              "normalization_context"={"groups"={"get-company", "date"}}
 *          },
 *         "put"={
 *              "security"="is_granted('ROLE_COMPANIES_EDIT')",
 *              "denormalization_context"={"groups"={"edit-company"}},
 *              "normalization_context"={"groups"={"get-company", "date"}}
 *          },
 *         "patch"={
 *              "security"="is_granted('ROLE_COMPANIES_EDIT')",
 *              "denormalization_context"={"groups"={"edit-company"}},
 *              "normalization_context"={"groups"={"get-company", "date"}}
 *          },
 *     },
 *     attributes={"order"={"id": "ASC"}}
 * )
 * @ORM\Table(schema="api", name="companies")
 * @ORM\Entity(repositoryClass="App\Repository\CompaniesRepository")
 * @ApiFilter(OrderFilter::class, properties={"id", "name", "location.country", "brandsAmount", "productsAmount"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(ProductsDateFilter::class, properties={"createdAt"})
 * @ApiFilter(RangeFilter::class, properties={"brandsAmount", "productsAmount"})
 * @ApiFilter(PropertyFilter::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("name",  message="The company name must be unique")
 * @UniqueEntity("originalName",  message="The origin company name must be unique")
 * @ApiFilter(SearchFilter::class, properties={
 *     "id": "exact",
 *     "name": "ipartial",
 *     "registrationNumber": "ipartial",
 *     "location":"exact",
 *     "types":"exact",
 *     "category":"exact",
 *     "brands.id":"exact",
 *     "mediaResources.mediaResourceType.id":"exact",
 *     "mediaResources.link":"ipartial",
 *     "location.id":"exact",
 *     "brands.id":"exact",
 * })
 */
class Companies
{
    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-companies", "get-company","get-brand","get-brands","get-product", "get-products", "get-trademarks","get-trademark","get-component","get-components","get-contacts","get-contact","get-patents","get-patent","get-company-name"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The company name cannot be empty")
     * @Groups({"get-companies", "get-company","get-brand","get-brands","get-product", "get-products", "get-trademarks","get-trademark","get-component","get-components","get-contacts","get-contact","get-patents","get-patent", "edit-company","get-company-name"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true,name="original_name")
     * @Groups({"get-company", "edit-company"})
     */
    private $originalName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Countries")
     * @ORM\JoinColumn(name="location", referencedColumnName="id", nullable=true)
     * @Groups({"get-company", "get-companies", "edit-company"})
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-company", "edit-company"})
     */
    private $address;

    /**
     * @ORM\Column(name="foundation_year", type="integer", length=4, nullable=true)
     * @SerializedName("foundationYear")
     * @Assert\GreaterThan(value="1700")
     * @Assert\Length(4)
     * @Groups({"get-company", "edit-company"})
     */
    private $foundationYear;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompanyImages", mappedBy="company")
     */
    private $media;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompanyFiles", mappedBy="company")
     * @SerializedName("files")
     */
    private $file;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Products", mappedBy="company")
     */
    private $products;

    /**
     * @ORM\Column(name="registration_number", type="text", nullable=true)
     * @SerializedName("registrationNumber")
     * @Groups({"get-company", "edit-company"})
     */
    private $registrationNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompanyMediaResources", mappedBy="company", orphanRemoval=true, cascade={"persist"})
     * @SerializedName("mediaResources")
     * @Groups({"get-company", "edit-company"})
     * @MaxDepth(2)
     */
    private $mediaResources;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get-company", "edit-company"})
     */
    private $description;

    /**
     * @ORM\Column(name="switched_to_other_industry", type="boolean", options={"default" = false})
     * @SerializedName("switchedToOtherIndustry")
     * @Groups({"get-company", "edit-company"})
     * @Assert\NotNull()
     */
    private $switchedToOtherIndustry;

    /**
     * @ORM\Column(name="out_of_business", type="boolean", options={"default" = false})
     * @SerializedName("outOfBusiness")
     * @Groups({"get-company", "edit-company"})
     * @Assert\NotNull()
     */
    private $outOfBusiness;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CountryRegions")
     * @Groups({"get-company", "get-companies", "edit-company"})
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompanyOtherNames", mappedBy="company", orphanRemoval=true, cascade={"persist"})
     * @SerializedName("otherNames")
     * @Groups({"get-company", "edit-company"})
     */
    private $otherNames;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Ubersearch\Companies", mappedBy="company", orphanRemoval=true)
     */
    private $ubersearchIndex;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invoices", mappedBy="company")
     */
    private $invoices;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CompanyCategory", mappedBy="companies")
     * @Groups({"get-company", "edit-company"})
     * @MaxDepth(2)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Components", mappedBy="company")
     * @MaxDepth(1)
     */
    private $components;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Trademark", mappedBy="company")
     * @MaxDepth(1)
     * @Groups({"get-company"})
     */
    private $trademarks;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     * @SerializedName("brandsAmount")
     * @Groups({"get-company", "get-companies"})
     */
    private $brandsAmount;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     * @SerializedName("productsAmount")
     * @Groups({"get-company", "get-companies"})
     */
    private $productsAmount;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Patent",mappedBy="company")
     */
    private $patents;

    /**
     * @ORM\ManyToMany(targetEntity=Brands::class, inversedBy="companies")
     * @ORM\JoinTable(schema="api", name="companies_has_brands",
     *     joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="brand_id", referencedColumnName="id")}
     * )
     * @MaxDepth(2)
     * @ORM\OrderBy({"name" = "ASC"})
     * @Groups({"edit-company"})
     */
    private $brands;

    /**
     * @ORM\ManyToMany(targetEntity=Types::class, inversedBy="companies")
     * @ORM\JoinTable(schema="api", name="companies_has_types",
     * joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")}
     * )
     * @MaxDepth(2)
     * @Groups({"get-company", "edit-company"})
     */
    private $types;

    /**
     * @SerializedName("patentAmount")
     * @Groups({"get-companies", "get-company"})
     */
    public function getPatentAmount()
    {
        return count($this->patents);
    }

    /**
     * @SerializedName("products")
     * @MaxDepth(1)
     * @Groups({"get-company"})
     */
    public function getAllProducts()
    {
        $productsByBrandsCollection = $this->getProducts();

        /** @var Brands $brand */
        foreach ($this->getBrands() as $brand) {

            if (!method_exists($brand, 'getProducts') || null === $brand->getProducts()) {
                continue;
            }

            /** @var Products $product */
            foreach ($brand->getProducts() as $product) {

                if (null === $product) {
                    continue;
                }

                if (!$productsByBrandsCollection->contains($product)) {
                    $productsByBrandsCollection->add($product);
                }
            }
        }

        return $productsByBrandsCollection;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function __construct()
    {
        $this->types = new ArrayCollection();
        $this->media = new ArrayCollection();
        $this->file = new ArrayCollection();
        $this->mediaResources = new ArrayCollection();
        $this->otherNames = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->components = new ArrayCollection();
        $this->trademarks = new ArrayCollection();
        $this->brands = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }


    public function getFoundationYear()
    {
        return $this->foundationYear;
    }

    /**
     * @return Collection|CompanyImages[]
     */
    public function getMedia()
    {
        return $this->media;
    }

    public function setFoundationYear($foundationYear)
    {
        if (empty($foundationYear)) {
            $foundationYear = null;
        }

        $this->foundationYear = $foundationYear;

        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }


    /**
     * @SerializedName("markets")
     * @Groups({"get-company"})
     * @MaxDepth(6)
     */
    public function getMarkets()
    {
        $markets = new ArrayCollection();
        $values = [];
        $products = $this->getProducts();

        if (!$products) {
            return $markets;
        }

        /** @var Products $product */
        foreach ($products as $product) {
            $featureValues = $product->getFeatures();

            if ($featureValues) {
                foreach ($featureValues as $featureValue) {
                    if ($featureValue->getFeature()->getId() === 30) {
                        $values[] = $featureValue;
                    }
                }
            }
        }

        if (empty($values)) {
            return $markets;

        }

        $options = $values[0]->getFeature()->getOptions();

        $arr = [];

        foreach ($values as $value) {
            $arr[] = $value->getValue();
        }

        $unique_keys = array_unique($arr);

        foreach ($options as $option) {
            if (in_array($option->getId(), $unique_keys)) {
                $markets[] = $option;
            }
        }

        return $markets;

    }


    public function getOriginalName()
    {
        return $this->originalName;
    }


    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
    }


    /**
     * @return Collection|ProductFiles[]
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getRegistrationNumber(): ?string
    {
        return $this->registrationNumber;
    }

    public function setRegistrationNumber(?string $registrationNumber): self
    {
        $this->registrationNumber = ($registrationNumber) ?: null;

        return $this;
    }

    /**
     * @return Collection|CompanyMediaResources[]
     */
    public function getMediaResources(): Collection
    {
        return $this->mediaResources;
    }

    public function addMediaResource(CompanyMediaResources $mediaResource): self
    {
        if (!$this->mediaResources->contains($mediaResource)) {
            $this->mediaResources[] = $mediaResource;
            $mediaResource->setCompany($this);
        }

        return $this;
    }

    public function removeMediaResource(CompanyMediaResources $mediaResource): self
    {
        if ($this->mediaResources->contains($mediaResource)) {
            $this->mediaResources->removeElement($mediaResource);
            // set the owning side to null (unless already changed)
            if ($mediaResource->getCompany() === $this) {
                $mediaResource->setCompany(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSwitchedToOtherIndustry(): ?bool
    {
        return $this->switchedToOtherIndustry;
    }

    public function setSwitchedToOtherIndustry(bool $switchedToOtherIndustry): self
    {
        $this->switchedToOtherIndustry = $switchedToOtherIndustry;

        return $this;
    }

    public function getOutOfBusiness(): ?bool
    {
        return $this->outOfBusiness;
    }

    public function setOutOfBusiness(bool $outOfBusiness): self
    {
        $this->outOfBusiness = $outOfBusiness;

        return $this;
    }

    /**
     * @SerializedName("defaultImage")
     * @Groups({"get-companies", "get-company"})
     */
    public function getDefaultImage(): ?CompanyImages
    {
        foreach ($this->getMedia() as $key => $image) {
            if ($image->getDefaultImg()) {
                return $image;
            }
        }

        return null;
    }

    public function getProductsAmount(): int
    {
        return $this->getAllProducts()->count();
    }

    public function getRegion(): ?CountryRegions
    {
        return $this->region;
    }

    public function setRegion(?CountryRegions $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection|CompanyOtherNames[]
     */
    public function getOtherNames(): Collection
    {
        return $this->otherNames;
    }

    public function addOtherName(CompanyOtherNames $otherName): self
    {
        if (!$this->otherNames->contains($otherName)) {
            $this->otherNames[] = $otherName;
            $otherName->setCompany($this);
        }

        return $this;
    }

    public function removeOtherName(CompanyOtherNames $otherName): self
    {
        if ($this->otherNames->contains($otherName)) {
            $this->otherNames->removeElement($otherName);
            // set the owning side to null (unless already changed)
            if ($otherName->getCompany() === $this) {
                $otherName->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invoices[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoices $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setCompany($this);
        }

        return $this;
    }

    public function removeInvoice(Invoices $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getCompany() === $this) {
                $invoice->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyCategory[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(CompanyCategory $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->addCompany($this);
        }

        return $this;
    }

    public function removeCategory(CompanyCategory $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
            $category->removeCompany($this);
        }

        return $this;
    }

    /**
     * @return Collection|Components[]
     */
    public function getComponents(): Collection
    {
        return $this->components;
    }

    public function addComponent(Components $component): self
    {
        if (!$this->components->contains($component)) {
            $this->components[] = $component;
            $component->setCompany($this);
        }

        return $this;
    }

    public function removeComponent(Components $component): self
    {
        if ($this->components->contains($component)) {
            $this->components->removeElement($component);
            // set the owning side to null (unless already changed)
            if ($component->getCompany() === $this) {
                $component->setCompany(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Trademark[]
     */
    public function getTrademarks(): Collection
    {
        return $this->trademarks;
    }

    public function addTrademark(Trademark $trademark): self
    {
        if (!$this->trademarks->contains($trademark)) {
            $this->trademarks[] = $trademark;
            $trademark->setCompany($this);
        }

        return $this;
    }

    public function removeTrademark(Trademark $trademark): self
    {
        if ($this->trademarks->contains($trademark)) {
            $this->trademarks->removeElement($trademark);
            // set the owning side to null (unless already changed)
            if ($trademark->getCompany() === $this) {
                $trademark->setCompany(null);
            }
        }

        return $this;
    }

    public function getBrandsAmount(): ?int
    {
        return $this->brandsAmount;
    }

    /**
     * @return mixed
     */
    public function getPatents()
    {
        return $this->patents;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function setBrandsAmount(): self
    {
        $this->brandsAmount = $this->getAllBrands()->count();

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function setProductsAmount(): self
    {
        if ($this->getAllProducts() === null) {
            $this->productsAmount = 0;
            return $this;
        }

        $this->productsAmount = $this->getAllProducts()->count();;

        return $this;
    }

    /**
     * @return Collection|Brands[]
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }

    /**
     * @SerializedName("brands")
     * @MaxDepth(2)
     * @Groups({"get-company"})
     */
    public function getAllBrands()
    {
        $brandCollection = $this->brands;

        if ($this->getProducts() === null) {
            return $brandCollection;
        }

        /** @var Products $product */
        foreach ($this->getProducts() as $product) {

            if (null === $product->getBrand()) {
                continue;
            }

            if (!$brandCollection->contains($product->getBrand())) {
                $brandCollection->add($product->getBrand());
            }
        }

        return $brandCollection;
    }

    public function addBrand(Brands $brand): self
    {
        if (!$this->brands->contains($brand)) {
            $this->brands[] = $brand;
        }

        return $this;
    }

    public function removeBrand(Brands $brand): self
    {
        $this->brands->removeElement($brand);

        return $this;
    }

    /**
     * @return Collection|Types[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    /**
     * @SerializedName("productsTypes")
     * @Groups({"get-company"})
     * @MaxDepth(2)
     */
    public function getProductsTypes()
    {
        $productsTypes = new ArrayCollection();
        $products = $this->getProducts();

        if (!$products) {
            return $productsTypes;
        }

        foreach ($products as $product) {
            $types = $product->getTypes();

            if ($types) {
                foreach ($types as $type) {
                    if (!$productsTypes->contains($type)) {
                        $productsTypes[] = $type;
                    }

                }
            }

        }

        return $productsTypes;

    }

    public function addType(Types $type): self
    {
        $products = $this->getProducts();

        if ($products) {
            foreach ($products as $product) {
                $productTypes = $product->getTypes();
                if ($productTypes->contains($type)) {
                    return $this;
                }
            }
        }

        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(Types $type): self
    {
        $this->types->removeElement($type);

        return $this;
    }

}
