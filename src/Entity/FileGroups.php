<?php

namespace App\Entity;

use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="api", name="file_groups")
 * @ORM\Entity(repositoryClass="App\Repository\FileGroupsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="A file group name must be unique"
 * )
 */
class FileGroups
{

    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @Assert\NotBlank(message="The file name bundle can't be empty")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Modules")
     * @ORM\JoinTable(schema="api", name="modules_has_file_groups",
     *      joinColumns={@ORM\JoinColumn(name="file_group_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="module_id", referencedColumnName="id")}
     *
     * )
     */
    private $modules;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Usergroups")
     * @ORM\JoinTable(schema="api", name="file_groups_has_usergroups",
     *      joinColumns={@ORM\JoinColumn(name="file_group_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="usergroup_id", referencedColumnName="id")}
     * )
     */
    private $userGroups;

    public function __construct()
    {
        $this->modules = new ArrayCollection();
        $this->userGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return Collection|Modules[]
     */
    public function getModules()
    {
        return $this->modules;
    }

    public function setModules(Modules $module)
    {
        $this->modules[] = $module;

        return $this;
    }

    public function removeModules(Modules $module)
    {
        $this->modules->removeElement($module);
    }

    /**
     * @return Collection|Usergroups[]
     */
    public function getUserGroups()
    {
        return $this->userGroups;
    }
}
