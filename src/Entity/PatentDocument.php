<?php

namespace App\Entity;

use App\Repository\PatentDocumentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(schema="patent", name="document")
 * @ORM\Entity(repositoryClass=PatentDocumentRepository::class)
 */
class PatentDocument
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberOfPages;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $documentFormat;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $documentSection;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="documents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PatentContent", mappedBy="contentInfo")
     */
    private $contents;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pageCounter;

    public function __construct()
    {
        $this->contents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberOfPages(): ?int
    {
        return $this->numberOfPages;
    }

    public function setNumberOfPages(?int $numberOfPages): self
    {
        $this->numberOfPages = $numberOfPages;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getDocumentFormat(): ?string
    {
        return $this->documentFormat;
    }

    public function setDocumentFormat(?string $documentFormat): self
    {
        $this->documentFormat = $documentFormat;

        return $this;
    }

    public function getDocumentSection(): ?string
    {
        return $this->documentSection;
    }

    public function setDocumentSection(?string $documentSection): self
    {
        $this->documentSection = $documentSection;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }

    /**
     * @return Collection|PatentContent[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(PatentContent $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setContentInfo($this);
        }

        return $this;
    }

    public function removeContent(PatentContent $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            // set the owning side to null (unless already changed)
            if ($content->getContentInfo() === $this) {
                $content->setContentInfo(null);
            }
        }

        return $this;
    }

    public function getPageCounter(): ?int
    {
        return $this->pageCounter;
    }

    public function setPageCounter(?int $pageCounter): self
    {
        $this->pageCounter = $pageCounter;

        return $this;
    }
}
