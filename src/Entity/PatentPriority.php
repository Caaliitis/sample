<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="patent", name="priority")
 * @ORM\Entity(repositoryClass="App\Repository\PatentPriorityRepository")
 */
class PatentPriority
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get-patent"})
     */
    private $sequence;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patent"})
     */
    private $kind;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patent"})
     */
    private $documentIdType;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"get-patent"})
     */
    private $docNumber;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-patent"})
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Patent::class, inversedBy="priorities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getKind(): ?string
    {
        return $this->kind;
    }

    public function setKind(?string $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    public function getDocumentIdType(): ?string
    {
        return $this->documentIdType;
    }

    public function setDocumentIdType(?string $documentIdType): self
    {
        $this->documentIdType = $documentIdType;

        return $this;
    }

    public function getDocNumber(): ?string
    {
        return $this->docNumber;
    }

    public function setDocNumber(?string $docNumber): self
    {
        $this->docNumber = $docNumber;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }
}
