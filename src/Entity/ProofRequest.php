<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\UserCreateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="api", name="proof_request")
 * @ORM\Entity(repositoryClass="App\Repository\ProofRequestRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     normalizationContext={"groups"={"get-proof", "date"}},
 *     collectionOperations={
 *         "get",
 *          "post"={
 *              "denormalization_context"={"groups"={"add-proof"}},
 *              "openapi_context"={
 *                "summary"="Set only one entity(product or patent) for Proof Request",
 *              }
 *          }
 *     },
 *     itemOperations={
 *         "get",
 *          "put"={
 *              "security"="is_granted('ROLE_STAFF')",
 *              "denormalization_context"={"groups"={"edit-proof"}}
 *          }
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "id": "exact",
 *     "product.id":"exact",
 *     "patent.id":"exact"
 * })
 * @ApiFilter(BooleanFilter::class, properties={"status"})
 */
class ProofRequest
{
	use TimestampableCreateEntity, UserCreateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-proof"})
     */
    private $id;

    /**
     * @ApiProperty(readableLink=false, writableLink=false)
     * @ORM\ManyToOne(targetEntity="App\Entity\Products")
     * @Groups({"get-proof","add-proof"})
     */
    private $product;

    /**
     * @ApiProperty(readableLink=false, writableLink=false)
     * @ORM\ManyToOne(targetEntity=Patent::class)
     * @Groups({"get-proof","add-proof"})
     */
    private $patent;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     * @Groups({"get-proof","edit-proof"})
     */
    private $status = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }
}
