<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use App\Controller\CreateTrademarkMediaObject;

/**
 * @ORM\Table(schema="trademark")
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={
 *         "groups"={"media_object_read"}
 *     },
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreateTrademarkMediaObject::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                "parameters" = {
 *                      {
 *                          "name" = "trademark_id",
 *                          "in" = "query",
 *                          "description" = "Trademark ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get"
 *     },
 *     itemOperations={
 *         "get",
 *          "delete"={
 *              "normalization_context"={"groups"={"get-trademark", "date"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          }
 *     }
 * )
 * @Vich\Uploadable()
 * @ORM\Entity(repositoryClass="App\Repository\TrademarkImageRepository")
 * @ApiFilter(SearchFilter::class, properties={
 *     "trademark.id":"exact",
 * })
 */
class TrademarkImage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-trademarks","get-trademark","media_object_read"})
     */
    private $id;

	/**
	 * @var string|null
	 *
	 * @ApiProperty(iri="http://schema.org/contentUrl")
	 * @Groups({"get-trademarks","get-trademark","media_object_read"})
	 */
	public $contentUrl;

	/**
	 * @var File|null
	 *
	 * @Assert\NotNull(groups={"media_object_create"})
	 * @Vich\UploadableField(mapping="wims_trademark_image", fileNameProperty="filePath")
	 */
	public $file;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(nullable=true)
	 */
	public $filePath;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trademark", inversedBy="trademarkImages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $trademark;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrademark(): ?Trademark
    {
        return $this->trademark;
    }

    public function setTrademark(?Trademark $trademark): self
    {
        $this->trademark = $trademark;

        return $this;
    }
}
