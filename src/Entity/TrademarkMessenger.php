<?php

namespace App\Entity;


use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-trademarks", "date"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-trademark"}},
 *          },
 *          "post_checker"={
 *              "method"="POST",
 *              "route_name"="trademark_messenger_checker",
 *              "normalization_context"={"groups"={"checker", "date"}},
 *          },
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-trademark", "date"}}
 *          },
 *          "put"={
 *              "denormalization_context"={"groups"={"edit-trademark"}},
 *          }
 *     }
 *
 * )
 * @ORM\Entity(repositoryClass="App\Repository\TrademarkMessengerRepository")
 * @ORM\Table(schema="trademark")
 * @ORM\HasLifecycleCallbacks()
 * @ApiFilter(NumericFilter::class, properties={"page", "count"})
 * @ApiFilter(SearchFilter::class, properties={"context": "iexact"})
 * @ApiFilter(BooleanFilter::class, properties={"status"})
 */
class TrademarkMessenger
{
	use TimestampableCreateEntity, TimestampableUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-trademarks","get-trademark","checker"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get-trademarks","get-trademark","add-trademark","checker"})
     */
    private $page;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-trademarks","get-trademark","add-trademark","checker"})
     */
    private $context;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     * @Groups({"get-trademarks","get-trademark","edit-trademark", "checker"})
     */
    private $status = false;


	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @Groups({"get-trademarks","get-trademark","edit-trademark","checker"})
	 */
	private $count;

	/**
	 * @SerializedName("updatedAt")
	 * @Groups({"get-trademarks","get-trademark", "checker"})
	 */
	public function getUpdatedAtData(): ?\DateTimeInterface
	{
		return $this->getUpdatedAt();
	}


	public function getId(): ?int
    {
        return $this->id;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

	public function getContext(): ?string
	{
		return $this->context;
	}

	public function setContext(?string $context): self
	{
		$this->context = $context;

		return $this;
	}

	public function getCount(): ?int
	{
		return $this->count;
	}

	public function setCount(?int $count): self
	{
		$this->count = $count;

		return $this;
	}

	public function isStatus(): bool
	{
		return $this->status;
	}

	public function setStatus( bool $status ): void
	{
		$this->status = $status;
	}
}
