<?php

namespace App\Entity;


use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Controller\CountController;
use App\Filter\ProductCompanyBrandsFilter;
use App\Filter\ProductFeaturesFilter;
use App\Filter\ProductsDateFilter;
use App\Filter\SearchAndConditionFilter;
use App\Filter\SearchNotConditionFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use RuntimeException;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use App\Model\TotalCount;
use App\Model\CountGraph;

/**
 * @ORM\Table(schema="api", name="products")
 * @ORM\Entity(repositoryClass="App\Repository\ProductsRepository")
 * @UniqueEntity("name", message="The product name must be unique")
 * @ApiResource(
 *     normalizationContext={"groups"={"get-product"}},
 *     denormalizationContext={"groups"={"product-write"}},
 *     collectionOperations={
 *         "get_names"={
 *              "normalization_context"={"groups"={"get-product-name"}},
 *               "path"="/products/name",
 *               "method"="GET"
 *         },
 *         "products_no_auth"={
 *              "normalization_context"={"groups"={"no-auth"}},
 *              "path"="/products/no_auth",
 *              "method"="GET",
 *         },
 *         "products_get_count_graph"={
 *              "route_name"="products_get_count_graph",
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "read"=false,
 *              "method"="GET",
 *              "filters"={},
 *              "pagination_enabled"=false,
 *              "output"=CountGraph::class,
 *              "normalization_context"={"groups"={"get-product"}},
 *              "openapi_context"={
 *                "summary"="Product Graph Data",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *         },
 *       "get"={
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "normalization_context"={"groups"={"get-products","date"}},
 *              "method"="GET",
 *              "openapi_context" = {
 *                     "summary"= "Retrieves the collection of Products resources. Attention! Use filter type.id fields for logic OR and type fields for logic AND",
 *              },
 *         },
 *         "post"={
 *              "security"="is_granted('ROLE_PRODUCTS_ADD')",
 *              "denormalization_context"={"groups"={"product-write"}},
 *          },
 *         "post_report"={
 *              "route_name"="products_post_report",
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "method"="POST"
 *          },
 *         "post_products_order"={
 *             "route_name"="post_products_order",
 *             "path"="/products/{id}/order",
 *             "method"="POST",
 *             "deserialize"=false,
 *             "output"=false,
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                "summary"= "Save and send order for current product. Successful response is empty 204",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Product ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "application/json"={
 *                             "schema"={
 *                                 "properties"={
 *                                     "quantity"={
 *                                         "type"="integer"
 *                                     },
 *                                      "comment"={
 *                                         "type"="string"
 *                                     },
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *          },
 *         "put_change_product_order_status"={
 *             "route_name"="put_change_product_order_status",
 *             "path"="/products/order/{id}/status/{status_id}",
 *             "method"="PUT",
 *             "deserialize"=false,
 *             "output"=false,
 *             "input"=false,
 *             "security"="is_granted('ROLE_STAFF')",
 *             "openapi_context"={
 *                "summary"= "Change sample product order status. Successful response is empty 204",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Order ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "status_id",
 *                          "in" = "path",
 *                          "description" = "Order Status ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *             }
 *          },
 *         "put_product_analysis_order_status"={
 *             "route_name"="put_product_analysis_order_status",
 *             "path"="/products/analysis-order/{id}/status/{status_id}",
 *             "method"="PUT",
 *             "deserialize"=false,
 *             "output"=false,
 *             "input"=false,
 *             "security"="is_granted('ROLE_STAFF')",
 *             "openapi_context"={
 *                "summary"= "Change product analysis order status. Successful response is empty 204",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Order ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "status_id",
 *                          "in" = "path",
 *                          "description" = "Order Status ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *             }
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "security"="is_granted('ROLE_PRODUCTS')",
 *              "normalization_context"={"groups"={"get-product","date"}}
 *          },
 *          "products_total_count"={
 *              "path"="/products/total_items",
 *              "controller"=CountController::class,
 *              "method"="GET",
 *              "input"=false,
 *              "output"=TotalCount::class,
 *              "normalization_context"={"groups"={"get-product"}},
 *              "openapi_context"={
 *                "summary"="Total products count",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *          },
 *          "vaporfly_products_total_count"={
 *              "path"="/products/vaporfly/total_items",
 *              "controller"=CountController::class,
 *              "method"="GET",
 *              "input"=false,
 *              "output"=TotalCount::class,
 *              "normalization_context"={"groups"={"get-product"}},
 *              "openapi_context"={
 *                "summary"="Total Vaporfly products count",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *          },
 *         "put"={
 *              "security"="is_granted('ROLE_PRODUCTS_EDIT')",
 *              "normalization_context"={"groups"={"get-product"}},
 *          },
 *         "patch"={
 *              "security"="is_granted('ROLE_PRODUCTS_EDIT')",
 *              "normalization_context"={"groups"={"get-product"}},
 *          },
 *         "post_product_draft_description"={
 *              "route_name"="post_product_draft_description",
 *              "method"="POST",
 *              "filters"={},
 *              "pagination_enabled"=false,
 *              "normalization_context"={"groups"={"get-product"}},
 *              "openapi_context"={
 *                "summary"="Product Draft update from Gather",
 *                "parameters"={},
 *              },
 *              "formats"={"json"}
 *         },
 *         "delete"={"security"="is_granted('ROLE_PRODUCTS_EDIT')"},
 *     },
 *     attributes={"order"={"id": "DESC"}, "filters"={ProductCompanyBrandsFilter::class}}
 * )
 * @ApiFilter(OrderFilter::class, properties={"id", "name", "appeared", "productionStatus.ordering"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(ProductsDateFilter::class, properties={"createdAt","appeared"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "id": "exact",
 *     "name": "ipartial",
 *
 *     "brand": "exact",
 *     "brand.name": "ipartial",
 *
 *     "company": "exact",
 *     "company.name": "ipartial",
 *
 *     "types.id": "exact",
 *
 *     "productionStatus.id": "exact",
 *
 *     "componentsInProduct.component.id": "exact",
 *
 * })
 * @ApiFilter(SearchAndConditionFilter::class, properties={
 *     "types": "exact",
 * })
 * @ApiFilter(SearchNotConditionFilter::class, properties={"types"})
 * @ApiFilter(ProductFeaturesFilter::class, properties={"features"})
 * @ApiFilter(PropertyFilter::class)
 * @ApiFilter(BooleanFilter::class, properties={"draft"})
 * @ORM\HasLifecycleCallbacks()
 */
class Products
{

    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"get-product", "get-products", "get-company", "get-component", "get-components", "get-brand", "no-auth", "get-analysis", "get-order", "get-section-products"})
     */
    private $id;
    /**
     * @Groups({"get-product", "get-products", "get-product-name", "get-company", "get-brand", "product-write", "get-analysis", "get-order", "get-section-products"})
     * @SerializedName("name")
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The product name cannot be empty")
     */
    private $name;
    /**
     * @Assert\NotNull
     * @Groups({"get-product", "get-products", "get-company", "product-write"})
     * @ORM\Column(name="appeared", type="date", nullable=false)
     */
    private $appeared;
    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;
    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\Column(name="specification", type="text", length=65535, nullable=true)
     */
    private $specification;
    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\Column(name="retail_price", type="float", precision=10, scale=0, nullable=false, options={"default"="0"})
     */
    private $retailPrice = 0;
    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\Column(name="wholesale_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $wholesalePrice = 0;

    /**
     * @Assert\NotNull
     * @Groups({"get-product", "get-products", "get-company", "product-write"})
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductionStatuses")
     * @ORM\JoinColumn(name="production_status", referencedColumnName="id", nullable=false)
     */
    private $productionStatus;

    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\Column(name="usp", type="text", length=65535, nullable=true)
     */
    private $usp;
    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\Column(name="new_solution", type="text", length=65535, nullable=true)
     */
    private $newSolution;
    /**
     * @ORM\Column(name="revision", type="integer", nullable=false, options={"default"="1"})
     */
    private $revision = '1';
    /**
     * @Groups({"get-product", "get-products"})
     * @SerializedName("images")
     * @ORM\OneToMany(targetEntity="App\Entity\ProductImages", mappedBy="product")
     */
    private $media;
    /**
     * @Groups({"get-product"})
     * @ORM\OneToMany(targetEntity="App\Entity\ProductFiles", mappedBy="product")
     */
    private $file;
    /**
     * @SerializedName("types")
     * @ORM\ManyToMany(targetEntity="App\Entity\Types", inversedBy="products")
     * @ORM\JoinTable(schema="api", name="products_has_types",
     *     joinColumns={
     *         @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     *     }
     * )
     * @Groups({"get-product", "get-products", "product-write"})
     * @MaxDepth(1)
     */
    private $types;
    /**
     * @Groups({"get-product", "get-products", "product-write"})
     * @ORM\OneToMany(targetEntity="App\Entity\ProductFeatureValues", mappedBy="product", orphanRemoval=true, cascade={"persist"})
     * @SerializedName("features")
     */
    private $productFeatureValue;
    /**
     * @Groups({"get-product", "get-products", "product-write"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Brands", inversedBy="products")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * @MaxDepth(1)
     */
    private $brand;
    /**
     * @Groups({"get-product", "get-products", "product-write"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies", inversedBy="products")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="oemVersions")
     */
    private $oemProduct;
    /**
     * @Groups({"get-product"})
     * @ORM\OneToMany(targetEntity="App\Entity\Products", mappedBy="oemProduct")
     */
    private $oemVersions;
    /**
     * @Groups({"get-product"})
     * @ORM\OneToMany(targetEntity="App\Entity\Samples", mappedBy="product")
     */
    private $samples;
    /**
     * @Groups({"get-product", "get-products","user", "user:write"})
     * @ORM\OneToMany(targetEntity="App\Entity\ProductsComponents", mappedBy="product", orphanRemoval=true)
     * @SerializedName("components")
     */
    private $componentsInProduct;
    /**
     * @ORM\Column(type="boolean", options={"default":"1"})
     * @Groups({"get-product", "get-products", "product-write"})
     */
    private $draft = true;
    /**
     * @Groups({"get-product", "product-write"})
     * @ORM\OneToMany(targetEntity="App\Entity\ProductInformationSource", mappedBy="product", orphanRemoval=true, cascade={"persist"})
     * @SerializedName("productInformationSources")
     */
    private $productInformationSources;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Ubersearch\Products", mappedBy="product", orphanRemoval=true)
     */
    private $ubersearchIndex;

    private $imageDefault = null;

    public function __construct()
    {
        $this->media = new ArrayCollection();
        $this->file = new ArrayCollection();
        $this->types = new ArrayCollection();
        $this->productFeatureValue = new ArrayCollection();
        $this->oemVersions = new ArrayCollection();
        $this->samples = new ArrayCollection();
        $this->componentsInProduct = new ArrayCollection();
        $this->productInformationSources = new ArrayCollection();
    }

    /**
     * @SerializedName("oemProduct")
     * @Groups({"get-product", "get-products"})
     */
    public function getOemProductBriefly()
    {
        $bOem = array();

        if (null !== $this->getOemProduct()) {
            $bOem['id'] = $this->getOemProduct()->getId();
            $bOem['name'] = $this->getOemProduct()->getName();
        }

        return $bOem ? $bOem : null;
    }

    public function getOemProduct(): ?self
    {
        return $this->oemProduct;
    }

    public function setOemProduct(?self $oemProduct): self
    {
        //if current product are tried to define like OEM and it contains OEM then throw error
        if ($this->oemVersions->count() > 0) {
            throw new RuntimeException("OEM product can\'t be OEM version", 400);
        }

        $this->oemProduct = $oemProduct;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @SerializedName("oemVersions")
     * @Groups({"get-product", "get-products", "product-write"})
     */
    public function getOemVersionsBriefly()
    {
        $oemVersionsBriefly = [];

        /** @var Products $oem */
        foreach ($this->getOemVersions() as $oem) {
            $bOem = array();
            $bOem['id'] = $oem->getId();
            $bOem['name'] = $oem->getName();
            $oemVersionsBriefly[] = $bOem;
        }

        return $oemVersionsBriefly;
    }

    public function getOemVersions(): Collection
    {
        return $this->oemVersions;
    }

    /**
     * @return Collection|ProductFeatureValues[]
     */
    public function getProductFeatureValue()
    {
        return $this->productFeatureValue;
    }

    public function getFeatures()
    {
        return $this->productFeatureValue;
    }

    /**
     * @SerializedName("hasFiles")
     */
    public function getHasFiles()
    {
        return count($this->getFile());
    }

    /**
     * @return Collection|ProductFiles[]
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return Collection|ProductImages[]
     */
    public function getMedia()
    {
        return $this->media;
    }

    public function setMediaForNormalizing(ArrayCollection $collection)
    {
        $this->media = $collection;
    }

    public function getAppeared(): DateTime
    {
        return $this->appeared;
    }

    public function setAppeared(DateTime $appeared)
    {
        $this->appeared = $appeared;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @SerializedName("description")
     */
    public function getDescriptionText(): string
    {
        return strip_tags(html_entity_decode($this->description));
    }

    public function getSpecification()
    {
        return $this->specification;
    }

    public function setSpecification(string $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @SerializedName("specification")
     */
    public function getSpecificationText(): string
    {
        $pattern = "</li>";
        $replace = $pattern . ";";

        return strip_tags(html_entity_decode(str_replace($pattern, $replace, $this->specification)));

    }

    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    public function setRetailPrice(float $retailPrice)
    {
        $this->retailPrice = $retailPrice;
    }

    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }

    public function setWholesalePrice(float $wholesalePrice)
    {
        $this->wholesalePrice = $wholesalePrice;
    }

    /**
     * @Groups({"get-product", "get-products"})
     * @SerializedName("productionStatus")
     */
    public function getProductionStatusName()
    {
        return $this->getProductionStatus()->getName();
    }

    public function getProductionStatus()
    {
        return $this->productionStatus;
    }

    public function setProductionStatus($productionStatus)
    {
        $this->productionStatus = $productionStatus;
    }

    public function getUsp()
    {
        return $this->usp;
    }

    public function setUsp(string $usp)
    {
        $this->usp = $usp;
    }

    /**
     * @SerializedName("usp")
     */
    public function getUspText(): string
    {
        return strip_tags(html_entity_decode($this->usp));
    }

    public function getNewSolution()
    {
        return $this->newSolution;
    }

    public function setNewSolution(string $newSolution)
    {
        $this->newSolution = $newSolution;
    }

    public function isHasSamples()
    {
        return $this->getSamples() ? true : false;
    }

    /**
     * @return Collection|Samples[]
     */
    public function getSamples(): Collection
    {
        return $this->samples;
    }

    /**
     * @SerializedName("newSolution")
     */
    public function getNewSolutionText(): string
    {
        return strip_tags(html_entity_decode($this->newSolution));

    }

    public function getRevision(): int
    {
        return $this->revision;
    }

    public function setRevision(int $revision)
    {
        $this->revision = $revision;
    }

    /**
     * @SerializedName("brand")
     * @Groups({"get-product", "get-products"})
     * @return null|string
     */
    public function getBrandName()
    {
        $brand = $this->getBrand();

        return $brand ? $brand->getName() : '';
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @SerializedName("company")
     * @Groups({"get-product", "get-products"})
     * @return null|string
     */
    public function getCompanyName()
    {
        $company = $this->getCompany();

        return $company ? $company->getName() : '';
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany($company)
    {
        $this->company = $company;
    }

    public function addSample(Samples $sample): self
    {
        if (!$this->samples->contains($sample)) {
            $this->samples[] = $sample;
            $sample->setProduct($this);
        }

        return $this;
    }

    public function removeSample(Samples $sample): self
    {
        if ($this->samples->contains($sample)) {
            $this->samples->removeElement($sample);
            // set the owning side to null (unless already changed)
            if ($sample->getProduct() === $this) {
                $sample->setProduct(null);
            }
        }

        return $this;
    }

    public function isUspProductByTag(): bool
    {
        foreach ($this->getTypes() as $type) {
            if (strtolower($type->getName()) === 'usp') {
                return true;
            }
        }

        return false;
    }

    public function getDefaultImagePath(): string
    {
        if ($this->getDefaultImage()) {
            return $this->getDefaultImage()->getFilePath() . $this->getDefaultImage()->getSource();
        }

        return '';
    }

    /**
     * @SerializedName("defaultImage")
     * @Groups({"get-product", "get-products", "no-auth"})
     */
    public function getDefaultImage(): ?ProductImages
    {
        if ($this->imageDefault !== null) {
            return $this->imageDefault;
        }

        foreach ($this->getMedia() as $key => $image) {
            if ($image->getDefaultImg()) {
                return $image;
            }
        }

        return null;
    }

    public function setDefaultImage(ProductImages $image)
    {
        $this->imageDefault = $image;

        return $this->imageDefault;
    }


    public function addComponentInProduct(Components $component): self
    {

        if (!$this->componentsInProduct->contains($component)) {
            $this->componentsInProduct[] = $component;
            $component->addProductWithComponent($this);
        }

        return $this;
    }

    public function removeComponentInProduct(Components $component): self
    {

        if ($this->componentsInProduct->contains($component)) {

            $this->componentsInProduct->removeElement($component);
            $component->removeProductWithComponent($this);

        }

        return $this;
    }

    public function getDraft(): ?bool
    {
        return $this->draft;
    }

    public function setDraft(bool $draft): self
    {
        $this->draft = $draft;

        return $this;
    }

    /**
     * @return Collection|ProductInformationSource[]
     */
    public function getProductInformationSources(): Collection
    {
        return $this->productInformationSources;
    }

    public function addProductInformationSource(ProductInformationSource $productInformationSource): self
    {
        if (!$this->productInformationSources->contains($productInformationSource)) {
            $this->productInformationSources[] = $productInformationSource;
            $productInformationSource->setProduct($this);
        }

        return $this;
    }

    public function removeProductInformationSource(ProductInformationSource $productInformationSource): self
    {
        if ($this->productInformationSources->contains($productInformationSource)) {
            $this->productInformationSources->removeElement($productInformationSource);
            // set the owning side to null (unless already changed)
            if ($productInformationSource->getProduct() === $this) {
                $productInformationSource->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @SerializedName("componentsAmount")
     * @Groups({"get-product", "get-products"})
     */
    public function getComponentsAmount(): int
    {
        return $this->getComponentsInProduct()->count();
    }

    /**
     * @return Collection|ProductsComponents[]
     */
    public function getComponentsInProduct(): Collection
    {
        return $this->componentsInProduct;
    }

    /**
     * @SerializedName("tradingPrice")
     * @Groups({"get-product", "get-products"})
     */
    public function getTradingPrice(): float
    {
        if ($this->wholesalePrice === 0 || $this->wholesalePrice === null) {
            return $this->wholesalePrice;
        }

        return round(($this->wholesalePrice + 1.5 + ($this->wholesalePrice * 0.27) + ($this->wholesalePrice * 0.10) + ($this->wholesalePrice * 0.15)), 2, PHP_ROUND_HALF_UP);
    }

    /**
     * @SerializedName("moq")
     * @Groups({"get-product", "get-products"})
     */
    public function getMoq(): float
    {
        foreach ($this->productFeatureValue as $featureValue) {
            /** @var ProductFeatureValues $featureValue */
            if ($featureValue->getFeature()->getId() === 35) {
                return $featureValue->getValue();
            }
        }

        return 0;
    }


    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Types $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(Types $type): self
    {
        if ($this->types->contains($type)) {
            $this->types->removeElement($type);
        }

        $this->types = new ArrayCollection($this->types->getValues());

        return $this;
    }


    public function addProductFeatureValue(ProductFeatureValues $productFeatureValue): self
    {
        if (!$this->productFeatureValue->contains($productFeatureValue)) {
            $this->productFeatureValue[] = $productFeatureValue;
            $productFeatureValue->setProduct($this);
        }

        return $this;
    }

    public function removeProductFeatureValue(ProductFeatureValues $productFeatureValue): self
    {
        if ($this->productFeatureValue->contains($productFeatureValue)) {
            $this->productFeatureValue->removeElement($productFeatureValue);
            // set the owning side to null (unless already changed)
            if ($productFeatureValue->getProduct() === $this) {
                $productFeatureValue->setProduct(null);
            }
        }

        return $this;
    }

}
