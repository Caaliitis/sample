<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use App\File\ContentUri;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ApiResource(
 *    collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-news", "date"}}
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-news-item", "date"}}
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(schema="api", name="news")
 * @ApiFilter(OrderFilter::class, properties={"id", "publishedAt"}, arguments={"orderParameterName"="order"})
 */
class News implements ContentUri
{
	use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-news","get-news-item"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-news","get-news-item"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-news","get-news-item"})
     */
    private $slug;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get-news","get-news-item"})
     */
    private $introText;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get-news-item"})
     */
    private $fullText;

	/**
	 * @ORM\Column(type="datetime",type="datetime")
	 * @SerializedName("publishedAt")
	 * @Groups({"get-news","get-news-item"})
	 */
	protected $publishedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsImage", mappedBy="news", orphanRemoval=true)
     * @Groups({"get-news","get-news-item"})
     * @MaxDepth(1)
     */
    private $images;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIntroText(): ?string
    {
        return $this->introText;
    }

    public function setIntroText(string $introText): self
    {
        $this->introText = $introText;

        return $this;
    }

    public function getFullText(): ?string
    {
        return $this->fullText;
    }

    public function setFullText(string $fullText): self
    {
        $this->fullText = $fullText;

        return $this;
    }

	public function setPublishedAt(): self
	{
		$this->publishedAt = new \DateTime();

		return $this;
	}

	public function getPublishedAt(): ?\DateTimeInterface
	{
		return $this->publishedAt;
	}

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getContentUris(): ?Collection
    {
        return $this->images;
    }
}
