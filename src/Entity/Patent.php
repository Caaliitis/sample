<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-patents", "date"}}
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-patent", "date"}}
 *          }
 *     }
 * )
 * @ORM\Table(schema="patent", name="patent")
 * @ORM\Entity(repositoryClass="App\Repository\PatentRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiFilter(PropertyFilter::class)
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "company.id": "exact","company.name": "ipartial"})
 * @ApiFilter(BooleanFilter::class, properties={"hasClaims", "hasDescription", "hasDocuments"})
 */
class Patent
{
    use TimestampableCreateEntity, TimestampableUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-patents","get-patent"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get-patents","get-patent"})
     */
    private $familyId;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"get-patents","get-patent"})
     */
    private $docNumber;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups({"get-patents","get-patent"})
     */
    private $kind;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Companies",inversedBy="patents")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-patents","get-patent"})
     * @MaxDepth(2)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PatentApplication", mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $applications;

    /**
     * @ORM\OneToMany(targetEntity=PatentInventor::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $inventors;

    /**
     * @ORM\OneToMany(targetEntity=PatentInventionTitle::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent","get-patents"})
     */
    private $inventionTitles;

    /**
     * @ORM\OneToMany(targetEntity=PatentApplicant::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $applicants;

    /**
     * @ORM\OneToMany(targetEntity=PatentPriority::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $priorities;

    /**
     * @ORM\OneToMany(targetEntity=PatentClassification::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $classifications;

    /**
     * @ORM\OneToMany(targetEntity=PatentAbstract::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $abstracts;

    /**
     * @ORM\OneToMany(targetEntity=PatentClassificationIpc::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $classificationIpcs;

    /**
     * @ORM\Column(type="string", length=3)
     * @Groups({"get-patents","get-patent"})
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"get-patents","get-patent"})
     */
    private $system;

    /**
     * @ORM\OneToMany(targetEntity=PatentPublicationReference::class, mappedBy="patent", orphanRemoval=true, cascade={"persist"})
     * @Groups({"get-patent"})
     */
    private $publicationReferences;

    /**
     * @ORM\OneToMany(targetEntity=PatentClaim::class, mappedBy="patent", orphanRemoval=true)
     */
    private $claims;

    /**
     * @ORM\OneToMany(targetEntity=PatentDescription::class, mappedBy="patent", orphanRemoval=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get-patent"})
     */
    private $hasClaims;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get-patent"})
     */
    private $hasDescription;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get-patent"})
     */
    private $hasDocuments;

    /**
     * @ORM\OneToMany(targetEntity=PatentDocument::class, mappedBy="patent", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PatentContent", mappedBy="patent", orphanRemoval=true)
     */
    private $contents;


    public function __construct()
    {
        $this->applications = new ArrayCollection();
        $this->inventors = new ArrayCollection();
        $this->inventionTitles = new ArrayCollection();
        $this->applicants = new ArrayCollection();
        $this->priorities = new ArrayCollection();
        $this->classifications = new ArrayCollection();
        $this->abstracts = new ArrayCollection();
        $this->classificationIpcs = new ArrayCollection();
        $this->publicationReferences = new ArrayCollection();
        $this->claims = new ArrayCollection();
        $this->description = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFamilyId(): ?int
    {
        return $this->familyId;
    }

    public function setFamilyId(int $familyId): self
    {
        $this->familyId = $familyId;

        return $this;
    }

    public function getDocNumber(): ?string
    {
        return $this->docNumber;
    }

    public function setDocNumber(string $docNumber): self
    {
        $this->docNumber = $docNumber;

        return $this;
    }

    public function getKind(): ?string
    {
        return $this->kind;
    }

    public function setKind(string $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany(?Companies $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|PatentApplication[]
     */
    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(PatentApplication $application): self
    {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setPatent($this);
        }

        return $this;
    }

    public function removeApplication(PatentApplication $application): self
    {
        if ($this->applications->contains($application)) {
            $this->applications->removeElement($application);
            // set the owning side to null (unless already changed)
            if ($application->getPatent() === $this) {
                $application->setPatent(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|PatentInventor[]
     */
    public function getInventors(): Collection
    {
        return $this->inventors;
    }

    public function addInventor(PatentInventor $inventor): self
    {
        if (!$this->inventors->contains($inventor)) {
            $this->inventors[] = $inventor;
            $inventor->setPatent($this);
        }

        return $this;
    }

    public function removeInventor(PatentInventor $inventor): self
    {
        if ($this->inventors->contains($inventor)) {
            $this->inventors->removeElement($inventor);
            // set the owning side to null (unless already changed)
            if ($inventor->getPatent() === $this) {
                $inventor->setPatent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PatentInventionTitle[]
     */
    public function getInventionTitles(): Collection
    {
        return $this->inventionTitles;
    }

    public function addInventionTitle(PatentInventionTitle $inventionTitle): self
    {
        if (!$this->inventionTitles->contains($inventionTitle)) {
            $this->inventionTitles[] = $inventionTitle;
            $inventionTitle->setPatent($this);
        }

        return $this;
    }

    public function removeInventionTitle(PatentInventionTitle $inventionTitle): self
    {
        if ($this->inventionTitles->contains($inventionTitle)) {
            $this->inventionTitles->removeElement($inventionTitle);
            // set the owning side to null (unless already changed)
            if ($inventionTitle->getPatent() === $this) {
                $inventionTitle->setPatent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PatentApplicant[]
     */
    public function getApplicants(): Collection
    {
        return $this->applicants;
    }

    public function addApplicant(PatentApplicant $applicant): self
    {
        if (!$this->applicants->contains($applicant)) {
            $this->applicants[] = $applicant;
            $applicant->setPatent($this);
        }

        return $this;
    }

    public function removeApplicant(PatentApplicant $applicant): self
    {
        if ($this->applicants->contains($applicant)) {
            $this->applicants->removeElement($applicant);
            // set the owning side to null (unless already changed)
            if ($applicant->getPatent() === $this) {
                $applicant->setPatent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PatentPriority[]
     */
    public function getPriorities(): Collection
    {
        return $this->priorities;
    }

    public function addPriority(PatentPriority $priority): self
    {
        if (!$this->priorities->contains($priority)) {
            $this->priorities[] = $priority;
            $priority->setPatent($this);
        }

        return $this;
    }

    public function removePriority(PatentPriority $priority): self
    {
        if ($this->priorities->contains($priority)) {
            $this->priorities->removeElement($priority);
            // set the owning side to null (unless already changed)
            if ($priority->getPatent() === $this) {
                $priority->setPatent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PatentClassification[]
     */
    public function getClassifications(): Collection
    {
        return $this->classifications;
    }

    public function addClassification(PatentClassification $classification): self
    {
        if (!$this->classifications->contains($classification)) {
            $this->classifications[] = $classification;
            $classification->setPatent($this);
        }

        return $this;
    }

    public function removeClassification(PatentClassification $classification): self
    {
        if ($this->classifications->contains($classification)) {
            $this->classifications->removeElement($classification);
            // set the owning side to null (unless already changed)
            if ($classification->getPatent() === $this) {
                $classification->setPatent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PatentAbstract[]
     */
    public function getAbstracts(): Collection
    {
        return $this->abstracts;
    }

    public function addAbstract(PatentAbstract $abstract): self
    {
        if (!$this->abstracts->contains($abstract)) {
            $this->abstracts[] = $abstract;
            $abstract->setPatent($this);
        }

        return $this;
    }

    public function removeAbstract(PatentAbstract $abstract): self
    {
        if ($this->abstracts->contains($abstract)) {
            $this->abstracts->removeElement($abstract);
            // set the owning side to null (unless already changed)
            if ($abstract->getPatent() === $this) {
                $abstract->setPatent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PatentClassificationIpc[]
     */
    public function getClassificationIpcs(): Collection
    {
        return $this->classificationIpcs;
    }

    public function addClassificationIpc(PatentClassificationIpc $classificationIpc): self
    {
        if (!$this->classificationIpcs->contains($classificationIpc)) {
            $this->classificationIpcs[] = $classificationIpc;
            $classificationIpc->setPatent($this);
        }

        return $this;
    }

    public function removeClassificationIpc(PatentClassificationIpc $classificationIpc): self
    {
        if ($this->classificationIpcs->contains($classificationIpc)) {
            $this->classificationIpcs->removeElement($classificationIpc);
            // set the owning side to null (unless already changed)
            if ($classificationIpc->getPatent() === $this) {
                $classificationIpc->setPatent(null);
            }
        }

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getSystem(): ?string
    {
        return $this->system;
    }

    public function setSystem(?string $system): self
    {
        $this->system = $system;

        return $this;
    }

    /**
     * @return Collection|PatentPublicationReference[]
     */
    public function getPublicationReferences(): Collection
    {
        return $this->publicationReferences;
    }

    public function addPublicationReference(PatentPublicationReference $publicationReference): self
    {
        if (!$this->publicationReferences->contains($publicationReference)) {
            $this->publicationReferences[] = $publicationReference;
            $publicationReference->setPatent($this);
        }

        return $this;
    }

    public function removePublicationReference(PatentPublicationReference $publicationReference): self
    {
        if ($this->publicationReferences->contains($publicationReference)) {
            $this->publicationReferences->removeElement($publicationReference);
            // set the owning side to null (unless already changed)
            if ($publicationReference->getPatent() === $this) {
                $publicationReference->setPatent(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|PatentClaim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(PatentClaim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setPatent($this);
        }

        return $this;
    }

    public function removeClaim(PatentClaim $claim): self
    {
        if ($this->claims->contains($claim)) {
            $this->claims->removeElement($claim);
            // set the owning side to null (unless already changed)
            if ($claim->getPatent() === $this) {
                $claim->setPatent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PatentDescription[]
     */
    public function getDescription(): Collection
    {
        return $this->description;
    }

    public function addDescription(PatentDescription $description): self
    {
        if (!$this->description->contains($description)) {
            $this->description[] = $description;
            $description->setPatent($this);
        }

        return $this;
    }

    public function removeDescription(PatentDescription $description): self
    {
        if ($this->description->contains($description)) {
            $this->description->removeElement($description);
            // set the owning side to null (unless already changed)
            if ($description->getPatent() === $this) {
                $description->setPatent(null);
            }
        }

        return $this;
    }


    public function getHasClaims(): ?bool
    {
        return $this->hasClaims;
    }

    public function setHasClaims(?bool $hasClaims): self
    {
        $this->hasClaims = $hasClaims;

        return $this;
    }

    public function getHasDescription(): ?bool
    {
        return $this->hasDescription;
    }

    public function setHasDescription(?bool $hasDescription): self
    {
        $this->hasDescription = $hasDescription;

        return $this;
    }

    /**
     * @return Collection|PatentDocument[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function getHasDocuments(): ?bool
    {
        return $this->hasDocuments;
    }

    public function setHasDocuments(?bool $hasDocuments): self
    {
        $this->hasDocuments = $hasDocuments;

        return $this;
    }
}
