<?php



namespace App\Entity\Ubersearch;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Class Products
 * @ORM\Entity
 * @ORM\Table(schema="api", name="products_tokenized")
 */
class Products
{
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\Column(name="description", type="tsvector", nullable=true)
	 */
	private $description;

	/**
	 *
	 * @ORM\Column(name="name", type="tsvector")
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Products", inversedBy="ubersearchIndex")
	 * @MaxDepth(1)
	 */
	private $product;

	/**
	 *
	 * @ORM\Column(name="usp", type="tsvector")
	 */
	private $usp;

	/**
	 *
	 * @ORM\Column(name="new_solution", type="tsvector")
	 */
	private $new_solution;

	/**
	 *
	 * @ORM\Column(name="specification", type="tsvector")
	 */
	private $specification;

	public function getId(): int
	{
		return $this->id;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getUsp()
	{
		return $this->usp;
	}

	public function getNewSolution()
	{
		return $this->new_solution;
	}

	public function getSpecification()
	{
		return $this->specification;
	}
}