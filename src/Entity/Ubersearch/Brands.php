<?php



namespace App\Entity\Ubersearch;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Brands
 * @ORM\Entity
 * @ORM\Table(schema="api", name="brands_tokenized")
 */
class Brands
{
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="tsvector")
	 */
	private $name;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Brands", inversedBy="ubersearchIndex")
	 */
	private $brand;


	public function getId(): int
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}
}