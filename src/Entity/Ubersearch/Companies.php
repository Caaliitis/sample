<?php



namespace App\Entity\Ubersearch;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Class Companies
 * @ORM\Entity
 * @ORM\Table(schema="api", name="companies_tokenized")
 */
class Companies
{
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\Column(name="description", type="tsvector", nullable=true)
	 */
	private $description;

	/**
	 * @ORM\Column(name="name", type="tsvector")
	 */
	private $name;

	/**
	 * @ORM\Column(name="address", type="tsvector", nullable=true)
	 */
	private $address;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Companies", inversedBy="ubersearchIndex")
	 * @MaxDepth(1)
	 */
	private $company;


	public function getId(): int
	{
		return $this->id;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getAddress()
	{
		return $this->address;
	}
}