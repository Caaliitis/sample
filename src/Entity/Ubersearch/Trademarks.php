<?php



namespace App\Entity\Ubersearch;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Class Trademarks
 * @ORM\Entity
 * @ORM\Table(schema="trademark", name="trademark_tokenized")
 */
class Trademarks
{
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\Column(name="holder", type="tsvector")
	 */
	private $holder;

	/**
	 *
	 * @ORM\Column(name="name", type="tsvector")
	 */
	private $name;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Trademark", inversedBy="ubersearchIndex")
	 * @MaxDepth(1)
	 */
	private $trademark;


	public function getId(): int
	{
		return $this->id;
	}

	public function getHolder()
	{
		return $this->holder;
	}

	public function getName()
	{
		return $this->name;
	}
}