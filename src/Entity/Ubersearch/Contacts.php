<?php



namespace App\Entity\Ubersearch;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contacts
 * @ORM\Entity
 * @ORM\Table(schema="api", name="contacts_tokenized")
 */
class Contacts
{
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\Column(name="surname", type="tsvector", nullable=true)
	 */
	private $surname;

	/**
	 * @ORM\Column(name="name", type="tsvector")
	 */
	private $name;

	/**
	 * @ORM\Column(name="position", type="tsvector", nullable=true)
	 */
	private $position;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Contacts", inversedBy="ubersearchIndex")
	 */
	private $contact;


	public function getId(): int
	{
		return $this->id;
	}

	public function getPosition()
	{
		return $this->position;
	}

	public function getSurname()
	{
		return $this->surname;
	}

	public function getName()
	{
		return $this->name;
	}
}