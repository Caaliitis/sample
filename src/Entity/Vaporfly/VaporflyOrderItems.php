<?php

namespace App\Entity\Vaporfly;

use App\Entity\Products;
use App\Repository\Vaporfly\VaporflyOrderItemsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(schema="vaporfly", name="order_items")
 * @ORM\Entity(repositoryClass=VaporflyOrderItemsRepository::class)
 */
class VaporflyOrderItems
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThan(value="0")
     * @Assert\LessThanOrEqual(value="50000000")
     * @Groups({"get-order","add-vaporfly-order"})
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Products::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-order","add-vaporfly-order"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=VaporflyOrder::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vaporflyOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getVaporflyOrder(): ?VaporflyOrder
    {
        return $this->vaporflyOrder;
    }

    public function setVaporflyOrder(?VaporflyOrder $vaporflyOrder): self
    {
        $this->vaporflyOrder = $vaporflyOrder;

        return $this;
    }
}
