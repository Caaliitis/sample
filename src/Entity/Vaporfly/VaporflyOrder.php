<?php

namespace App\Entity\Vaporfly;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Doctrine\OrderStatusInterface;
use App\Entity\OrderStatus;
use App\Security\OwnerSecurity;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(schema="vaporfly", name="orders")
 * @ORM\Entity(repositoryClass="App\Repository\VaporflyOrderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-order", "date"}},
 *              "openapi_context"={
 *                "summary"= "Retrieves the collection of ProductOrder resources for Vaporfly. Needs ROLE_STAFF or ROLE_SHOP accesses for successful operation",
 *              }
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-vaporfly-order"}},
 *              "normalization_context"={"groups"={"get-order", "date"}},
 *              "security"="is_granted('ROLE_SHOP')",
 *              "openapi_context"={
 *                "summary"= "Creates a resource. Needs ROLE_SHOP access for successful operation",
 *              }
 *          },
 *         "put_change_vaporfly_order_status"={
 *             "route_name"="put_change_vaporfly_order_status",
 *             "path"="/api/vaporfly_orders/{id}/status/{status_id}",
 *             "method"="PUT",
 *             "deserialize"=false,
 *             "output"=false,
 *             "input"=false,
 *             "security"="is_granted('ROLE_STAFF')",
 *             "openapi_context"={
 *                "summary"= "Change Vaporfly order status. Successful response is empty 204",
 *                "parameters" = {
 *                      {
 *                          "name" = "id",
 *                          "in" = "path",
 *                          "description" = "Order ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "status_id",
 *                          "in" = "path",
 *                          "description" = "Order Status ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      }
 *                  },
 *             }
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-order", "date"}},
 *              "security"="is_granted('ROLE_STAFF') or is_granted('ROLE_SHOP')",
 *              "openapi_context"={
 *                "summary"= "Retrieves a resource. Needs ROLE_STAFF or ROLE_SHOP accesses for successful operation",
 *              }
 *          },
 *          "put"={
 *              "denormalization_context"={"groups"={"edit-vaporfly-order"}},
 *              "normalization_context"={"groups"={"get-order", "date"}},
 *              "security"="is_granted('ROLE_STAFF')",
 *              "openapi_context"={
 *                "summary"= "Replaces the Users resource. Needs ROLE_STAFF access for successful operation",
 *              }
 *          }
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "comment", "status.ordering" }, arguments={"orderParameterName"="order"})
 */
class VaporflyOrder implements OwnerSecurity, OrderStatusInterface
{
	use UserCreateEntity, UserUpdateEntity, TimestampableCreateEntity, TimestampableUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-order"})
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $isSent = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-order","add-vaporfly-order"})
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderStatus")
     * @Groups({"get-order","edit-vaporfly-order"})
     */
    private $status;

    /**
     * @Groups({"get-order","add-vaporfly-order"})
     * @ORM\OneToMany(targetEntity=VaporflyOrderItems::class, mappedBy="vaporflyOrder", orphanRemoval=true, cascade={"persist"})
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsSent(): ?bool
    {
        return $this->isSent;
    }

    public function setIsSent(bool $isSent): self
    {
        $this->isSent = $isSent;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getStatus(): ?OrderStatus
    {
        return $this->status;
    }

    public function setStatus(?OrderStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|VaporflyOrderItems[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(VaporflyOrderItems $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setVaporflyOrder($this);
        }

        return $this;
    }

    public function removeItem(VaporflyOrderItems $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getVaporflyOrder() === $this) {
                $item->setVaporflyOrder(null);
            }
        }

        return $this;
    }
}
