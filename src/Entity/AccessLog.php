<?php

namespace App\Entity;

use App\Repository\AccessLogRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AccessLogRepository::class)
 * @ORM\Table(schema="api", name="access_log")
 * @ORM\HasLifecycleCallbacks()
 */
class AccessLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $host;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $method;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=2056, nullable=true)
     */
    private $queryString;

    /**
     * @ORM\Column(type="string", length=2056, nullable=true)
     */
    private $payload;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $recaptchaResponse = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $error;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $recaptchaNotification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(?string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(?string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getQueryString(): ?string
    {
        return $this->queryString;
    }

    public function setQueryString(?string $queryString): self
    {
        $this->queryString = $queryString;

        return $this;
    }

    public function getPayload(): ?string
    {
        return $this->payload;
    }

    public function setPayload(?string $payload): self
    {
        $this->payload = $payload;

        return $this;
    }

    public function getRecaptchaResponse(): ?array
    {
        return $this->recaptchaResponse;
    }

    public function setRecaptchaResponse(?array $recaptchaResponse): self
    {
        $this->recaptchaResponse = $recaptchaResponse;

        return $this;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     * @return AccessLog
     */
    public function setCreatedAt(): self
    {
        $this->createdAt = new DateTime();

        return $this;
    }

    public function getRecaptchaNotification(): ?bool
    {
        return $this->recaptchaNotification;
    }

    public function setRecaptchaNotification(?bool $recaptchaNotification): self
    {
        $this->recaptchaNotification = $recaptchaNotification;

        return $this;
    }
}
