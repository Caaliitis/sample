<?php

namespace App\Entity;

use App\Repository\PatentMessengerRepository;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(schema="patent", name="patent_messenger")
 * @ORM\Entity(repositoryClass=PatentMessengerRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class PatentMessenger
{
	use TimestampableCreateEntity, TimestampableUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", options={"default" = false}))
     */
    private $status = false;

    /**
     * @ORM\ManyToOne(targetEntity=Companies::class, inversedBy="patentMessengers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="integer")
     */
    private $startPosition;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCompany(): ?Companies
    {
        return $this->company;
    }

    public function setCompany(?Companies $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getStartPosition(): ?int
    {
        return $this->startPosition;
    }

    public function setStartPosition(int $startPosition): self
    {
        $this->startPosition = $startPosition;

        return $this;
    }

}
