<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use App\Controller\CreatePatentMediaObject;
use App\Repository\PatentContentRepository;


/**
 * @ORM\Table(schema="patent", name="content")
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={
 *         "groups"={"media_object_read"}
 *     },
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreatePatentMediaObject::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                "parameters" = {
 *                      {
 *                          "name" = "patent_id",
 *                          "in" = "query",
 *                          "description" = "Patent ID",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "page_number",
 *                          "in" = "query",
 *                          "description" = "Patent Page Number",
 *                          "required" = "true",
 *                          "type" : "integer"
 *                      },
 *                      {
 *                          "name" = "content_info",
 *                          "in" = "query",
 *                          "description" = "Patent Document ID",
 *                          "type" : "integer"
 *                      },
 *                  },
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get"
 *     },
 *     itemOperations={
 *         "get",
 *          "delete"={
 *              "normalization_context"={"groups"={"get-patents", "date"}}
 *          }
 *     }
 * )
 * @Vich\Uploadable()
 * @ORM\Entity(repositoryClass=PatentContentRepository::class)
 * @ApiFilter(SearchFilter::class, properties={
 *     "patent.id":"exact",
 *     "contentInfo.description":"ipartial"
 * })
 * @ApiFilter(OrderFilter::class, properties={"pageNumber"}, arguments={"orderParameterName"="order"})
 */
class PatentContent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"media_object_read"})
     */
    private $id;

    /**
     * @var string|null
     *
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @Groups({"media_object_read"})
     */
    public $contentUrl;

    /**
     * @var File|null
     *
     * @Assert\NotNull(groups={"media_object_create"})
     * @Vich\UploadableField(mapping="wims_patent_content", fileNameProperty="filePath")
     */
    public $file;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    public $filePath;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PatentDocument", inversedBy="contents")
     */
    private $contentInfo;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"media_object_read"})
     */
    private $pageNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patent", inversedBy="contents", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContentInfo(): ?PatentDocument
    {
        return $this->contentInfo;
    }

    public function setContentInfo(?PatentDocument $contentInfo): self
    {
        $this->contentInfo = $contentInfo;

        return $this;
    }

    public function getPageNumber(): ?int
    {
        return $this->pageNumber;
    }

    public function setPageNumber(int $pageNumber): self
    {
        $this->pageNumber = $pageNumber;

        return $this;
    }

    public function getPatent(): ?Patent
    {
        return $this->patent;
    }

    public function setPatent(?Patent $patent): self
    {
        $this->patent = $patent;

        return $this;
    }
}
