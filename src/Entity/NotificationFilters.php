<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationFiltersRepository")
 * @ORM\Table(schema="api", name="notification_filters")
 */
class NotificationFilters
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $companies = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $contacts = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $products = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $components = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="notificationFilters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $users;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getItems(string $type): ?array
    {
        switch ($type) {
            case 'companies':
                return $this->getCompanies();
            case 'products':
                return $this->getProducts();
            case 'components':
                return $this->getComponents();
            case 'contacts':
                return $this->getContacts();
            default:
                return null;
        }
    }

    public function setItems(string $type, array $items): self
    {
        switch ($type) {
            case 'companies':
                $this->setCompanies($items);
                break;
            case 'products':
                $this->setProducts($items);
                break;
            case 'components':
                $this->setComponents($items);
                break;
            case 'contacts':
                $this->setContacts($items);
                break;
        }

        return $this;
    }

    public function getCompanies(): ?array
    {
        return $this->companies;
    }

    public function setCompanies(?array $companies): self
    {
        $this->companies = $companies;

        return $this;
    }

    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    public function setContacts(?array $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    public function getProducts(): ?array
    {
        return $this->products;
    }

    public function setProducts(?array $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function getComponents(): ?array
    {
        return $this->components;
    }

    public function setComponents(?array $components): self
    {
        $this->components = $components;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }
}
