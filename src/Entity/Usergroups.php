<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Core\Annotation\ApiResource;


/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(
 *     schema="api",
 *     name="usergroups",
 *     indexes={
 *          @ORM\Index(name="idx_26517_idx_usergroup_adjacency_lookup", columns={"parent_id"}),
 *          @ORM\Index(name="idx_26517_idx_usergroup_nested_set_lookup", columns={"lft", "rgt"}),
 *          @ORM\Index(name="idx_26517_idx_usergroup_parent_title_lookup", columns={"parent_id", "title"}),
 *          @ORM\Index(name="idx_26517_idx_usergroup_title_lookup", columns={"title"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\UsergroupsRepository")
 * @ApiResource()
 */
class Usergroups
{
	public function __construct()
	{
		$this->users = new ArrayCollection();
	}

	/**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
    private $title;


	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\Users", mappedBy="groups")
	 * @ORM\JoinTable(
	 *     schema="api",
	 *     name="user_usergroup_map",
	 *     joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
	 *  )
	 */
    private $users;

	/**
	 * @Gedmo\TreeLeft()
	 * @ORM\Column(type="integer")
	 */
    private $lft;

	/**
	 * @Gedmo\TreeRight()
	 * @ORM\Column(type="integer")
	 */
    private  $rgt;

	/**
	 * @Gedmo\TreeParent
	 * @ORM\ManyToOne(targetEntity="Usergroups", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
	 */
	private $parent;

	/**
	 * @ORM\OneToMany(targetEntity="Usergroups", mappedBy="parent")
	 * @ORM\OrderBy({"lft" = "ASC"})
	 */
	private $children;

	public function getId()
	{
		return $this->id;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return Collection|Users[]
	 */
	public function getUsers()
	{
		return $this->users;
	}


	public function getRoot()
	{
		return $this->root;
	}

	public function setParent(?Usergroups $parent)
	{
		$this->parent = $parent;
	}

	public function getParent(): ?Usergroups
	{
		return $this->parent;
	}

}
