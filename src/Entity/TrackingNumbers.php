<?php

namespace App\Entity;

use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Table(schema="api", name="tracking_numbers")
 * @ORM\Entity(repositoryClass="App\Repository\TrackingNumbersRepository")
 */
class TrackingNumbers
{
	use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @SerializedName("trackingNumber")
     */
    private $trackingNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Invoices", inversedBy="trackingNumbers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $invoice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Carriers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $carrier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->trackingNumber;
    }

    public function setTrackingNumber(string $trackingNumber): self
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    public function getInvoice(): ?Invoices
    {
        return $this->invoice;
    }

    public function setInvoice(?Invoices $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function getCarrier(): ?Carriers
    {
        return $this->carrier;
    }

    public function setCarrier(?Carriers $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }
}
