<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use App\Traits\TimestampableCreateEntity;
use App\Traits\TimestampableUpdateEntity;
use App\Traits\UserCreateEntity;
use App\Traits\UserUpdateEntity;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"get-brands", "date"}}
 *          },
 *         "get_names"={
 *              "normalization_context"={"groups"={"get-brand-name"}},
 *               "path"="/brands/name",
 *               "method"="GET"
 *         },
 *          "post"={
 *              "denormalization_context"={"groups"={"add-brand"}},
 *              "security"="is_granted('ROLE_BRAND_EDIT')"
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"get-brand", "date"}}
 *          },
 *          "put"={
 *              "denormalization_context"={"groups"={"edit-brand"}},
 *               "security"="is_granted('ROLE_BRAND_EDIT')"
 *          }
 *     }
 * )
 * @ORM\Table(schema="api", name="brands")
 * @ORM\Entity(repositoryClass="App\Repository\BrandsRepository")
 * @UniqueEntity("name", message="The field name must be unique")
 * @ORM\HasLifecycleCallbacks()
 * @ApiFilter(OrderFilter::class, properties={"id", "name", "companiesAmount", "productsAmount"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(RangeFilter::class, properties={"companiesAmount", "productsAmount"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "id":"exact",
 *     "name":"ipartial",
 *     "companies":"exact",
 *     "companies.name":"ipartial",
 *     "companies.id":"exact",
 *     })
 */
class Brands
{

    use TimestampableCreateEntity, TimestampableUpdateEntity, UserCreateEntity, UserUpdateEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get-company","get-product", "get-products","get-brand","get-brands", "get-product-name"})
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(
     *     message="The field name cannot be empty"
     * )
     * @Groups({"get-company","get-product", "get-products","get-brand","get-brands","add-brand", "get-product-name"})
     */
    private $name;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Ubersearch\Brands", mappedBy="brand", orphanRemoval=true)
     */
    private $ubersearchIndex;

    /**
     * @Groups({"get-brand","get-brands","add-brand","edit-brand"})
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $originalName;

    /**
     * @Groups({"get-brand","get-brands","add-brand","edit-brand"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ownerName;

    /**
     * @Groups({"get-brand","get-brands","add-brand","edit-brand"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ownerOriginalName;

    /**
     * @Groups({"get-brand","get-brands","add-brand","edit-brand"})
     * @ORM\Column(type="date", nullable=true)
     */
    private $registeredAt;

    /**
     * @Groups({"get-brand","get-brands","add-brand","edit-brand"})
     * @ORM\Column(type="text", nullable=true)
     */
    private $logoLink;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Products", mappedBy="brand")
     * @Groups({"get-brand"})
     * @MaxDepth(1)
     */
    private $products;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     * @SerializedName("companiesAmount")
     * @Groups({"get-brand","get-brands"})
     */
    private $companiesAmount;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     * @SerializedName("productsAmount")
     * @Groups({"get-brand","get-brands"})
     */
    private $productsAmount;

    /**
     * @ORM\ManyToMany(targetEntity=Companies::class, mappedBy="brands")
     * @Groups({"get-brand","get-brands"})
     */
    private $companies;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->companies = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


    public function getProducts()
    {
        return $this->products;
    }


    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    public function setOriginalName(?string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getOwnerName(): ?string
    {
        return $this->ownerName;
    }

    public function setOwnerName(?string $ownerName): self
    {
        $this->ownerName = $ownerName;

        return $this;
    }

    public function getOwnerOriginalName(): ?string
    {
        return $this->ownerOriginalName;
    }

    public function setOwnerOriginalName(?string $ownerOriginalName): self
    {
        $this->ownerOriginalName = $ownerOriginalName;

        return $this;
    }

    public function getRegisteredAt(): ?DateTimeInterface
    {
        return $this->registeredAt;
    }

    public function setRegisteredAt(?DateTimeInterface $registeredAt): self
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    public function getLogoLink(): ?string
    {
        return $this->logoLink;
    }

    public function setLogoLink(?string $logoLink): self
    {
        $this->logoLink = $logoLink;

        return $this;
    }


    public function getCompaniesAmount(): ?int
    {
        return $this->companiesAmount;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function setCompaniesAmount(): self
    {
        $this->companiesAmount = $this->companies->count();

        return $this;
    }

    public function getProductsAmount(): ?int
    {
        return $this->productsAmount;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function setProductsAmount(): self
    {
        $this->productsAmount = $this->products->count();

        return $this;
    }

    /**
     * @return Collection|Companies[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(Companies $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->addBrand($this);
        }

        return $this;
    }

    public function removeCompany(Companies $company): self
    {
        if ($this->companies->removeElement($company)) {
            $company->removeBrand($this);
        }

        return $this;
    }

}
