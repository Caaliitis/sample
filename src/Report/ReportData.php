<?php


namespace App\Report;

use App\Repository\ProductsRepository;


final class ReportData
{

    private $format;

    private $entity;

    private $template;

    private $hasLogo;

    private $items;

    private $defaultImageType;

    private $productsRepository;


    public function __construct(ProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }


    public function getFormat(): string
    {
        return $this->format;
    }


    public function setFormat(string $format): void
    {
        $this->format = $format;
    }


    public function getHasLogo(): bool
    {
        return $this->hasLogo;
    }


    public function setHasLogo(bool $hasLogo): void
    {
        $this->hasLogo = $hasLogo;
    }


    public function getItems(): ?array
    {
        return $this->items;
    }


    public function setItems(array $items): void
    {
        $products = $this->productsRepository->findBy(['id' => $items]);

        $this->items = $products;
    }

    public function getDefaultImageType(): ?int
    {
        return $this->defaultImageType;
    }

    public function setDefaultImageType(?int $defaultImageType): void
    {
        $this->defaultImageType = $defaultImageType;
    }

    public function getEntity(): string
    {
        return $this->entity;
    }

    public function setEntity(string $entity): void
    {
        $this->entity = $entity;
    }

    public function getTemplate(): int
    {
        return $this->template;
    }

    public function setTemplate(int $template): void
    {
        $this->template = $template;
    }

}