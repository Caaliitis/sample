<?php


namespace App\Report;

use App\Entity\Report\GeneratedReportFiles;
use App\Report\Twig\TwigTemplate;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;


class PdfReport implements Report
{
    private $data;

    private $twigTemplate;

    private $generatedReport;

    private $path;

    public function generate(bool $hasContentTable = false): ?string
    {
        set_time_limit(480);
        ini_set("pcre.backtrack_limit", "150000000");

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mPdf = new Mpdf([
            'mode' => '',
            'format' => 'A4-L',
            'default_font_size' => 10,
            'default_font' => 'opensans',
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 27,
            'margin_bottom' => 10,
            'margin_header' => -1.5,
            'margin_footer' => 0,
            'fontDir' => array_merge($fontDirs, [
                $this->getPath() . '/public/reports/pdf/fonts',
            ]),
            'fontdata' => $fontData + [
                    'pfdpro' => [
                        'R' => "PFDinTextCondPro-MI.ttf",
                    ]
                ],
//			'debug' => true,
        ]);


        // LOAD a stylesheet
        $stylesheet = file_get_contents($this->getPath() . '/public/reports/pdf/css/doc.css');
        try {
            $mPdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text
        } catch (MpdfException $e) {
            return null;
        }

        //Generate table of content
        $mPdf->keepColumns = true;
        $mPdf->showImageErrors = true;

        //Generate Table Of Content
        if ($hasContentTable && null !== $this->getGeneratedReport()) {
            $tableContentHtml = $this->getTwig()->getTableContent($this->getGeneratedReport(), $this->getData());
            try {
                $mPdf->WriteHTML($tableContentHtml);
            } catch (MpdfException $e) {
                return null;
            }
        }

        if (null !== $this->getGeneratedReport()) {
            $contentHtml = $this->getTwig()->getFullContent($this->getGeneratedReport(), $this->getData());
            try {
                $mPdf->WriteHTML($contentHtml);
            } catch (MpdfException $e) {
                return null;
            }
        }

        if ($this->getData()->getItems() !== null) {
            foreach ($this->getData()->getItems() as $item) {

                $contentHtml = $this->getTwig()->getContent($item, $this->getData());
                try {
                    $mPdf->WriteHTML($contentHtml);
                } catch (MpdfException $e) {
                    return null;
                }
            }
        }

        try {
            return $mPdf->Output(null, Destination::STRING_RETURN);
        } catch (MpdfException $e) {
            return null;
        }
    }

    public function getData(): ReportData
    {
        return $this->data;
    }

    public function setData(ReportData $data)
    {
        $this->data = $data;
    }

    public function getGeneratedReport(): ?GeneratedReportFiles
    {
        return $this->generatedReport;
    }

    public function setGeneratedReport(GeneratedReportFiles $generatedReport)
    {
        $this->generatedReport = $generatedReport;
    }

    public function getTwig(): ?TwigTemplate
    {
        return $this->twigTemplate;
    }

    public function setTwig(?TwigTemplate $twigTemplate)
    {
        $this->twigTemplate = $twigTemplate;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path)
    {
        $this->path = $path;
    }
}