<?php


namespace App\Report;


use App\Entity\Features;
use App\Entity\ProductFeatureValues;
use App\Entity\Products;
use App\Entity\Types;
use App\Report\Twig\TwigTemplate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;


class XlsxReport implements Report
{

    const HEADER = ['ID', 'Name', 'Production Status', 'Release date', 'Company/Factory', 'Retail Price', 'Wholesale Price'];

    private $data;

    private $filesystem;

    private $factory;

    private $extraHeaders;

    private $extraDataClass;

    private $title;

    public function generate()
    {
        /** @var Spreadsheet $spreadsheet */
        $spreadsheet = $this->factory->createSpreadsheet();
        $activeSpreadsheet = $spreadsheet->getActiveSheet();
        $activeSpreadsheet->setTitle($this->getTitle());

        //Create Styles Array
        $styleArrayFirstRow = [
            'font' => [
                'bold' => true,
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => ['rgb' => 'C0C0C0']
            ]
        ];

        //Header
        $extraHeader = $this->getExtraHeaders();
        $activeSpreadsheet->fromArray(
            [array_merge(self::HEADER, $this->getExtraHeaderNames())],  // The data to set
            NULL
        );

        //Body
        $productsContentArray = $this->getExtraBody();

        $activeSpreadsheet->fromArray(
            $productsContentArray,  // The data to set
            NULL,
            'A2'
        );

        //Set style for a first row
        $highestColumn = $activeSpreadsheet->getHighestColumn();
        $activeSpreadsheet->getStyle('A1:' . $highestColumn . '1')->applyFromArray($styleArrayFirstRow);
        foreach ($this->excelColumnRange('A', $highestColumn) as $column) {
            $activeSpreadsheet->getColumnDimension($column)->setAutoSize(true);
        }


        $writerXlsx = $this->factory->createWriter($spreadsheet, 'Xlsx');

        $fileName = \str_replace('.', '', \uniqid('', true)) . ".xlsx";
        $tempFile = tempnam(sys_get_temp_dir(), $fileName);

        $writerXlsx->save($tempFile);

        return file_get_contents($tempFile);
    }

    public function getData(): ReportData
    {
        return $this->data;
    }

    public function setData(ReportData $data)
    {
        $this->data = $data;
    }

    public function getTwig(): ?TwigTemplate
    {
        // TODO: Implement getTwig() method.
    }

    public function setTwig(?TwigTemplate $twigTemplate)
    {
        // TODO: Implement setTwig() method.
    }

    public function getFilesystem()
    {
        return $this->filesystem;
    }

    public function setFilesystem($filesystem): void
    {
        $this->filesystem = $filesystem;
    }

    public function getFactory()
    {
        return $this->factory;
    }

    public function setFactory($factory): void
    {
        $this->factory = $factory;
    }

    public function getExtraHeaderNames(): array
    {
        return array_map(function ($item) {
            return $item["name"];
        }, $this->extraHeaders);
    }

    public function getExtraHeaders(): array
    {
        return $this->extraHeaders;
    }

    public function setExtraHeaders($extraHeaders): void
    {
        $this->extraHeaders = $extraHeaders;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    private function getExtraBody(): array
    {
        $class = $this->getExtraDataClass();
        $instance = new $class();

        if ($instance instanceof Types) {
            return $this->extraTypesBody();
        }

        if ($instance instanceof Features) {
            return $this->extraFeaturesBody();
        }

        return [];
    }

    private function extraTypesBody(): array
    {
        $extraHeader = $this->getExtraHeaders();

        return array_map(function ($item) use ($extraHeader) {

            $resultArray = [];
            if (!$item instanceof Products) {
                return $resultArray;
            }

            $resultArray[] = $item->getId();
            $resultArray[] = $item->getName();
            $resultArray[] = $item->getProductionStatusName();
            $resultArray[] = $item->getAppeared()->format('Y-m-d');
            $resultArray[] = $item->getCompanyName();
            $resultArray[] = $item->getRetailPrice();
            $resultArray[] = $item->getWholesalePrice();

            foreach ($extraHeader as $type) {
                $value = null;

                /** @var Types $pType */
                foreach ($item->getTypes() as $pType) {
                    if ($pType->getId() === $type['id']) {
                        $value = 'yes';
                    }
                }

                $resultArray[] = $value;
            }

            return $resultArray;

        }, $this->getData()->getItems(), $extraHeader);
    }

    private function extraFeaturesBody(): array
    {
        $extraHeader = $this->getExtraHeaders();

        return array_map(function ($item) use ($extraHeader) {

            $resultArray = [];

            if (!$item instanceof Products) {
                return $resultArray;
            }

            $resultArray[] = $item->getId();
            $resultArray[] = $item->getName();
            $resultArray[] = $item->getProductionStatusName();
            $resultArray[] = $item->getAppeared()->format('Y-m-d');
            $resultArray[] = $item->getCompanyName();
            $resultArray[] = $item->getRetailPrice();
            $resultArray[] = $item->getWholesalePrice();

            foreach ($extraHeader as $feature) {
                $value = null;

                /** @var ProductFeatureValues $pFeatureValue */
                foreach ($item->getProductFeatureValue() as $pFeatureValue) {

                    if ($pFeatureValue->getFeature()->getId() === $feature['id']) {

                        if ($pFeatureValue->getFeature()->getFieldType() === 'number') {
                            $value = $pFeatureValue->getValue();
                        } else {
                            ($value !== null) ? $value .= "; " . $pFeatureValue->getTextValue() : $value .= $pFeatureValue->getTextValue();
                        }
                    }

                }

                $resultArray[] = $value;
            }

            return $resultArray;

        }, $this->getData()->getItems(), $extraHeader);
    }


    public function getExtraDataClass(): string
    {
        return $this->extraDataClass;
    }

    public function setExtraDataClass(string $extraDataClass): void
    {
        $this->extraDataClass = $extraDataClass;
    }

    private function excelColumnRange($lower, $upper)
    {
        ++$upper;
        for ($i = $lower; $i !== $upper; ++$i) {
            yield $i;
        }
    }
}