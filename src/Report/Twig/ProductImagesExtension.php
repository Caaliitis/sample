<?php

namespace App\Report\Twig;

use App\Entity\Products;
use App\Report\Image\ProductRequiredImageArray;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;


class ProductImagesExtension extends AbstractExtension
{

    private $photoCollection;

    public function __construct(ProductRequiredImageArray $photoCollection)
    {
        $this->photoCollection = $photoCollection;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('productImages', [$this, 'productImages']),
        );
    }

    public function productImages(Products $product, ?int $typeId): ?array
    {
        return $this->photoCollection->getPaths($product, $typeId);
    }

}