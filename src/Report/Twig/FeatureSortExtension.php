<?php


namespace App\Report\Twig;

use Doctrine\Common\Collections\ArrayCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;


class FeatureSortExtension extends AbstractExtension
{

	public function getFilters()
	{
		return array(
			new TwigFilter('featureSort', array($this, 'sortFeatures')),
		);
	}


	public function sortFeatures($featuresValueCollection)
	{

		$iterator = $featuresValueCollection->getIterator();

		$iterator->uasort(function ($a, $b) {
			return ($a->getFeature()->getOrdering() < $b->getFeature()->getOrdering()) ? -1 : 1;
		});

        return new ArrayCollection(iterator_to_array($iterator));
	}
}