<?php

namespace App\Report\Twig;

use App\Entity\Products;
use App\Report\Image\ProductRequiredImage;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;


class ProductRequiredImageExtension extends AbstractExtension
{

	private $mainPhotoUrl;

	public function __construct(ProductRequiredImage $mainPhotoUrl)
	{
		$this->mainPhotoUrl = $mainPhotoUrl;
	}

	public function getFunctions()
	{
		return array(
			new TwigFunction('productRequiredImage', [$this, 'productRequiredImage']),
		);
	}


	public function productRequiredImage(Products $product, ?int $typeId): ?string
	{

		return $this->mainPhotoUrl->getPath($product, $typeId);

	}

}