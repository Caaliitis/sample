<?php


namespace App\Report\Twig;

use App\Entity\Report\GeneratedReportFiles;
use App\Report\ReportData;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class TwigTemplate
{

    const TWIG_PATH = 'report/templates/%s/%s/%d/%s.html.twig';


    protected $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getTableContent(GeneratedReportFiles $report, ReportData $data): ?string
    {
        $twigTemplate = sprintf(self::TWIG_PATH, $data->getFormat(), $data->getEntity(), $data->getTemplate(), 'table');

        try {
            return $this->twig->render($twigTemplate, ['report' => $report->getReport()]);
        } catch (LoaderError $e) {
            return null;
        } catch (RuntimeError $e) {
            return null;
        } catch (SyntaxError $e) {
            return null;
        }
    }

    public function getContent($item, ReportData $data): ?string
    {

        $twigTemplate = sprintf(self::TWIG_PATH, $data->getFormat(), $data->getEntity(), $data->getTemplate(), 'page');

        try {
            return $this->twig->render($twigTemplate, ['item' => $item, 'data' => $data]);
        } catch (LoaderError $e) {
            return null;
        } catch (RuntimeError $e) {
            return null;
        } catch (SyntaxError $e) {
            return null;
        }
    }

    public function getFullContent(GeneratedReportFiles $report, ReportData $data): ?string
    {

        $twigTemplate = sprintf(self::TWIG_PATH, $data->getFormat(), $data->getEntity(), $data->getTemplate(), 'content');


        try {
            return $this->twig->render($twigTemplate, ['reportFile' => $report]);
        } catch (LoaderError $e) {
            dd(11);
            return null;
        } catch (RuntimeError $e) {
            dd(22);
            return null;
        } catch (SyntaxError $e) {
            dd(33);
            return null;
        }
    }

}