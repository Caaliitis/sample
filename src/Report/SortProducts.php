<?php

namespace App\Report;

use Doctrine\Common\Collections\ArrayCollection;

class SortProducts
{

	public function sortProducts($productsCollection, $productOrderColumn)
	{

		$iterator = $productsCollection->getIterator();

		switch ($productOrderColumn)
		{
			case 1: //production status
				$iterator->uasort(function ($a, $b) {
					return ($a->getProductionStatus()->getOrdering() < $b->getProductionStatus()->getOrdering()) ? -1 : 1;
				});
				break;
			case 2: //appeared
				$iterator->uasort(function ($a, $b) {
					return ($a->getAppeared() > $b->getAppeared()) ? -1 : 1;
				});
				break;
			case 3: //created
				$iterator->uasort(function ($a, $b) {
					return ($a->getCreated() > $b->getCreated()) ? -1 : 1;
				});
				break;
			default:
				return $productsCollection;
		}

        return new ArrayCollection(iterator_to_array($iterator));
	}
}