<?php

namespace App\Report\Image;


use App\Entity\Products;
use App\Entity\Types;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class ProductRequiredImage
{

    private $entityManager;

    private $filesystem;

    private $parameterBag;

    private $baseUrl;

    private $logger;

    public function __construct(
        FilesystemInterface $filesystem,
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag,
        LoggerInterface $logger
    )
    {
        $this->entityManager = $entityManager;
        $this->filesystem = $filesystem;
        $this->parameterBag = $parameterBag;
        $this->baseUrl = $parameterBag->get('uploads_base_url');
        $this->logger = $logger;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function getPath(Products $product, ?int $typeId, bool $hasAbsoluteUrl = true): ?string
    {
        return $path = $this->path($product, $typeId, $hasAbsoluteUrl);
    }

    private function path(Products $product, ?int $typeId, bool $hasAbsoluteUrl): ?string
    {

        $defaultImg = $product->getDefaultImage();

        $baseUrl = $this->baseUrl;

        if($hasAbsoluteUrl === false){
            $baseUrl = '';
        }

        if ($typeId === null && $defaultImg === null) {
            return null;
        }


        if (
            $typeId !== null &&
            null !== $type = $this->entityManager->getRepository(Types::class)->find($typeId)
        ) {

            //get first media with needed tag
            foreach ($product->getMedia() as $image) {

                $typeImageUrl = $image->getFilePath() . $image->getSource();

                if ($image->getTypes()->contains($type) && $this->checkImg($typeImageUrl)) {
                    return $baseUrl . $image->getFilePath() . $image->getSource();
                }
            }
        }

        $this->logger->notice($defaultImg->getUrl());
        if ($defaultImg !== null &&
            $this->checkImg($defaultImg->getFilePath().$defaultImg->getSource())) {
            return $defaultImg->getUrl(); //return default img
        }

        return null;

    }


    private function checkImg(string $path): bool
    {
        $this->logger->notice($path);
        return $this->filesystem->has($path);
    }

}