<?php

namespace App\Report\Image;


use App\Entity\Products;
use App\Entity\Types;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class ProductRequiredImageArray
{

    private $entityManager;

    private $filesystem;

    private $parameterBag;

    private $baseUrl;

    public function __construct(
        FilesystemInterface $filesystem,
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag
    )
    {
        $this->entityManager = $entityManager;
        $this->filesystem = $filesystem;
        $this->parameterBag = $parameterBag;
        $this->baseUrl = $parameterBag->get('uploads_base_url');
    }

    public function getPaths(Products $product, ?int $typeId): ?array
    {
        return $path = $this->paths($product, $typeId);
    }

    private function paths(Products $product, ?int $typeId): ?array
    {

        if ($typeId === null) {
            return null;
        }

        $result = [];

        if (
            null !== $type = $this->entityManager->getRepository(Types::class)->find($typeId)
        ) {

            //get first media with needed tag
            foreach ($product->getMedia() as $image) {

                $typeImageUrl = $image->getFilePath() . $image->getSource();

                if ($image->getTypes()->contains($type) && $this->checkImg($typeImageUrl)) {
                    $result[] = $this->baseUrl . $image->getFilePath() . $image->getSource();
                }
            }
        }

        return $result;

    }


    private function checkImg(string $path): bool
    {
        return $this->filesystem->has($path);
    }

}