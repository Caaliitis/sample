<?php

namespace App\Report\Standing;


use App\Entity\Report\GeneratedReportFiles;
use App\Entity\ProductImages;
use App\Entity\Products;
use App\Report\SortProducts;
use Doctrine\Common\Collections\ArrayCollection;
use League\Flysystem\FilesystemInterface;
use PhpOffice\PhpPresentation\DocumentLayout;
use PhpOffice\PhpPresentation\Shape\Drawing\Base64;
use PhpOffice\PhpPresentation\Style\Bullet;
use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\Style\Color;
use PhpOffice\PhpPresentation\Style\Alignment;
use PhpOffice\PhpPresentation\IOFactory;

class PptxReport
{

	private $sortProducts;

	private $filesystem;

	public function __construct(SortProducts $sortProducts, FilesystemInterface $filesystem)
	{
		$this->sortProducts = $sortProducts;
		$this->filesystem   = $filesystem;
	}

	public function generateReport(GeneratedReportFiles $reportFile , string $path): bool
	{
		$report = $reportFile->getReport();

		set_time_limit(240);
		ini_set("pcre.backtrack_limit", "30000000");
		$columnElement = 41;
		$pageElement   = 123;
		$columnWidth   = 375;
		$columnStart   = 10;
		$pager         = 0;


		$objPHPPresentation = new PhpPresentation();
		$objPHPPresentation->getLayout()->setDocumentLayout(['cx' => 1125, 'cy' => 800], true)
			->setCX(1125, DocumentLayout::UNIT_PIXEL)
			->setCY(800, DocumentLayout::UNIT_PIXEL);

		$currentSlide = $objPHPPresentation->getActiveSlide();

		if ($reportFile->getCurrentPage() === 0)
		{

			//Table of content
			$shape        = $currentSlide->createRichTextShape()
				->setHeight(30)
				->setWidth(1125)
				->setOffsetX(10)
				->setOffsetY(10);

			$textRun = $shape->createTextRun('Table of contents');

			$textRun->getFont()->setBold(true)
				->setSize(18)->setName('Arial')
				->setColor(new Color('FF000000'));

			$shape->createBreak();

			$dColumnStart = $columnStart;

			$shape = $currentSlide->createRichTextShape()
				->setHeight(750)
				->setWidth($columnWidth)
				->setOffsetX($dColumnStart)
				->setOffsetY(40);


			foreach ($report->getSections() as $section)
			{

				$pager = $pager + 2; /////??????

				$shape->createBreak();
				$textRun = $shape->createTextRun($section->getName());
				$textRun->getFont()->setBold(true)
					->setSize(10)->setName('Arial')
					->setColor(new Color('FF000000'));
				$shape->createBreak();

				$sortedProducts = $this->sortProducts->sortProducts($section->getProducts(), $section->getOrdering());

				foreach ($sortedProducts as $product)
				{

					$textRun = $shape->createTextRun($product->getName());
					$textRun->getFont()
						->setSize(10)->setName('Arial')
						->setColor(new Color('FF000000'));
					$shape->createBreak();

					if ($pager % $pageElement == 0)
					{
						$dColumnStart = $columnStart;

						$currentSlide = $objPHPPresentation->createSlide();

						$shape = $currentSlide->createRichTextShape()
							->setHeight(30)
							->setWidth(1125)
							->setOffsetX(10)
							->setOffsetY(10);

						$textRun = $shape->createTextRun('Table of contents');

						$textRun->getFont()->setBold(true)
							->setSize(18)->setName('Arial')
							->setColor(new Color('FF000000'));


						$shape = $currentSlide->createRichTextShape()
							->setHeight(750)
							->setWidth($columnWidth)
							->setOffsetX($dColumnStart)
							->setOffsetY(40);

					}
					elseif ($pager % $columnElement == 0)
					{

						$dColumnStart = $dColumnStart + $columnWidth;
						$shape        = $currentSlide->createRichTextShape()
							->setHeight(750)
							->setWidth($columnWidth)
							->setOffsetX($dColumnStart)
							->setOffsetY(40);
					}

					$pager++;

				}

				if (($pager - 1) % $pageElement == 0)
				{
					$dColumnStart = $columnStart;

					$currentSlide = $objPHPPresentation->createSlide();

					$shape = $currentSlide->createRichTextShape()
						->setHeight(30)
						->setWidth(1125)
						->setOffsetX(10)
						->setOffsetY(10);

					$textRun = $shape->createTextRun('Table of contents');

					$textRun->getFont()->setBold(true)
						->setSize(18)->setName('Arial')
						->setColor(new Color('FF000000'));

					$shape = $currentSlide->createRichTextShape()
						->setHeight(750)
						->setWidth($columnWidth)
						->setOffsetX($dColumnStart)
						->setOffsetY(40);
				}
				elseif ($pager % $columnElement == 0)
				{

					$dColumnStart = $dColumnStart + $columnWidth;
					$shape        = $currentSlide->createRichTextShape()
						->setHeight(750)
						->setWidth($columnWidth)
						->setOffsetX($dColumnStart)
						->setOffsetY(40);
				}


			}
		}

		///CONTENT
		///
		$i = 0;

		foreach ($report->getSections() as $section)
		{

			if ($i <= $reportFile->getCurrentPage() + $reportFile->getStep() && $i >= $reportFile->getCurrentPage())
			{

				if (!$section->getIsHidden())
				{

					$currentSlide = $objPHPPresentation->createSlide();

					$shape = $currentSlide->createRichTextShape()
						->setHeight(150)
						->setWidth(1100)
						->setOffsetX(10)
						->setOffsetY(350);

					$textRun = $shape->createTextRun($section->getName());

					$textRun->getFont()->setBold(true)
						->setSize(30)->setName('Arial')
						->setColor(new Color('FF000000'));
				}
			}


			$sortedProducts = $this->sortProducts->sortProducts($section->getProducts(), $section->getOrdering());

			$objPHPPresentation = $this->productPage($objPHPPresentation, $sortedProducts, false, $this->filesystem, $i, $reportFile);

			$i = $i + $section->getProductQuantity();
		}

		//Create report

		$part = floor($reportFile->getCurrentPage() / $reportFile->getStep() + 1);

		$tempFile = sprintf('%s/var/reports/%s/report_%s_part.pptx', $path, $reportFile->getId(), $part);

		try
		{
			$oWriterPPTX = IOFactory::createWriter($objPHPPresentation, 'PowerPoint2007');
			$oWriterPPTX->save($tempFile);
		}
		catch (\Exception $exception)
		{
			return false;
		}

		return true;
	}


	protected function productPage(PhpPresentation $objPHPPresentation, $productCollection, $hasFirstSlide = false, FilesystemInterface $filesystem, int $i, GeneratedReportFiles $reportFile)
	{


		/** @var Products $product */
		foreach ($productCollection as $key => $product)
		{
			$i++;

			if ($i <= $reportFile->getCurrentPage() + $reportFile->getStep() && $i > $reportFile->getCurrentPage())
			{

				// Create slide
				if ($key == 0 && $hasFirstSlide)
				{
					$currentSlide = $objPHPPresentation->getActiveSlide();
				}
				else
				{
					$currentSlide = $objPHPPresentation->createSlide();
				}

				// Create a shape (TITLE)
				$shape = $currentSlide->createRichTextShape()
					->setHeight(40)
					->setWidth(495)
					->setOffsetX(30)
					->setOffsetY(40);
				$shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
				$textRun = $shape->createTextRun($product->getName());
				$textRun->getFont()->setBold(true)
					->setSize(17)->setName('Arial Narrow')->setItalic(true)
					->setColor(new Color('FF000000'));


				/** @var ProductImages $media */
				foreach ($product->getMedia() as $media)
				{
					$path = $media->getFilePath() . $media->getSource();

					if ($media->getDefaultImg() && $filesystem->has($path))
					{

						$mimeType = $filesystem->getMimetype($path);

						if ($mimeType === "image/jpeg")
						{
							$filePath = 'https://'.$media->getUrl();

							$shapeBase64 = new Base64();
							$imageData   = "data:image/jpeg;base64," . base64_encode(file_get_contents($filePath));

							$shapeBase64->setName('Product image')
								->setDescription('Image for ' . $product->getName())
								->setData($imageData)
								->setResizeProportional(false)
								->setHeight(662)
								->setWidth(495)
								->setOffsetX(40)
								->setOffsetY(100);

							$currentSlide->addShape($shapeBase64);
						}

					}

				}

				// Create a shape (description)
				$shape = $currentSlide->createRichTextShape()
					->setHeight(750)
					->setWidth(530)
					->setOffsetX(550)
					->setOffsetY(50);
				$shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
				$textRun = $shape->createTextRun($product->getDescriptionText());

				$textRun->getFont()
					->setSize(11)->setName('Arial')
					->setColor(new Color('FF000000'));
				$shape->createBreak();
				$shape->createBreak();

				// Create a shape (Manufacturer)
				$textRun = $shape->createTextRun("Manufacturer: ");

				$textRun->getFont()
					->setSize(11)->setName('Arial')->setBold(true)
					->setColor(new Color('FF000000'));

				$manufacturer = "";

				if ($product->getCompany())
				{
					$manufacturer .= $product->getCompany()->getName() . " ";
				}
				if ($product->getBrand())
				{
					$manufacturer .= $product->getBrand()->getName();
				}

				if (empty($manufacturer))
				{
					$manufacturer = "n/a";
				}


				$textRun = $shape->createTextRun($manufacturer);

				$textRun->getFont()
					->setSize(11)->setName('Arial')
					->setColor(new Color('FF000000'));

				$shape->createBreak();


				////////Development status

				$textRun = $shape->createTextRun("Development status: ");

				$textRun->getFont()
					->setSize(11)->setName('Arial')->setBold(true)
					->setColor(new Color('FF000000'));

				$textRun = $shape->createTextRun($product->getProductionStatus()->getName());

				$textRun->getFont()
					->setSize(11)->setName('Arial')
					->setColor(new Color('FF000000'));

				$shape->createBreak();

				////////Retail Price
				if (!empty($product->getRetailPrice()))
				{
					$textRun = $shape->createTextRun("Retail price, US$: ");

					$textRun->getFont()
						->setSize(11)->setName('Arial')->setBold(true)
						->setColor(new Color('FF000000'));


					$textRun = $shape->createTextRun($product->getRetailPrice());

					$textRun->getFont()
						->setSize(11)->setName('Arial')
						->setColor(new Color('FF000000'));

					$shape->createBreak();
				}

				////////Specification
				if (!empty($product->getSpecification()))
				{
					$textRun = $shape->createTextRun("Specification: ");

					$textRun->getFont()
						->setSize(11)->setName('Arial')->setBold(true)
						->setColor(new Color('FF000000'));


					//Main features
					foreach ($this->sortFeatures($product->getFeatures()) as $feature)
					{
						$shape->createParagraph()->getBulletStyle()->setBulletType(Bullet::TYPE_BULLET)->setBulletChar('- ');

						if ($feature->getFeature()->getFieldType() == 'number')
						{
							$textRun = $shape->createTextRun($feature->getFeature()->getName() . ": " . $feature->getValue());
						}

						if ($feature->getFeature()->getFieldType() == 'list')
						{
							$optionText = "";
							foreach ($feature->getFeature()->getOptions() as $option)
							{
								if ($option->getId() == $feature->getValue())
									$optionText .= $option->getValue() . "; ";
							}
							$textRun = $shape->createTextRun($feature->getFeature()->getName() . ": " . $optionText);
						}

						$textRun->getFont()
							->setSize(11)->setName('Arial')
							->setColor(new Color('FF000000'));
					}

					//Additional features

					$shape->createParagraph()->getBulletStyle()->setBulletType(Bullet::TYPE_BULLET)->setBulletChar('- ');

					$specification = html_entity_decode(strip_tags($product->getSpecification()));

					$pos = strrpos($specification, "\r\n");
					if ($pos != false)
					{
						$specification = substr($specification, 0, $pos);
					}
					$specification = preg_replace('/^\h*\v+/m', '', $specification);


					$textRun = $shape->createTextRun($specification);
					$textRun->getFont()
						->setSize(11)->setName('Arial')
						->setColor(new Color('FF000000'));

					$shape->createBreak();
				}

				////////UPS
				if (!empty($product->getUsp()))
				{
					$textRun = $shape->createTextRun("UPS: ");

					$textRun->getFont()
						->setSize(11)->setName('Arial')->setBold(true)
						->setColor(new Color('FF000000'));

					$textRun = $shape->createTextRun($product->getUspText());

					$textRun->getFont()
						->setSize(11)->setName('Arial')
						->setColor(new Color('FF000000'));
				}

				////////New Solution
				if (!empty($product->getNewSolution()))
				{
					$textRun = $shape->createTextRun("New solution: ");

					$textRun->getFont()
						->setSize(11)->setName('Arial')->setBold(true)
						->setColor(new Color('E51A4B'));

					$textRun = $shape->createTextRun($product->getNewSolutionText());

					$textRun->getFont()
						->setSize(11)->setName('Arial')
						->setColor(new Color('FF000000'));
				}

			}
		}

		return $objPHPPresentation;
	}


	protected function sortFeatures($featuresValueCollection)
	{

		$iterator = $featuresValueCollection->getIterator();

		$iterator->uasort(function ($a, $b) {
			return ($a->getFeature()->getOrdering() < $b->getFeature()->getOrdering()) ? -1 : 1;
		});

        return new ArrayCollection(iterator_to_array($iterator));
	}
}