<?php


namespace App\Report;


use App\Entity\Products;
use App\Report\Image\ProductRequiredImage;
use App\Report\Twig\TwigTemplate;
use Doctrine\Common\Collections\ArrayCollection;
use PhpOffice\PhpPresentation\DocumentLayout;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\Shape\Drawing\Base64;
use PhpOffice\PhpPresentation\Style\Alignment;
use PhpOffice\PhpPresentation\Style\Bullet;
use PhpOffice\PhpPresentation\Style\Color;

class PptxReport implements Report
{

    private $data;

    private $image;

    private $filesystem;

    public function generate()
    {
        $objPHPPresentation = new PhpPresentation();
        $objPHPPresentation->getLayout()->setDocumentLayout(['cx' => 1125, 'cy' => 800], true)
            ->setCX(1125, DocumentLayout::UNIT_PIXEL)
            ->setCY(800, DocumentLayout::UNIT_PIXEL);

        /** @var Products $product */
        foreach ($this->getData()->getItems() as $key => $product) {

            $currentSlide = $objPHPPresentation->getActiveSlide();
            if ($key) {
                $currentSlide = $objPHPPresentation->createSlide();
            }


            // Create a shape (TITLE)
            $shape = $currentSlide->createRichTextShape()
                ->setHeight(40)
                ->setWidth(495)
                ->setOffsetX(30)
                ->setOffsetY(40);
            $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
            $textRun = $shape->createTextRun($product->getName());
            $textRun->getFont()->setBold(true)
                ->setSize(17)->setName('Arial Narrow')->setItalic(true)
                ->setColor(new Color('FF000000'));

            $urlAbsolute = $this->image->getPath($product, $this->getData()->getDefaultImageType());
            $path = $this->image->getPath($product, $this->getData()->getDefaultImageType(), false);

            $mimeType = $this->filesystem->getMimetype($path);


            if ($mimeType === "image/jpeg") {

                $shapeBase64 = new Base64();
                $size = $this->getFileSize($urlAbsolute);
                $imageData = "data:image/jpeg;base64," . base64_encode(file_get_contents($urlAbsolute));

                $shapeBase64->setName('Product image')
                    ->setDescription('Image for ' . $product->getName())
                    ->setData($imageData)
                    ->setResizeProportional(false)
                    ->setHeight($size[1])
                    ->setWidth($size[0])
                    ->setOffsetX(40)
                    ->setOffsetY(100);

                $currentSlide->addShape($shapeBase64);
            }


            // Create a shape (description)
            $shape = $currentSlide->createRichTextShape()
                ->setHeight(750)
                ->setWidth(530)
                ->setOffsetX(550)
                ->setOffsetY(50);
            $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
            $textRun = $shape->createTextRun($product->getDescriptionText());

            $textRun->getFont()
                ->setSize(11)->setName('Arial')
                ->setColor(new Color('FF000000'));
            $shape->createBreak();
            $shape->createBreak();

            // Create a shape (Manufacturer)
            $textRun = $shape->createTextRun("Manufacturer: ");

            $textRun->getFont()
                ->setSize(11)->setName('Arial')->setBold(true)
                ->setColor(new Color('FF000000'));

            $manufacturer = "";

            if ($product->getCompany()) {
                $manufacturer .= $product->getCompany()->getName() . " ";
            }
            if ($product->getBrand()) {
                $manufacturer .= $product->getBrand()->getName();
            }

            if (empty($manufacturer)) {
                $manufacturer = "n/a";
            }


            $textRun = $shape->createTextRun($manufacturer);

            $textRun->getFont()
                ->setSize(11)->setName('Arial')
                ->setColor(new Color('FF000000'));

            $shape->createBreak();


            //Production status

            $textRun = $shape->createTextRun("Development status: ");

            $textRun->getFont()
                ->setSize(11)->setName('Arial')->setBold(true)
                ->setColor(new Color('FF000000'));

            $textRun = $shape->createTextRun($product->getProductionStatus()->getName());

            $textRun->getFont()
                ->setSize(11)->setName('Arial')
                ->setColor(new Color('FF000000'));

            $shape->createBreak();

            //Retail Price
            if (!empty($product->getRetailPrice())) {
                $textRun = $shape->createTextRun("Retail price, US$: ");

                $textRun->getFont()
                    ->setSize(11)->setName('Arial')->setBold(true)
                    ->setColor(new Color('FF000000'));


                $textRun = $shape->createTextRun($product->getRetailPrice());

                $textRun->getFont()
                    ->setSize(11)->setName('Arial')
                    ->setColor(new Color('FF000000'));

                $shape->createBreak();
            }

            ////////Specification
            if (!empty($product->getSpecification())) {
                $textRun = $shape->createTextRun("Specification: ");

                $textRun->getFont()
                    ->setSize(11)->setName('Arial')->setBold(true)
                    ->setColor(new Color('FF000000'));


                //Main features
                foreach ($this->sortFeatures($product->getFeatures()) as $feature) {
                    $shape->createParagraph()->getBulletStyle()->setBulletType(Bullet::TYPE_BULLET)->setBulletChar('- ');

                    if ($feature->getFeature()->getFieldType() == 'number') {
                        $textRun = $shape->createTextRun($feature->getFeature()->getName() . ": " . $feature->getValue());
                    }

                    if ($feature->getFeature()->getFieldType() == 'list') {
                        $optionText = "";
                        foreach ($feature->getFeature()->getOptions() as $option) {
                            if ($option->getId() == $feature->getValue())
                                $optionText .= $option->getValue() . "; ";
                        }
                        $textRun = $shape->createTextRun($feature->getFeature()->getName() . ": " . $optionText);
                    }

                    $textRun->getFont()
                        ->setSize(11)->setName('Arial')
                        ->setColor(new Color('FF000000'));
                }

                //Additional features

                $shape->createParagraph()->getBulletStyle()->setBulletType(Bullet::TYPE_BULLET)->setBulletChar('- ');

                $specification = html_entity_decode(strip_tags($product->getSpecification()));

                $pos = strrpos($specification, "\r\n");
                if ($pos != false) {
                    $specification = substr($specification, 0, $pos);
                }
                $specification = preg_replace('/^\h*\v+/m', '', $specification);


                $textRun = $shape->createTextRun($specification);
                $textRun->getFont()
                    ->setSize(11)->setName('Arial')
                    ->setColor(new Color('FF000000'));

                $shape->createBreak();
            }

            //USP
            if (!empty($product->getUsp())) {
                $textRun = $shape->createTextRun("USP: ");

                $textRun->getFont()
                    ->setSize(11)->setName('Arial')->setBold(true)
                    ->setColor(new Color('FF000000'));

                $textRun = $shape->createTextRun($product->getUspText());

                $textRun->getFont()
                    ->setSize(11)->setName('Arial')
                    ->setColor(new Color('FF000000'));
            }

            //New Solution
            if (!empty($product->getNewSolution())) {
                $textRun = $shape->createTextRun("New solution: ");

                $textRun->getFont()
                    ->setSize(11)->setName('Arial')->setBold(true)
                    ->setColor(new Color('E51A4B'));

                $textRun = $shape->createTextRun($product->getNewSolutionText());

                $textRun->getFont()
                    ->setSize(11)->setName('Arial')
                    ->setColor(new Color('FF000000'));
            }


        }

        //Create temporary file

        $fileName = \str_replace('.', '', \uniqid('', true)) . ".pptx";
        $tempFile = tempnam(sys_get_temp_dir(), $fileName);

        $oWriterPPTX = IOFactory::createWriter($objPHPPresentation, 'PowerPoint2007');
        $oWriterPPTX->save($tempFile);

        return file_get_contents($tempFile);
    }

    public function getData(): ReportData
    {
        return $this->data;
    }

    public function setData(ReportData $data)
    {
        $this->data = $data;
    }

    public function getTwig(): ?TwigTemplate
    {
        // TODO: Implement getTwig() method.
    }

    public function setTwig(?TwigTemplate $twigTemplate)
    {
        // TODO: Implement setTwig() method.
    }

    protected function sortFeatures($featuresValueCollection)
    {

        $iterator = $featuresValueCollection->getIterator();

        $iterator->uasort(function ($a, $b) {
            return ($a->getFeature()->getOrdering() < $b->getFeature()->getOrdering()) ? -1 : 1;
        });

        return new ArrayCollection(iterator_to_array($iterator));
    }

    private function getFileSize(string $path): array
    {
        $info = getimagesize($path);
        $y = ceil(495 / $info[0] * $info[1]);

        return [495, $y];
    }

    public function getImage(): ProductRequiredImage
    {
        return $this->image;
    }

    public function setImage(ProductRequiredImage $image): void
    {
        $this->image = $image;
    }


    public function getFilesystem()
    {
        return $this->filesystem;
    }

    public function setFilesystem($filesystem): void
    {
        $this->filesystem = $filesystem;
    }
}