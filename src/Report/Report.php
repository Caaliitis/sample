<?php


namespace App\Report;


use App\Report\Twig\TwigTemplate;

interface Report
{
    public function generate();

    public function getData(): ReportData;

    public function setData(ReportData $data);

    public function getTwig(): ?TwigTemplate;

    public function setTwig(?TwigTemplate $twigTemplate);

}