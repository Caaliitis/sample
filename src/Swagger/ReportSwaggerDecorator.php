<?php


namespace App\Swagger;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class ReportSwaggerDecorator implements NormalizerInterface
{
    private $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        $docs['components']['schemas']['ReportLink'] = [
            'type' => 'object',
            'properties' => [
                'link' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ];

        $docs['components']['schemas']['ReportData'] = [
            'type' => 'object',
            'properties' => [
                'format' => [
                    'type' => 'string',
                    'example' => 'pdf',
                ],
                'entity' => [
                    'type' => 'string',
                    'example' => 'products',
                ],
                'template' => [
                    'type' => 'int',
                    'example' => 1,
                ],
                'hasLogo' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'defaultImageType' => [
                    'type' => 'int',
                    'example' => 1,
                ],
                'items' => [
                    'type' => 'array',
                    'items'=> ['type' => 'integer'],
                    'example' => [1 ,3],
                ],
            ],
        ];

        $tokenDocumentation = [
            'paths' => [
                '/api/report_generator' => [
                    'post' => [
                        'tags' => ['Report Generator'],
                        'operationId' => 'postCredentialsItem',
                        'summary' => 'Generate report and get report link',
                        'requestBody' => [
                            'description' => 'Generate Report and Get Link',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/ReportData',
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Get Report Link',
                                'content' => [
                                    'application/json' => [
                                        'schema' => [
                                            '$ref' => '#/components/schemas/ReportLink',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return array_merge_recursive($docs, $tokenDocumentation);
    }
}