<?php

namespace App\Controller;

use App\Entity\Features;
use App\Entity\Types;
use App\Report\Image\ProductRequiredImage;
use App\Report\PdfReport;
use App\Report\PptxReport;
use App\Report\ReportData;
use App\Report\Twig\TwigTemplate;
use App\Report\XlsxReport;
use App\Repository\ProductsRepository;
use League\Flysystem\FileExistsException;
use League\Flysystem\FilesystemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Yectep\PhpSpreadsheetBundle\Factory;

class ReportController extends AbstractController
{

    private $twigTemplate;

    private $parameterBag;

    private $temporaryReportFilesystem;

    private $productRequiredImage;

    private $filesystem;

    private $spreadsheetFactory;

    public function __construct(
        TwigTemplate $twigTemplate,
        ParameterBagInterface $parameterBag,
        FilesystemInterface $temporaryReportFilesystem,
        FilesystemInterface $filesystem,
        ProductRequiredImage $productRequiredImage,
        Factory $spreadsheetFactory
    )
    {
        $this->twigTemplate = $twigTemplate;
        $this->parameterBag = $parameterBag;
        $this->temporaryReportFilesystem = $temporaryReportFilesystem;
        $this->productRequiredImage = $productRequiredImage;
        $this->filesystem = $filesystem;
        $this->spreadsheetFactory = $spreadsheetFactory;
    }

    /**
     * @Route(
     *     "/api/report_generator",
     *      name="report-generator",
     *      methods={"POST"}
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ProductsRepository $productsRepository
     * @return JsonResponse
     */
    public function report(
        Request $request,
        SerializerInterface $serializer,
        ProductsRepository $productsRepository
    )
    {
        $reportData = new ReportData($productsRepository);

        /*
         * Validation??
         */

        $serializer->deserialize(
            $request->getContent(),
            ReportData::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $reportData]
        );

        switch ($reportData->getFormat()) {
            case 'pdf':
                $fileAbsolutePath = $this->pdfReport($reportData);
                break;
            case 'pptx':
                $fileAbsolutePath = $this->pptxReport($reportData);
                break;
            case 'xlsx':
                $fileAbsolutePath = $this->xlsxReport($reportData);
                break;
            default:
                $fileAbsolutePath = null;
        }

        if ($fileAbsolutePath === null) {
            return new JsonResponse(['detail' => 'A report can\'t been generated. Please try later.'], 400);
        }

        return new JsonResponse([
            'link' => $fileAbsolutePath
        ], 200);

    }

    private function pdfReport(ReportData $reportData): ?string
    {
        $report = new PdfReport();

        $report->setData($reportData);
        $report->setPath($this->parameterBag->get('kernel.project_dir'));
        $report->setTwig($this->twigTemplate);


        $fileContent = $report->generate();

        if (null === $fileContent) {
            return null;
        }

        $filePath = '/files/' . \str_replace('.', '', \uniqid('', true)) . ".pdf";
        $fileAbsolutePath = $this->parameterBag->get('temporary_report_base_url') . $filePath;

        try {
            $this->temporaryReportFilesystem->write($filePath, $fileContent);
        } catch (FileExistsException $e) {
            return null;
        }

        return $fileAbsolutePath;
    }

    private function pptxReport(ReportData $reportData): ?string
    {
        $report = new PptxReport();

        $report->setData($reportData);
        $report->setFilesystem($this->filesystem);
        $report->setImage($this->productRequiredImage);

        $fileContent = $report->generate();

        if (null === $fileContent) {
            return null;
        }

        $filePath = '/files/' . \str_replace('.', '', \uniqid('', true)) . ".pptx";
        $fileAbsolutePath = $this->parameterBag->get('temporary_report_base_url') . $filePath;

        try {
            $this->temporaryReportFilesystem->write($filePath, $fileContent);
        } catch (FileExistsException $e) {
            return null;
        }

        return $fileAbsolutePath;
    }


    private function xlsxReport(ReportData $reportData): ?string
    {
        $report = new XlsxReport();

        $report->setData($reportData);
        $report->setFilesystem($this->filesystem);
        $report->setFactory($this->spreadsheetFactory);

        switch ($report->getData()->getTemplate()) {
            case 1:
                $extraHeaders = $this->getDoctrine()->getRepository(Types::class)->findAllInArray();
                $report->setTitle("WIMS Products with Types");
                $report->setExtraDataClass(Types::class);
                break;
            case 2:
                $extraHeaders = $this->getDoctrine()->getRepository(Features::class)->findAllInArray();
                $report->setTitle("WIMS Products with Features");
                $report->setExtraDataClass(Features::class);
                break;
            default:
                return null;
        }

        $report->setExtraHeaders($extraHeaders);

        $fileContent = $report->generate();

        if (null === $fileContent) {
            return null;
        }

        $filePath = '/files/' . \str_replace('.', '', \uniqid('', true)) . ".xlsx";
        $fileAbsolutePath = $this->parameterBag->get('temporary_report_base_url') . $filePath;

        try {
            $this->temporaryReportFilesystem->write($filePath, $fileContent);
        } catch (FileExistsException $e) {
            return null;
        }

        return $fileAbsolutePath;
    }

}
