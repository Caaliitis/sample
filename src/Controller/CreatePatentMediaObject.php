<?php


namespace App\Controller;

use App\Entity\Patent;
use App\Entity\PatentContent;
use App\Entity\PatentDocument;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreatePatentMediaObject
{
    public function __invoke(Request $request, EntityManagerInterface $manager): PatentContent
    {

        $patentDocument = null;
        $patentId = $request->query->getInt('patent_id');
        $pageNumber = $request->query->getInt('page_number');

        $patentInfo = $request->query->getInt('content_info');

        if ($pageNumber === 0) {
            throw new BadRequestHttpException('Page Number is empty');
        }

        if ($patentInfo !== 0) {
            $patentDocument = $manager->getRepository(PatentDocument::class)->find($patentInfo);
        }

        if ($patentInfo !== 0 && $patentDocument === null) {
            throw new BadRequestHttpException('Patent document doesnt exist');
        }

        $patent = $manager->getRepository(Patent::class)->find($patentId);

        if (null === $patent) {
            throw new BadRequestHttpException('Patent doesnt exist yet');
        }

        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $mediaObject = new PatentContent();
        $mediaObject->setPatent($patent);
        $mediaObject->setPageNumber($pageNumber);
        $mediaObject->setContentInfo($patentDocument);
        $mediaObject->file = $uploadedFile;

        return $mediaObject;
    }
}