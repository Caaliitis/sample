<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Products;
use App\Entity\Report\GeneratedReportFiles;
use App\Entity\Report\ReportSections;
use App\Entity\Report\Reports;
use App\Entity\Users;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;


class StandingReportController extends AbstractController
{

    private $validator;

    private $serializer;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer
    )
    {
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     name="post_report_section",
     *     path="/api/reports/{id}/sections",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_collection_operation_name"="post_report_section"
     *     }
     * )
     * @ParamConverter("report")
     * @param Reports|null $report
     * @param Request $request
     * @return ReportSections
     */
    public function createReportSection(
        ?Reports $report,
        Request $request
    )
    {
        $this->checkData($report);

        $section = new ReportSections();
        $section->setReport($report);

        return $this->saveSection($request->getContent(), $section);

    }

    /**
     * @Route(
     *     name="put_report_section",
     *     path="/api/reports/{id}/sections/{section_id}",
     *     methods={"PUT", "PATCH"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_item_operation_name"="put_report_section"
     *     }
     * )
     * @ParamConverter("report", options={"id" = "id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @param Reports|null $report
     * @param ReportSections|null $section
     * @param Request $request
     * @return ReportSections
     */
    public function changeReportSection(
        ?Reports $report,
        ?ReportSections $section,
        Request $request
    )
    {
        $this->checkData($report, $section);

        return $this->saveSection($request->getContent(), $section);

    }

    /**
     * @Route(
     *     name="delete_report_section",
     *     path="/api/reports/{id}/sections/{section_id}",
     *     methods={"DELETE"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_item_operation_name"="delete_report_section"
     *     }
     * )
     * @ParamConverter("report", options={"id" = "id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @param Reports|null $report
     * @param ReportSections|null $section
     * @return JsonResponse
     */
    public function deleteReportSection(
        ?Reports $report,
        ?ReportSections $section
    ): JsonResponse
    {
        $this->checkData($report);

        $em = $this->getDoctrine()->getManager();

        $em->remove($section);
        $em->flush();

        return new JsonResponse(null, 204);
    }


    /**
     * @Route(
     *     name="post_section_products",
     *     path="/api/reports/{id}/sections/{section_id}/add",
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_item_operation_name"="post_section_products"
     *     }
     * )
     * @ParamConverter("report", options={"id" = "id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @param Reports|null $report
     * @param ReportSections|null $section
     * @param Request $request
     * @return ReportSections
     */
    public function addProductsToSection(
        ?Reports $report,
        ?ReportSections $section,
        Request $request
    )
    {
        $this->checkData($report, $section);

        $currentProducts = $section->getProducts();
        $currentProductsArray = [];

        /** @var Products $product */
        foreach ($currentProducts as $product) {
            $currentProductsArray[] = $product->getId();
        }

        $requestProducts = json_decode($request->getContent(), true);


        if ($requestProducts === null || array_key_exists('products', $requestProducts) === false) {
            throw new BadRequestHttpException("Wrong data format");
        }

        $requestArray["products"] = array_unique(array_merge($requestProducts["products"], $currentProductsArray), SORT_NUMERIC);

        return $this->saveSection(json_encode($requestArray), $section);
    }

    /**
     * @Route(
     *     name="delete_section_products",
     *     path="/api/reports/{id}/sections/{section_id}/delete",
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_item_operation_name"="delete_section_products"
     *     }
     * )
     * @ParamConverter("report", options={"id" = "id"})
     * @ParamConverter("section", options={"id" = "section_id"})
     * @param Reports|null $report
     * @param ReportSections|null $section
     * @param Request $request
     * @return ReportSections
     */
    public function deleteProductsFromSection(
        ?Reports $report,
        ?ReportSections $section,
        Request $request
    )
    {
        $this->checkData($report, $section);

        $currentProducts = $section->getProducts();
        $currentProductsArray = [];

        /** @var Products $product */
        foreach ($currentProducts as $product) {
            $currentProductsArray[] = $product->getId();
        }

        $requestProducts = json_decode($request->getContent(), true);


        if ($requestProducts === null || array_key_exists('products', $requestProducts) === false) {
            throw new BadRequestHttpException("Wrong data format");
        }

        $requestArray["products"] = array_diff($currentProductsArray, $requestProducts["products"]);

        return $this->saveSection(json_encode($requestArray), $section);
    }

    /**
     * @Route(
     *     name="clone_report",
     *     path="/api/reports/{id}/clone",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_collection_operation_name"="clone_report"
     *     }
     * )
     * @ParamConverter("report")
     * @param Reports|null $report
     * @return Reports|null
     */
    public function copy(?Reports $report)
    {
        $this->checkData($report);

        $newReport = clone $report;

        /** @var Users $user */
        $user = $this->getUser();

        $newReport->setId(null);
        $newReport->setCreated($user);
        $newReport->setName($report->getName() . ' (Copy-' . uniqid() . ')');

        $newReport->addSections($report->getSections());

        $em = $this->getDoctrine()->getManager();
        $em->persist($newReport);
        $em->flush();

        return $newReport;
    }


    /**
     * @Route(
     *     name="clean_report",
     *     path="/api/reports/{id}/sections/clean",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_collection_operation_name"="clean_report"
     *     }
     * )
     * @ParamConverter("report")
     * @param Reports|null $report
     * @return JsonResponse
     */
    public function clean(?Reports $report)
    {
        $this->checkData($report);

        $report->cleanSections();

        $em = $this->getDoctrine()->getManager();
        $em->persist($report);
        $em->flush();

        return new JsonResponse(null, 204);
    }


    /**
     * @Route(
     *     name="generate_report",
     *     path="/api/reports/{id}/generate",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Reports::class,
     *         "_api_collection_operation_name"="generate_report"
     *     }
     * )
     * @ParamConverter("report")
     * @param Reports|null $report
     * @param Request $request
     * @return JsonResponse
     */
    public function generate(?Reports $report, Request $request)
    {
        $this->checkData($report);

        $reportFile = new GeneratedReportFiles();
        $reportFile->setReport($report);

        $this->saveGeneratedReport($request->getContent(), $reportFile);

        return new JsonResponse(null, 204);
    }


    private function saveSection(string $request, ReportSections $section): ReportSections
    {
        $this->serializer->deserialize(
            $request,
            ReportSections::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $section]
        );

        $errors = $this->validator->validate($section);

        if ($errors === null) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($section);
            $em->flush();
        }

        return $section;
    }

    private function saveGeneratedReport(string $request, GeneratedReportFiles $generatedReport): GeneratedReportFiles
    {
        $this->serializer->deserialize(
            $request,
            GeneratedReportFiles::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $generatedReport]
        );

        $errors = $this->validator->validate($generatedReport);

        if ($errors === null) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($generatedReport);
            $em->flush();
        }

        return $generatedReport;
    }

    private function checkData(?Reports $report, $section = false)
    {
        if (!$report) {
            throw new NotFoundHttpException("The report not found");
        }

        if (false === $section) {
            return;
        }

        if (!$section) {
            throw $this->createNotFoundException('The section not found');
        }
    }

}
