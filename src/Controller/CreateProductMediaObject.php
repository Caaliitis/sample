<?php


namespace App\Controller;

use App\Entity\ProductImages;
use App\Entity\Products;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateProductMediaObject
{
    public function __invoke(Request $request, EntityManagerInterface $manager): ProductImages
    {

        $productImage = null;
        $productId = $request->query->getInt('product_id');
        $isDefault = $request->query->getBoolean('is_default');


        $product = $manager->getRepository(Products::class)->find($productId);

        if (null === $product) {
            throw new BadRequestHttpException('Product doesnt exist yet');
        }

        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $mediaObject = new ProductImages();
        $mediaObject->setProduct($product);
        $mediaObject->setDefaultImg($isDefault);
        $mediaObject->setState(1);

        /** @var UploadedFile $uploadedFile */
        $mediaObject->file = $uploadedFile;
        $mediaObject->setFileSize($uploadedFile->getSize());
        $mediaObject->setMimeType($uploadedFile->getMimeType());

        return $mediaObject;
    }
}