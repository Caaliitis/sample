<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Service\Ubersearch;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UbersearchController
 * @package App\Controller
 *
 * @property Ubersearch $ubersearch
 * @property ParameterBagInterface $parameterBag
 */
class UbersearchController extends AbstractController
{

	private $parameterBag;
	private $ubersearch;

	public function __construct(ParameterBagInterface $parameterBag, Ubersearch $ubersearch)
	{
		$this->parameterBag = $parameterBag;
		$this->ubersearch = $ubersearch;
	}

	/**
	 * @Route(
	 *     name="search",
	 *     path="/api/search",
	 *     methods={"GET"},
	 * )
	 * @param Request $request
	 * @throws \Exception
	 * @return Response
	 */
	public function search(Request $request): Response
	{

		$searchConfig = $this->parameterBag->get('ubersearch');

		//array of entities to search
		$searchEntities = $request->query->get('entities');
		$extendedFieldsSearch = $request->query->get('extended_fields_search', 0);

		if (!$searchEntities or !count($searchEntities)) {
			throw new BadRequestHttpException('List of search entities not provided. Valid entities: ' . implode(', ', array_keys($searchConfig)));
		}

		if (!$searchEntities) {
			$searchEntities = implode(', ', array_keys($searchConfig));
		}

		$searchText = $request->query->get('q');

		if (!strlen($searchText)) {
			throw new BadRequestHttpException('Search query');
		}

		$depth = $request->query->get('depth', 0);

		$limit = $request->query->get('count', 100);
		//page numbers start with 1, not with 0
		$offset = ($request->query->get('page', 1) - 1) * $limit;

		foreach ($searchEntities as $searchEntity) {
			if (!isset($searchConfig[$searchEntity])) {
				throw new NotFoundHttpException('Undefined search entity: ' . $searchEntity . '; Valid entities: ' . implode(', ', array_keys($searchConfig)));
			}
		}

		$parsedText = Ubersearch::parseSearchString($searchText);

		$response = new Response(json_encode($this->ubersearch->unionSearch($parsedText, $searchEntities, $limit, $offset, $extendedFieldsSearch, $depth)));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}
}