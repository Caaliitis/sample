<?php


namespace App\Controller;

use App\Entity\Companies;
use App\Entity\CompanyImages;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateCompanyMediaObject
{
    public function __invoke(Request $request, EntityManagerInterface $manager): CompanyImages
    {

        $productImage = null;
        $companyId = $request->query->getInt('company_id');
        $isDefault = $request->query->getBoolean('is_default');

        $company = $manager->getRepository(Companies::class)->find($companyId);

        if (null === $company) {
            throw new BadRequestHttpException('Company doesnt exist yet');
        }

        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $mediaObject = new CompanyImages();
        $mediaObject->setCompany($company);
        $mediaObject->setDefaultImg($isDefault);
        $mediaObject->setState(1);

        /** @var UploadedFile $uploadedFile */
        $mediaObject->file = $uploadedFile;
        $mediaObject->setFileSize($uploadedFile->getSize());
        $mediaObject->setMimeType($uploadedFile->getMimeType());

        return $mediaObject;
    }
}