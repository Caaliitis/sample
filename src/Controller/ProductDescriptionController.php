<?php

namespace App\Controller;

use App\Entity\Features;
use App\Entity\ProductFeatureValues;
use App\Entity\Products;
use App\Entity\Types;
use App\Repository\BrandsRepository;
use App\Repository\CompaniesRepository;
use App\Repository\FeaturesRepository;
use App\Repository\ProductionStatusesRepository;
use App\Repository\ProductsRepository;
use App\Repository\TypesRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductDescriptionController extends AbstractController
{

    /** @var UserRepository $userRepository */
    private $userRepository;
    /**
     * @var ProductsRepository
     */
    private $productsRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var string[]
     */
    private $descriptionKeys = [
        "heater-material",
        "metal-of-coil",
        "brands",
        "companies",
        "types",
        "production-statuses",
        "pod-capacity",
        "battery-capacity",
        "output-power",
        "output-voltage",
        "heater-resistance"
    ];

    /**
     * @var string[]
     */
    private $relationOptions = ["brands", "companies", "types", "production-statuses"];
    /**
     * @var FeaturesRepository
     */
    private $featuresRepo;

    /**
     * OrderController constructor.
     * @param UserRepository $usersRepository
     * @param ProductsRepository $productsRepository
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $parameterBag
     * @param FeaturesRepository $featuresRepo
     */
    public function __construct(
        UserRepository $usersRepository,
        ProductsRepository $productsRepository,
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag,
        FeaturesRepository $featuresRepo
    )
    {
        $this->userRepository = $usersRepository;
        $this->productsRepository = $productsRepository;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->featuresRepo = $featuresRepo;
    }

    /**
     * @Route(
     *     name="post_product_draft_description",
     *     path="/api/products/{id}/draft_description",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Products::class,
     *         "_api_collection_operation_name"="post_product_draft_description"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function description(int $id, Request $request): JsonResponse
    {
        $content = json_decode($request->getContent(), true);

        $product = $this->productsRepository->find($id);

        if (!$product instanceof Products) {
            return new JsonResponse(
                ['error' => 'Product not found!'], 404);
        }

        foreach ($content as $key => $value) {
            if (!in_array($key, $this->descriptionKeys)) {
                continue;
            }

            // do relationships ManyToOne
            if (in_array($key, $this->relationOptions)) {
                if ($this->parseRelationshipOptions($product, $key, $value)) continue;
            }

            // do features
            if ($this->parseFeatureOptions($product, $key, $value)) {
                continue;
            }

            new JsonResponse("Error in product feature value lookup", 500);
        }

        $this->entityManager->flush();

        return new JsonResponse(
            null,
            204
        );
    }

    /**
     * @param Products $product
     * @param int $key
     * @param $value
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function parseRelationshipOptions(Products $product, int $key, $value): bool
    {
        $className = str_replace(" ", "", ucwords(str_replace("-", " ", $key)));

        if ($product->{"get" . $className}()) {
            return true;
        }

        /** @var BrandsRepository|CompaniesRepository|TypesRepository|ProductionStatusesRepository $repo */
        $repo = $this->entityManager->getRepository("\App\Entity\\" . $className);

        $item = $repo->searchByLowercaseTitle($value);

        if ($item instanceof Types) {
            $product->{"add" . $className}($item);
            $this->entityManager->persist($product);
            return true;
        }

        $product->{"set" . $className}($item);
        $this->entityManager->persist($product);
        return true;
    }

    /**
     * @param Products $product
     * @param int $key
     * @param $value
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function parseFeatureOptions(Products $product, int $key, $value): bool
    {
        // check feature base
        $keyInWords = str_replace("-", "", $key);
        $feature = $this->featuresRepo->searchByLowercaseTitle($keyInWords);
        if (!$feature instanceof Features) return true;


        // search in existing product feature values
        $identicalProductFeatureValues = $product->getProductFeatureValue()->filter(function (ProductFeatureValues $featureValues, $value, $featureId) {
            return $featureValues->getTextValue() == $value && $featureValues->getFeatureId() == $featureId;
        });
        if ($identicalProductFeatureValues) return true;


        $similarProductFeatureValues = $product->getProductFeatureValue()->filter(function (ProductFeatureValues $featureValues, $value, $featureId) {
            return $featureValues->getTextValue() != $value && $featureValues->getFeatureId() == $featureId;
        });

        if ($similarProductFeatureValues) {
            if ($similarProductFeatureValues->count() != 1) {
                return true;
            }

            $this->updateExistingFeatureValue($similarProductFeatureValues, $value);
            return true;
        }

        // if I dont have specific feature on product, then create it
        $hasSpecificProductFeatureValue = $product->getProductFeatureValue()->filter(function (ProductFeatureValues $featureValues, $featureId) {
            return $featureValues->getFeatureId() == $featureId;
        });
        if ($hasSpecificProductFeatureValue->count()) return false;


        $this->createProductFeatureValue($product, $feature, $value);

        return true;
    }

    /**
     * @param $input
     * @param $value
     */
    private function updateExistingFeatureValue($input, $value)
    {
        foreach ($input as $similarProductFeatureValue) {
            /** @var Features $baseFeature */
            $baseFeature = $similarProductFeatureValue->getFeature();

            if ((bool)count($baseFeature->getValues())) { // has options values
                $featureValues = $baseFeature->getValues();

                $optionKey = array_search($value, $featureValues);
                if ($optionKey !== false) {
                    $similarProductFeatureValue->setValue($optionKey);
                    $this->entityManager->persist($similarProductFeatureValue);
                }

                continue;
            }
            $similarProductFeatureValue->setValue($value);
            $this->entityManager->persist($similarProductFeatureValue);
        }
    }

    /**
     * @param Products $product
     * @param Features $feature
     * @param $value
     */
    private function createProductFeatureValue(Products $product, Features $feature, $value)
    {
        $productFeatureValue = new ProductFeatureValues();
        $productFeatureValue->setValue($value);
        $productFeatureValue->setFeature($feature);
        $productFeatureValue->setProduct($product);
        $this->entityManager->persist($productFeatureValue);
    }

}
