<?php


namespace App\Controller;


use Google\Cloud\Core\Exception\BadRequestException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\TrademarkMessenger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class TrademarkMessengerController extends AbstractController
{
	/**
	 * @Route(
	 *     name="trademark_messenger_checker",
	 *     path="/api/trademark_messengers/checker",
	 *     methods={"POST"},
	 *     defaults={
	 *         "_api_resource_class"=TrademarkMessenger::class,
	 *         "_api_collection_operation_name"="post_checker"
	 *     }
	 * )
	 * @throws BadRequestException
	 */
	public function checker( Request $request, SerializerInterface $serializer ): TrademarkMessenger
	{
		$data = json_decode( $request->getContent() );

		if ( ! key_exists( 'page', $data ) ||
		     ! key_exists( 'page', $data ) ||
		     ! key_exists( 'count', $data ) ) {
			throw new BadRequestException( 'Bad Request' );
		}

		$tmMessenger = $this->getDoctrine()->getRepository( TrademarkMessenger::class )->findOneBy(
			[
				'page'    => $data->page,
				'context' => $data->context,
				'count'   => $data->count,
			]
		);

		if ( null === $tmMessenger ) {
			$tmMessenger = new TrademarkMessenger();
		}

		$serializer->deserialize( $request->getContent(), TrademarkMessenger::class, 'json', [ AbstractNormalizer::OBJECT_TO_POPULATE => $tmMessenger ] );

		return $tmMessenger;
	}
}