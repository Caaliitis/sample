<?php


namespace App\Controller;

use App\Entity\News;
use App\Entity\NewsImage;
use App\Entity\Trademark;
use App\Entity\TrademarkImage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateNewsMediaObject
{
	public function __invoke( Request $request, EntityManagerInterface $manager ): NewsImage
	{
		$newsId = $request->query->getInt( 'news_id' );
		$intro = $request->query->getBoolean( 'intro' );

		$newsArticle = $manager->getRepository( News::class )->find( $newsId );

		if ( null === $newsArticle ) {
			throw new BadRequestHttpException( 'News doesnt exist yet' );
		}

		$uploadedFile = $request->files->get( 'file' );
		if ( ! $uploadedFile ) {
			throw new BadRequestHttpException( '"file" is required' );
		}

		$mediaObject = new NewsImage();
		$mediaObject->setIntro($intro);
		$mediaObject->setNews( $newsArticle );
		$mediaObject->file = $uploadedFile;

		return $mediaObject;
	}
}