<?php

namespace App\Controller;

use App\Entity\Usergroups;
use App\Mailer\MailGunMailer;
use App\Service\RecaptchaValidator;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Entity\Users;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AuthController extends AbstractController
{

    /** @var UserRepository $userRepository */
    private $userRepository;
    /** @var MailGunMailer */
    private $mailer;
    /** @var RecaptchaValidator */
    private $recaptchaValidator;

    /**
     * AuthController Constructor
     *
     * @param UserRepository $usersRepository
     * @param MailGunMailer $mailer
     */
    public function __construct(
        UserRepository $usersRepository,
        MailGunMailer $mailer,
        RecaptchaValidator $recaptchaValidator
    )
    {
        $this->userRepository = $usersRepository;
        $this->mailer = $mailer;
        $this->recaptchaValidator = $recaptchaValidator;
    }


    /**
     * @Route(
     *     name="user_registration",
     *     path="/api/users/register",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Users::class,
     *         "_api_collection_operation_name"="post_registration"
     *     }
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function register(Request $request, SerializerInterface $serializer, EntityManagerInterface $em)
    {
        try {
            $this->recaptchaValidator->validateAndLogOnRegistration($request);
        } catch (BadRequestHttpException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 400);
        }

        $json = $request->getContent();

        /** @var Users $user */
        $user = $serializer->deserialize($json, Users::class, 'json');

        $existUserInSystem = $this->getDoctrine()->getRepository(Users::class)->findOneBy(["email" => $user->getEmail()]);

        if (null !== $existUserInSystem) {
            return new JsonResponse(
                ['error' => 'the requested user exists in system'], 400);
        }

        //User only sent an access request and automatically are blocked
        $user->setUsername($user->getEmail());
        $user->setBlock(true);
        $user->setPassword('empty');
        $user->setRegisterdate(new DateTimeImmutable());

        try {
            $em->persist($user);
            $em->flush();
        } catch (Exception $exception) {
            return new JsonResponse(
                ['error' => 'Invalid data'], 400);
        }

        return new JsonResponse(
            null,
            204
        );
    }

    /**
     * @Route(
     *     name="user_registration",
     *     path="/api/users/register_shop",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Users::class,
     *         "_api_collection_operation_name"="post_registration"
     *     }
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function registerForShop(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ParameterBagInterface $parameterBag)
    {
        try {
            $this->recaptchaValidator->validateAndLogOnRegistration($request);
        } catch (BadRequestHttpException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 400);
        }

        $json = $request->getContent();

        /** @var Users $user */
        $user = $serializer->deserialize($json, Users::class, 'json');

        //Needs 2 groups for VaporFly User
        $groupShop = $this->getDoctrine()->getRepository(Usergroups::class)->findOneBy(["title" => "ROLE_SHOP"]);
        $groupProduct = $this->getDoctrine()->getRepository(Usergroups::class)->findOneBy(["title" => "ROLE_PRODUCTS"]);

        $existUserInSystem = $this->getDoctrine()->getRepository(Users::class)->findOneBy(["email" => $user->getEmail()]);

        if (null !== $existUserInSystem) {
            return new JsonResponse(
                ['error' => 'the requested user exists in system'], 400);
        }


        if (null === $groupShop || null === $groupProduct) {
            return new JsonResponse(
                ['error' => 'Invalid data'], 400);
        }

        $user->setUsername($user->getEmail());
        $user->setBlock(false);
        $user->addGroup($groupShop);
        $user->addGroup($groupProduct);
        $user->setRegisterdate(new DateTimeImmutable());
        $password = base64_encode(random_bytes(10));
        $user->setNewPassword($password);

        try {
            $em->persist($user);
            $em->flush();
        } catch (Exception $exception) {
            return new JsonResponse(
                ['error' => 'Invalid data'], 400);
        }

        try {

            $email = $parameterBag->get('vaporfly_manager_email');
            $emailFrom = $parameterBag->get('vaporfly_email_from');

            //Send email for lead;
            $this->mailer->sendEmail(
                $user->getEmail(),
                "Registration completed!",
                "vaporfly-new-user-password.html.twig",
                [
                    "email" => $user->getEmail(),
                    "password" => $password
                ],
                'alternative',
                'Vaporfly',
                $emailFrom
            );

            //Send email for manager;
            $this->mailer->sendEmail(
                $email,
                "A new user has been registered!",
                "vaporfly-new-user-password.html.twig",
                [
                    "email" => $user->getEmail(),
                    "password" => null
                ],
                'alternative',
                'Vaporfly',
                $emailFrom
            );
        } catch (Exception $e) {
            return new JsonResponse(["error" => "Email not sent", 500]);
        }

        return new JsonResponse(
            $user,
            201
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     * @Route(path="/register",name="register_local",methods={"POST"})
     *
     */
    public function registerLocal(Request $request): Response
    {
        $newUserData['email'] = $request->get('username');
        $newUserData['password'] = $request->get('password');

        $user = $this->userRepository->createNewUser($newUserData);

        return new Response(sprintf('User %s successfully created', $user->getUsername()));
    }

    /**
     * @Route(
     *     name="get_user_profile",
     *     path="/api/users/profile",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=Users::class
     *     }
     * )
     * @return JsonResponse
     */
    public function profile()
    {
        /**
         * @var Users $user
         */
        $user = $this->getUser();

        return new JsonResponse(
            [
                'name' => $user->getName(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'registerDate' => $user->getRegisterdate(),
                'lastVisitDate' => $user->getLastvisitdate(),
                'lastResetTime' => $user->getLastresettime(),
                'expirtyDate' => $user->getExpiryDate(),
                'phone' => $user->getPhone(),
                'company' => $user->getCompany(),
                'groups' => $user->getGroupsArray(),
                'ui' => $user->getUiSettings()
            ],
            200
        );
    }

    /**
     * @Route(
     *     name="update_user_profile",
     *     path="/api/users/profile",
     *     methods={"patch"},
     *     defaults={
     *         "_api_resource_class"=Users::class
     *     }
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function updateProfile(Request $request, SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $json = json_decode($request->getContent());

        if (!$json) {
            return new JsonResponse(
                ['error' => 'Invalid data'], 400);
        }

        /**
         * @var Users $user
         */
        $user = $this->getUser();

        foreach ($json as $key => $value) {
            $user->{'set' . ucfirst($key)}($value);
        }

        try {
            $em->persist($user);
            $em->flush();
        } catch (Exception $e) {
            return new JsonResponse(
                ['error' => $e->getMessage()], $e->getCode());
        }

        return new JsonResponse(
            [
                'name' => $user->getName(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'registerDate' => $user->getRegisterdate(),
                'lastVisitDate' => $user->getLastvisitdate(),
                'lastResetTime' => $user->getLastresettime(),
                'expirtyDate' => $user->getExpiryDate(),
                'phone' => $user->getPhone(),
                'company' => $user->getCompany(),
                'groups' => $user->getGroupsArray(),
                'ui' => $user->getUiSettings()
            ],
            200
        );
    }

    /**
     * @Route("/docs/login", name="docs_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {

        if ($this->getUser()) {
            //return $this->redirectToRoute('api_doc');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/docs_login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
}
