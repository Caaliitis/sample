<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Mime\MimeTypes;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Products;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ProductReportController extends AbstractController
{

	private $parameterBag;
	private $apiUrl;

	public function __construct(ParameterBagInterface $parameterBag)
	{
		$this->parameterBag = $parameterBag;
		$this->apiUrl = $this->parameterBag->get('wims_api_url');
	}


	/**
	 * @Route(
	 *     name="products_post_report",
	 *     path="/api/products/report",
	 *     methods={"POST"},
	 *     defaults={
	 *         "_api_resource_class"=Products::class,
	 *         "_api_collection_operation_name"="post_report"
	 *     }
	 * )
	 */
	public function generateReport(Request $request)
	{
		$reportResponse = $this->getReport($request->getContent());

		if (null === $reportResponse)
		{
			throw new BadRequestHttpException();
		}

		if (200 !== $reportResponse->getStatusCode())
		{
			throw new BadRequestHttpException();
		}

		$headers = $reportResponse->getHeaders();


		$mimeType = new MimeTypes();
		$exts     = $mimeType->getExtensions($headers['content-type'][0]);
		$fileName = 'WIMS-report-' . date('Y-m-d_h-i-s') . '.' . $exts[0];


		$response = new Response($reportResponse->getContent());

		$disposition = HeaderUtils::makeDisposition(
			HeaderUtils::DISPOSITION_ATTACHMENT,
			$fileName
		);

		$response->headers->set('Content-Disposition', $disposition);
		$response->headers->set('Content-Type', $headers['content-type'][0]);


		return $response;
	}


	private function requestToken(): ?string
	{

		$client = HttpClient::create();

		$response = $client->request('POST', $this->apiUrl . 'tokens',
			[
				'auth_basic' =>
					[
						$this->parameterBag->get('wims_api_user'),
						$this->parameterBag->get('wims_api_password')
					]
			]
		);

		if (200 !== $response->getStatusCode())
		{
			return null;
		}

		$content = $response->getContent();

		$valuesArray = json_decode($content, true);

		if (!key_exists('token', $valuesArray))
		{
			return null;
		}

		return $valuesArray['token'];

	}

	private function getReport(string $text): ?ResponseInterface
	{
		$data = json_decode($text);

		if (!$data)
		{
			return null;
		}

		switch ($data->format)
		{
			case "pdf":
				$url = $this->apiUrl . 'files/pdf/template/' . $data->template;
				break;
			case "presentation":
				$url = $this->apiUrl . 'files/report/presentation/template/1';
				break;
			case "sheets-types":
				$url = $this->apiUrl . 'files/report/spreadsheet/template/1/report.xls';
				break;
			case "sheets-features":
				$url = $this->apiUrl . 'files/report/spreadsheet/template/2/report.xls';
				break;
			default:
				return null;
		}


		$client = HttpClient::create();

		/** @var ResponseInterface $response */
		$response = $client->request('POST', $url,
			[
				'auth_bearer' => $this->requestToken(),
				'body'        => $text
			]
		);

		return $response;
	}


}