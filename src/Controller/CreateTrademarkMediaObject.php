<?php


namespace App\Controller;

use App\Entity\Trademark;
use App\Entity\TrademarkImage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateTrademarkMediaObject
{
	public function __invoke( Request $request, EntityManagerInterface $manager ): TrademarkImage
	{
		$trademarkId = $request->query->getInt( 'trademark_id' );

		$trademark = $manager->getRepository( Trademark::class )->find( $trademarkId );

		if ( null === $trademark ) {
			throw new BadRequestHttpException( 'Trademark doesnt exist yet' );
		}

		$uploadedFile = $request->files->get( 'file' );
		if ( ! $uploadedFile ) {
			throw new BadRequestHttpException( '"file" is required' );
		}

		$mediaObject = new TrademarkImage();
		$mediaObject->setTrademark( $trademark );
		$mediaObject->file = $uploadedFile;

		return $mediaObject;
	}
}