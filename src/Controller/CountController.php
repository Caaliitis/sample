<?php


namespace App\Controller;


use App\Entity\Companies;
use App\Entity\Components;
use App\Entity\Contacts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Products;

class CountController extends AbstractController
{

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="products_get_count_graph",
	 *     path="/api/products/count_graph",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Products::class,
	 *         "_api_collection_operation_name"="products_get_count_graph"
	 *     }
	 * )
	 */
	public function getProductsCount()
	{
		return $this->getCount( Products::class );
	}

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="components_get_count_graph",
	 *     path="/api/components/count_graph",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Components::class,
	 *         "_api_collection_operation_name"="get_count_graph"
	 *     }
	 * )
	 */
	public function getComponentsCount()
	{
		return $this->getCount( Components::class );
	}

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="companies_get_count_graph",
	 *     path="/api/companies/count_graph",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Companies::class,
	 *         "_api_collection_operation_name"="get_count_graph"
	 *     }
	 * )
	 */
	public function getCompaniesCount()
	{
		return $this->getCount( Companies::class );
	}

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="contacts_get_count_graph",
	 *     path="/api/contacts/count_graph",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Contacts::class,
	 *         "_api_collection_operation_name"="contacts_get_count_graph"
	 *     }
	 * )
	 */
	public function getContactsCount()
	{
		return $this->getCount( Contacts::class );
	}

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="products_total_count",
	 *     path="/api/products/total_items",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Products::class,
	 *         "_api_collection_operation_name"="products_total_count"
	 *     }
	 * )
	 */
	public function getProductTotalCount( Request $request )
	{
		$draft = $request->query->getBoolean( 'draft' );

		$entityManager = $this->getDoctrine()->getManager();

		$resultArray = [
			'total' => $entityManager->getRepository( Products::class )->totalCount($draft)
		];

		return new JsonResponse( $resultArray );
	}

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="component_total_count",
	 *     path="/api/components/total_items",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Components::class,
	 *         "_api_collection_operation_name"="components_total_count"
	 *     }
	 * )
	 */
	public function getComponentTotalCount()
	{
		$entityManager = $this->getDoctrine()->getManager();

		$resultArray = [
			'total' => $entityManager->getRepository( Components::class )->totalCount()
		];

		return new JsonResponse( $resultArray );
	}

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="companies_total_count",
	 *     path="/api/companies/total_items",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Companies::class,
	 *         "_api_collection_operation_name"="companies_total_count"
	 *     }
	 * )
	 */
	public function getCompaniesTotalCount()
	{
		$entityManager = $this->getDoctrine()->getManager();

		$resultArray = [
			'total' => $entityManager->getRepository( Companies::class )->totalCount()
		];

		return new JsonResponse( $resultArray );
	}

	/**
	 * @return JsonResponse
	 * @Route(
	 *     name="contacts_total_count",
	 *     path="/api/contacts/total_items",
	 *     methods={"GET"},
	 *     defaults={
	 *         "_api_resource_class"=Contacts::class,
	 *         "_api_collection_operation_name"="contacts_total_count"
	 *     }
	 * )
	 */
	public function getContactsTotalCount()
	{
		$entityManager = $this->getDoctrine()->getManager();

		$resultArray = [
			'total' => $entityManager->getRepository( Contacts::class )->totalCount()
		];

		return new JsonResponse( $resultArray );
	}

	/**
	 * @param $class
	 *
	 * @return JsonResponse
	 */
	public function getCount( $class )
	{
		$repo = $this->getDoctrine()->getRepository( $class );

		$data = $repo->getCountsPerMonth();

		return new JsonResponse( $data );
	}


    /**
     * @return JsonResponse
     * @Route(
     *     name="vaporfly_products_total_count",
     *     path="/api/products/vaporfly/total_items",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=Products::class,
     *         "_api_collection_operation_name"="vaporfly_products_total_count"
     *     }
     * )
     */
    public function getVaporflyProductTotalCount( Request $request )
    {
        $draft = $request->query->getBoolean( 'draft' );

        $entityManager = $this->getDoctrine()->getManager();

        $resultArray = [
            'total' => $entityManager->getRepository( Products::class )->vaporflyTotalCount($draft)
        ];

        return new JsonResponse( $resultArray );
    }


}