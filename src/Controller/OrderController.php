<?php

namespace App\Controller;

use App\Entity\ProductOrder;
use App\Entity\Products;
use App\Entity\Vaporfly\VaporflyOrder;
use App\Repository\OrderStatusRepository;
use App\Repository\ProductAnalysisOrderRepository;
use App\Repository\ProductOrderRepository;
use App\Repository\ProductsRepository;
use App\Repository\UserRepository;
use App\Repository\VaporflyOrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{

    /** @var UserRepository $userRepository */
    private $userRepository;
    /**
     * @var ProductsRepository
     */
    private $productsRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var OrderStatusRepository
     */
    private $orderStatusRepository;
    /**
     * @var ProductOrderRepository
     */
    private $orderRepository;
    /**
     * @var ProductAnalysisOrderRepository
     */
    private $analysisOrderRepository;
    /**
     * @var VaporflyOrderRepository
     */
    private $vaporflyProductOrderRepository;

    /**
     * OrderController constructor.
     * @param UserRepository $usersRepository
     * @param ProductsRepository $productsRepository
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $parameterBag
     * @param OrderStatusRepository $orderStatusRepository
     * @param ProductOrderRepository $orderRepository
     * @param ProductAnalysisOrderRepository $analysisOrderRepository
     * @param VaporflyOrderRepository $vaporflyProductOrderRepository
     */
    public function __construct(
        UserRepository $usersRepository,
        ProductsRepository $productsRepository,
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag,
        OrderStatusRepository $orderStatusRepository,
        ProductOrderRepository $orderRepository,
        ProductAnalysisOrderRepository $analysisOrderRepository,
        VaporflyOrderRepository $vaporflyProductOrderRepository
    )
    {
        $this->userRepository = $usersRepository;
        $this->productsRepository = $productsRepository;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;

        $this->orderStatusRepository = $orderStatusRepository;
        $this->orderRepository = $orderRepository;
        $this->analysisOrderRepository = $analysisOrderRepository;
        $this->vaporflyProductOrderRepository = $vaporflyProductOrderRepository;
    }

    /**
     * @Route(
     *     name="post_products_order",
     *     path="/api/products/{id}/order",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Products::class,
     *         "_api_collection_operation_name"="post_products_order"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws TransportExceptionInterface
     */
    public function register(int $id, Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());

        $product = $this->productsRepository->find($id);

        if (null === $product) {
            return new JsonResponse(
                ['error' => 'Product not found!'], 404);
        }

        $productOrder = new ProductOrder();
        $productOrder->setProduct($product);
        if (property_exists($content, 'comment')) {
            $productOrder->setComment($content->comment);
        }
        if (property_exists($content, 'quantity') && $content->quantity > 0) {
            $productOrder->setQuantity($content->quantity);
        } else {
            $productOrder->setQuantity(1);
        }

        $this->entityManager->persist($productOrder);
        $this->entityManager->flush();

        return new JsonResponse(
            null,
            204
        );
    }

    /**
     * @Route(
     *     name="put_change_product_order_status",
     *     path="/api/products/order/{id}/status/{status_id}",
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=Products::class,
     *         "_api_collection_operation_name"="put_change_product_order_status"
     *     }
     * )
     * @param int $id
     * @param int $status_id
     * @return JsonResponse
     */
    public function changeOrderStatus(int $id, int $status_id): JsonResponse
    {

        $order = $this->orderRepository->find($id);
        $orderStatus = $this->orderStatusRepository->find($status_id);
        $canUseInOrder = $orderStatus->getUseInProductOrder();

        if (null === $order || null === $orderStatus || false === $canUseInOrder) {
            return new JsonResponse(
                ['error' => 'Order not found!'], 404);
        }

        $order->setStatus($orderStatus);

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return new JsonResponse(
            null,
            204
        );
    }


    /**
     * @Route(
     *     name="put_change_vaporfly_order_status",
     *     path="/api/vaporfly_orders/{id}/status/{status_id}",
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=VaporflyOrder::class,
     *         "_api_collection_operation_name"="put_change_vaporfly_order_status"
     *     }
     * )
     * @param int $id
     * @param int $status_id
     * @return JsonResponse
     */
    public function changeVaporflyOrderStatus(int $id, int $status_id): JsonResponse
    {

        $order = $this->vaporflyProductOrderRepository->find($id);
        $orderStatus = $this->orderStatusRepository->find($status_id);
        $canUseInOrder = $orderStatus->getUseInProductOrder();

        if (null === $order || null === $orderStatus || false === $canUseInOrder) {
            return new JsonResponse(
                ['error' => 'Order not found!'], 404);
        }

        $order->setStatus($orderStatus);

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return new JsonResponse(
            null,
            204
        );
    }

    /**
     * @Route(
     *     name="put_product_analysis_order_status",
     *     path="/api/products/analysis-order/{id}/status/{status_id}",
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=Products::class,
     *         "_api_collection_operation_name"="put_product_analysis_order_status"
     *     }
     * )
     * @param int $id
     * @param int $status_id
     * @return JsonResponse
     */
    public function changeAnalysisOrderStatus(int $id, int $status_id): JsonResponse
    {

        $order = $this->analysisOrderRepository->find($id);
        $orderStatus = $this->orderStatusRepository->find($status_id);
        $canUseInOrder = $orderStatus->getUseInAnalysisOrder();

        if (null === $order || null === $orderStatus || false === $canUseInOrder) {
            return new JsonResponse(
                ['error' => 'Order not found!'], 404);
        }

        $order->setStatus($orderStatus);

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return new JsonResponse(
            null,
            204
        );
    }

    /**
     * @Route("/change-order-status", name="change_order_status")
     * @param Request $request
     * @return Response
     */
    public function changeOrderStatusForm(Request $request): Response
    {
        $orderId = $request->query->getInt('orderId');
        $futureStatusId = $request->query->getInt('futureStatusId');
        $futureStatusText = $request->query->get('futureStatusText');
        $typeId = $request->query->getInt('typeId');

        if (empty($orderId) || empty($futureStatusId) || empty($futureStatusText) || empty($typeId)) {
            throw $this->createNotFoundException('The order does not exist');
        }


        return $this->render('order/status.html.twig', []);
    }

}
