<?php

namespace App\EventListener;

use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class LoginListener
 * @package App\EventListener
 */
class LoginListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * LoginListener constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param JWTCreatedEvent $event
     * @throws \Exception
     */
    public function onLexikjwtauthenticationOnjwtcreated(JWTCreatedEvent $event)
    {
        /** @var Users $user */
        $user = $event->getUser();

        if(null !== $user->getVisitDate()){
            $user->setLastvisitdate($user->getVisitDate());
        }

        $user->setVisitDate();

        $this->em->persist($user);
        $this->em->flush();
    }
}