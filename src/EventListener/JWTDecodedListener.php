<?php


namespace App\EventListener;

use App\Entity\Users;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class JWTDecodedListener
{

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param ParameterBagInterface $parameterBag
     * @param UserRepository $userRepository
     */
    public function __construct(ParameterBagInterface $parameterBag, UserRepository $userRepository)
    {
        $this->parameterBag = $parameterBag;
        $this->userRepository = $userRepository;
    }


    /**
     * @param JWTDecodedEvent $event
     *
     * @return void
     */
    public function onJWTDecoded(JWTDecodedEvent $event)
    {

        $payload = $event->getPayload();

        /** @var Users $user */
        $user = $this->userRepository->findOneBy(['username' => $payload['username']]);
        $roles = $user->getRoles();


        if (!in_array('ROLE_SHOP', $roles) && preg_match($this->parameterBag->get('shop_host'), $payload['host']) ) {
            $event->markAsInvalid();
        }
    }
}