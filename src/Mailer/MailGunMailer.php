<?php

namespace App\Mailer;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;


class MailGunMailer
{

    private $mailgun_sender;
    private $parameterBag;
    private $mailer;


    public function __construct(
        ParameterBagInterface $parameterBag,
        MailerInterface $mailer
    )
    {
        $this->parameterBag = $parameterBag;
        $this->mailer = $mailer;
        $this->mailgun_sender = $this->parameterBag->get('email_from');
    }


    public function sendEmail(
        string $to_email,
        string $subject,
        string $template,
        array $templateData = [],
        string $transport = 'main',
        string $fromName = 'WIMS',
        ?string $sender = null
    ): bool
    {
        $loader = new FilesystemLoader($this->parameterBag->get('kernel.project_dir') . '/templates');
        $twig = new Environment($loader);

        if(null !== $sender){
            $this->mailgun_sender = $sender;
        }

        try {
            $email = (new Email())
                ->from(Address::fromString(sprintf('%s <%s>', $fromName, $this->mailgun_sender)))
                ->to($to_email)
                ->replyTo($this->mailgun_sender)
                ->subject($subject)
                ->html(
                    $twig->render(
                        'emails/' . $template,
                        $templateData
                    )
                );
        } catch (LoaderError $e) {
            return false;
        } catch (RuntimeError $e) {
            return false;
        } catch (SyntaxError $e) {
            return false;
        }

        try {
            $email->getHeaders()->addTextHeader('X-Transport', $transport);
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            return false;
        }
        return true;
    }

}
