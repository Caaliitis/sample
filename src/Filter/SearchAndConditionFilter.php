<?php

/*
 * This file is part of the API Platform project.
 *
 * (c) Kévin Dunglas <dunglas@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare( strict_types=1 );

namespace App\Filter;

use ApiPlatform\Core\Api\IdentifiersExtractorInterface;
use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterTrait;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryBuilderHelper;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Types\Type as DBALType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;

/**
 * Filter the collection by given properties.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class SearchAndConditionFilter extends AbstractContextAwareFilter implements SearchFilterInterface
{
	use SearchFilterTrait;

	public const DOCTRINE_INTEGER_TYPE = DBALType::INTEGER;

	private $em;

	public function __construct(
		ManagerRegistry $managerRegistry,
		?RequestStack $requestStack,
		IriConverterInterface $iriConverter,
		PropertyAccessorInterface $propertyAccessor = null,
		LoggerInterface $logger = null,
		array $properties = null,
		IdentifiersExtractorInterface $identifiersExtractor = null,
		NameConverterInterface $nameConverter = null,
		EntityManagerInterface $em
	) {
		parent::__construct( $managerRegistry, $requestStack, $logger, $properties, $nameConverter );

		if ( null === $identifiersExtractor ) {
			@trigger_error( 'Not injecting ItemIdentifiersExtractor is deprecated since API Platform 2.5 and can lead to unexpected behaviors, it will not be possible anymore in API Platform 3.0.', E_USER_DEPRECATED );
		}

		$this->iriConverter         = $iriConverter;
		$this->identifiersExtractor = $identifiersExtractor;
		$this->propertyAccessor     = $propertyAccessor ?: PropertyAccess::createPropertyAccessor();
		$this->em                   = $em;
	}

	protected function getIriConverter(): IriConverterInterface
	{
		return $this->iriConverter;
	}

	protected function getPropertyAccessor(): PropertyAccessorInterface
	{
		return $this->propertyAccessor;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function filterProperty( string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null )
	{
		if (
			null === $value ||
			! $this->isPropertyEnabled( $property, $resourceClass ) ||
			! $this->isPropertyMapped( $property, $resourceClass, true )
		) {
			return;
		}

		$alias = $queryBuilder->getRootAliases()[0];

		$field = $property;

		$associations = [];
		if ( $this->isPropertyNested( $property, $resourceClass ) ) {
			[
				$alias,
				$field,
				$associations
			] = $this->addJoinsForNestedProperty( $property, $alias, $queryBuilder, $queryNameGenerator, $resourceClass );
		}

		$values = $this->normalizeValues( (array) $value, $property );
		if ( null === $values ) {
			return;
		}

		$caseSensitive = true;
		$metadata      = $this->getNestedMetadata( $resourceClass, $associations );


		// metadata doesn't have the field, nor an association on the field
		if ( ! $metadata->hasAssociation( $field ) ) {
			return;
		}

		$values                     = array_map( [ $this, 'getIdFromValue' ], $values );
		$associationFieldIdentifier = 'id';
		$doctrineTypeField          = $this->getDoctrineFieldType( $property, $resourceClass );

		if ( null !== $this->identifiersExtractor ) {
			$associationResourceClass   = $metadata->getAssociationTargetClass( $field );
			$associationFieldIdentifier = $this->identifiersExtractor->getIdentifiersFromResourceClass( $associationResourceClass )[0];
			$doctrineTypeField          = $this->getDoctrineFieldType( $associationFieldIdentifier, $associationResourceClass );
		}

		if ( ! $this->hasValidValues( $values, $doctrineTypeField ) ) {
			$this->logger->notice( 'Invalid filter ignored', [
				'exception' => new InvalidArgumentException( sprintf( 'Values for field "%s" are not valid according to the doctrine type.', $field ) ),
			] );

			return;
		}

		$association    = $field;
		$valueParameter = $queryNameGenerator->generateParameterName( $association );

		if ( $metadata->isCollectionValuedAssociation( $association ) ) {
			$associationAlias = QueryBuilderHelper::addJoinOnce( $queryBuilder, $queryNameGenerator, $alias, $association );
			$associationField = $associationFieldIdentifier;
		} else {
			$associationAlias = $alias;
			$associationField = $field;
		}

		if ( 1 === \count( $values ) ) {
			$queryBuilder
				->andWhere( sprintf( '%s.%s = :%s', $associationAlias, $associationField, $valueParameter ) )
				->setParameter( $valueParameter, $values[0] );
		} else {

			$doubleAlias            = $queryNameGenerator->generateJoinAlias( $alias );
			$doubleAssociationAlias = $queryNameGenerator->generateJoinAlias( $associationAlias );
			$newQueryBuilder        = $this->em->createQueryBuilder()
			                                   ->select( $doubleAlias . '.id' )
			                                   ->from( $resourceClass, $doubleAlias )
			                                   ->leftJoin( $doubleAlias . '.' . $field, $doubleAssociationAlias )
			                                   ->groupBy( $doubleAlias . '.id' )
			                                   ->having( sprintf( 'COUNT(%s.id) = %s', $doubleAlias, \count( $values ) ) );


            foreach ( $values as $key => $value ) {
                $newQueryBuilder
                    ->orWhere( sprintf( '%s.%s = :%s', $doubleAssociationAlias, $associationField, $valueParameter . '_' . $key ) );
            }

			$expr = $this->em->getExpressionBuilder();

			$queryBuilder->andWhere(
				$expr->in(
					$alias . '.id',
					$newQueryBuilder->getDQL()
				)
			);


            foreach ( $values as $key => $value ) {
                $queryBuilder = $queryBuilder
                    ->setParameter( $valueParameter . '_' . $key, $value );
            }



			//Doesn\'t work, main alias isn\'t replaces
//			$queryBuilder->addGroupBy($alias.'.id');
//			$queryBuilder->having($alias.'.id = '. \count($values));


		}
	}

	/**
	 * Adds where clause according to the strategy.
	 *
	 * @throws InvalidArgumentException If strategy does not exist
	 */
	protected function addWhereByStrategy( string $strategy, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, $value, bool $caseSensitive )
	{
		$wrapCase       = $this->createWrapCase( $caseSensitive );
		$valueParameter = $queryNameGenerator->generateParameterName( $field );

		switch ( $strategy ) {
			case null:
			case self::STRATEGY_EXACT:
				$queryBuilder
					->andWhere( sprintf( $wrapCase( '%s.%s' ) . ' = ' . $wrapCase( ':%s' ), $alias, $field, $valueParameter ) )
					->setParameter( $valueParameter, $value );
				break;
			case self::STRATEGY_PARTIAL:
				$queryBuilder
					->andWhere( sprintf( $wrapCase( '%s.%s' ) . ' LIKE ' . $wrapCase( 'CONCAT(\'%%\', :%s, \'%%\')' ), $alias, $field, $valueParameter ) )
					->setParameter( $valueParameter, $value );
				break;
			case self::STRATEGY_START:
				$queryBuilder
					->andWhere( sprintf( $wrapCase( '%s.%s' ) . ' LIKE ' . $wrapCase( 'CONCAT(:%s, \'%%\')' ), $alias, $field, $valueParameter ) )
					->setParameter( $valueParameter, $value );
				break;
			case self::STRATEGY_END:
				$queryBuilder
					->andWhere( sprintf( $wrapCase( '%s.%s' ) . ' LIKE ' . $wrapCase( 'CONCAT(\'%%\', :%s)' ), $alias, $field, $valueParameter ) )
					->setParameter( $valueParameter, $value );
				break;
			case self::STRATEGY_WORD_START:
				$queryBuilder
					->andWhere( sprintf( $wrapCase( '%1$s.%2$s' ) . ' LIKE ' . $wrapCase( 'CONCAT(:%3$s, \'%%\')' ) . ' OR ' . $wrapCase( '%1$s.%2$s' ) . ' LIKE ' . $wrapCase( 'CONCAT(\'%% \', :%3$s, \'%%\')' ), $alias, $field, $valueParameter ) )
					->setParameter( $valueParameter, $value );
				break;
			default:
				throw new InvalidArgumentException( sprintf( 'strategy %s does not exist.', $strategy ) );
		}
	}

	/**
	 * Creates a function that will wrap a Doctrine expression according to the
	 * specified case sensitivity.
	 *
	 * For example, "o.name" will get wrapped into "LOWER(o.name)" when $caseSensitive
	 * is false.
	 */
	protected function createWrapCase( bool $caseSensitive ): \Closure
	{
		return static function ( string $expr ) use ( $caseSensitive ): string {
			if ( $caseSensitive ) {
				return $expr;
			}

			return sprintf( 'LOWER(%s)', $expr );
		};
	}

	/**
	 * {@inheritdoc}
	 */
	protected function getType( string $doctrineType ): string
	{
		switch ( $doctrineType ) {
			case DBALType::TARRAY:
				return 'array';
			case DBALType::BIGINT:
			case DBALType::INTEGER:
			case DBALType::SMALLINT:
				return 'int';
			case DBALType::BOOLEAN:
				return 'bool';
			case DBALType::DATE:
			case DBALType::TIME:
			case DBALType::DATETIME:
			case DBALType::DATETIMETZ:
				return \DateTimeInterface::class;
			case DBALType::FLOAT:
				return 'float';
		}

		if ( \defined( DBALType::class . '::DATE_IMMUTABLE' ) ) {
			switch ( $doctrineType ) {
				case DBALType::DATE_IMMUTABLE:
				case DBALType::TIME_IMMUTABLE:
				case DBALType::DATETIME_IMMUTABLE:
				case DBALType::DATETIMETZ_IMMUTABLE:
					return \DateTimeInterface::class;
			}
		}

		return 'string';
	}
}
