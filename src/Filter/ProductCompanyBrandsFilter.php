<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Brands;
use App\Entity\Companies;
use Doctrine\ORM\QueryBuilder;

final class ProductCompanyBrandsFilter extends AbstractContextAwareFilter
{


	public function getDescription( string $resourceClass ): array
	{
		if ( ! $this->properties ) {
			return [];
		}

		$description = [];

		foreach ( $this->properties as $property => $strategy ) {
			$description["companyWithBrands_$property"] = [
				'property' => $property,
				'type'     => 'string',
				'required' => false,
				'swagger'  => [
					'description' => 'Filters products using company and embedded brands to company. ONLY FOR PRODUCT PAGE FILTER',
					'name'        => 'BrandsFilter',
					'type'        => 'string',
				],
			];
		}

		return $description;
	}

	protected function filterProperty( string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null )
	{

		if ( ! is_numeric( $value ) ) {
			return;
		}

		$company = $this->managerRegistry->getRepository( Companies::class )->find( (int) $value );

		if ( null === $company ) {
			return;
		}

		if ( $property == 'companyWithBrands_id' ) {
			$property = 'id';
		}


		// otherwise filter is applied to order and page as well
		if (
			! $this->isPropertyEnabled( $property, $resourceClass ) ||
			! $this->isPropertyMapped( $property, $resourceClass )
		) {
			return;
		}

		$companyProperty = 'company';

		$companyParameterName = $queryNameGenerator->generateParameterName( $companyProperty ); // Generate a unique parameter name to avoid collisions with other filters

		$alias = $queryBuilder->getRootAliases()[0];

		$paramsArray = [];
		$sqlQuery    = '';

		$brandProperty = 'brand';
		/** @var Brands $brand */
		foreach ( $company->getBrands() as $brand ) {
			$brandParameterName                 = $queryNameGenerator->generateParameterName( $brandProperty ); // Generate a unique parameter name to avoid collisions with other filters
			$sqlQuery                           .= sprintf( '%s.%s = :%s OR ', $alias, $brandProperty, $brandParameterName );
			$paramsArray[ $brandParameterName ] = $brand;
		}

		$sqlQuery                             .= sprintf( '%s.%s = :%s', $alias, $companyProperty, $companyParameterName );
		$paramsArray[ $companyParameterName ] = $company;
		$queryBuilder
			->andWhere( "($sqlQuery)" )
			->setParameters( $paramsArray );

	}
}