<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Common\Filter\SearchFilterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class ProductFeaturesFilter extends AbstractContextAwareFilter implements SearchFilterInterface
{
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'description' => 'Filters product feature values from JSON string! ONLY FOR PRODUCT PAGE FILTER',
                    'name' => 'ProductFeatureFilter',
                    'type' => 'string',
                ],
            ];
        }

        return $description;
    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)

    protected function filterProperty(string $property, $param, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {

        if (
            !$this->isPropertyEnabled($property) ||
//            !$this->isPropertyMapped($property, $resourceClass, true) ||
            is_null(json_decode($param, true)) || empty($param)
        ) {
            return;
        }

        $param = json_decode(urldecode($param), true);

        $parameterName = $queryNameGenerator->generateParameterName($property); // Generate a unique parameter name to avoid collisions with other filters

        $queryBuilder->innerJoin("o.productFeatureValue", $parameterName);
        foreach ($param as $item) {
            foreach ($item as $feature => $value) {
                if (is_array($value)) { //multiple list
                    $value = implode("','", $value);
                    $value = "$parameterName.value in ('$value')";
                } else if (strpos($value, '...') !== false) {
                    $borders = explode("...", $value);

                    if(empty($borders[0]) && empty($borders[1])){
                        continue;
                    }

                    $value = "$parameterName.value >= '" . $borders[0] . "' and $parameterName.value <= '" . $borders[1] . "'";

                    if(empty($borders[0])){
                        $value = "$parameterName.value <= '" . $borders[1] . "'";
                    }

                    if(empty($borders[1])){
                        $value = "$parameterName.value >= '" . $borders[0]. "'";
                    }

                } else {
                    $value = "$parameterName.value = '$value'";
                }

                $queryBuilder->andWhere("($parameterName.feature = '$feature' and $value)");

            }
        }

    }
}