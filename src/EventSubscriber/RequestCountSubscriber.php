<?php

namespace App\EventSubscriber;

use App\Service\RecaptchaValidator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

class RequestCountSubscriber implements EventSubscriberInterface
{
    /** @var RecaptchaValidator */
    private $recaptchaValidator;

    /**
     * RequestCountSubscriber constructor.
     * @param RecaptchaValidator $recaptchaValidator
     */
    public function __construct(
        RecaptchaValidator $recaptchaValidator
    )
    {
        $this->recaptchaValidator = $recaptchaValidator;
    }


    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.terminate' => 'onKernelTerminate',
        ];
    }


    public function onKernelTerminate(TerminateEvent $event)
    {
        if (!in_array($event->getRequest()->getRequestUri(), ['/api/users/register_shop', '/api/users/register'])) {
            $this->recaptchaValidator->notifyIfCountLimitReached();
        }
    }
}