<?php

namespace App\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class UserEntitySubscriber implements EventSubscriber
{

	private $tokenStorage;

	public function __construct(TokenStorageInterface $tokenStorage)
	{

		$this->tokenStorage = $tokenStorage;
	}

	public function getSubscribedEvents()
	{
		return [
			Events::preUpdate,
			Events::prePersist
		];
	}

	public function preUpdate(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();
		$token = $this->tokenStorage->getToken();

		if (method_exists($entity, 'setUpdated'))
		{

			if (null !== $token)
			{
				$entity->setUpdated($token->getUser());
			}
		}
	}

	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();
		$token  = $this->tokenStorage->getToken();

		if (method_exists($entity, 'setCreated'))
		{
			if (null !== $token)
			{
				$entity->setCreated($token->getUser());
			}
		}

		$this->preUpdate($args);

	}
}