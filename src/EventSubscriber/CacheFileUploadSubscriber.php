<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Liip\ImagineBundle\Service\FilterService;
use Vich\UploaderBundle\Event\Event;


class CacheFileUploadSubscriber implements EventSubscriberInterface
{

    private $filterService;

    public function __construct(FilterService $filterService)
    {
        $this->filterService = $filterService;
    }


    public function onVichUploaderPostUpload(Event $event)
    {

        $image = $event->getObject();

        if (property_exists($image, 'source')) {
            $imageName = $image->getSource();
            $directoryNamer = $event->getMapping()->getDirectoryNamer();

            $path = $directoryNamer->directoryName($image, $event->getMapping());

            $this->filterService->getUrlOfFilteredImage($path . $imageName, 'thumbnail_small');
            $this->filterService->getUrlOfFilteredImage($path . $imageName, 'thumbnail_square');
        }

    }

    public static function getSubscribedEvents()
    {
        return [
            'vich_uploader.post_upload' => 'onVichUploaderPostUpload',
        ];
    }
}
