<?php


namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Comment;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class CommentPostSubscriber implements EventSubscriberInterface
{

	public static function getSubscribedEvents(): array
    {
		return [
			KernelEvents::VIEW => ['checkPost', EventPriorities::PRE_WRITE],
		];
	}

	public function checkPost(ViewEvent $event): void
	{
		$comment = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();

		if (!$comment instanceof Comment || Request::METHOD_POST !== $method) {
			return;
		}

		$count = 0;

		$count = $count + ($comment->getProduct()?1:0);
		$count = $count + ($comment->getBrand()?1:0);
		$count = $count + ($comment->getCompany()?1:0);
		$count = $count + ($comment->getComponent()?1:0);
		$count = $count + ($comment->getTrademark()?1:0);
		$count = $count + ($comment->getContact()?1:0);
		$count = $count + ($comment->getPatent()?1:0);


		if (0 === $count) {
			throw new BadRequestHttpException("Set at least one relation!");
		}

		if (1 < $count) {
			throw new BadRequestHttpException("Only one relation is allowed!");
		}

	}
}