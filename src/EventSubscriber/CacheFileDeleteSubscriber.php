<?php

namespace App\EventSubscriber;

use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Vich\UploaderBundle\Event\Event;


class CacheFileDeleteSubscriber implements EventSubscriberInterface
{

    private $cacheManager;

    public function __construct(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }


    public function onVichUploaderDeleteUpload(Event $event)
    {

        $image = $event->getObject();

        if (property_exists($image, 'source')) {
            $imageName = $image->getSource();
            $directoryNamer = $event->getMapping()->getDirectoryNamer();

            $path = $directoryNamer->directoryName($image, $event->getMapping());

            $this->cacheManager->remove($path . $imageName, 'thumbnail_small');
        }

    }

    public static function getSubscribedEvents()
    {
        return [
            'vich_uploader.pre_remove' => 'onVichUploaderDeleteUpload',
        ];
    }
}
