<?php

namespace App\EventSubscriber;

use App\Entity\Users;
use App\Mailer\MailGunMailer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class UserRegisterSubscriber implements EventSubscriber
{

    private $mailer;

    private $parameterBag;

    private $managerEmail;

    private $sendEmail;

    public function __construct(MailGunMailer $mailer, ParameterBagInterface $parameterBag)
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->managerEmail = $this->parameterBag->get('manager_email');
        $this->sendEmail = filter_var($this->parameterBag->get('send_user_registration_email'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist
        ];
    }


    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Users && $this->sendEmail === true) {
            $this->mailer->sendEmail(
                $this->managerEmail,
                'A new user has registered',
                'new-user.html.twig',
                [
                    'user' => $entity
                ]
            );
        }
    }
}