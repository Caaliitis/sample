<?php

namespace App\EventSubscriber;

use App\Connector\GatherConnector;
use App\Entity\Products;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;


class ProductPostSubscriber implements EventSubscriber
{

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws GuzzleException
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Products) {
            return;
        }

        $data = [
            "id" => $entity->getId(),
            "name" => $entity->getName(),
            "draft" => $entity->getDraft(),
        ];

        $connector = new GatherConnector((new Client()));
        $connector->postProductUpdate($data);
    }
}