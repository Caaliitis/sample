<?php

namespace App\EventSubscriber;

use App\Entity\ProofRequest;
use App\Mailer\MailGunMailer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class RequestProofsSubscriber implements EventSubscriber
{

    private $mailer;

    private $parameterBag;

    private $managerEmail;

    public function __construct(MailGunMailer $mailer, ParameterBagInterface $parameterBag)
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->managerEmail = $this->parameterBag->get('manager_email');
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist
        ];
    }


    public function postPersist(LifecycleEventArgs $args)
    {
        /** @var ProofRequest $entity */
        $entity = $args->getEntity();

        if (!$entity instanceof ProofRequest) {
            return;
        }

        $proofItem = '';

        if ($entity->getProduct() !== null) {
            $proofItem = sprintf(' Product "%s"', $entity->getProduct()->getName());
        }

        if ($entity->getPatent() !== null) {
            $proofItem = sprintf(' Patent "%s-%s-%s" (ID=%d)', $entity->getPatent()->getCountry(), $entity->getPatent()->getDocNumber(), $entity->getPatent()->getKind(), $entity->getPatent()->getId());
        }

        $this->mailer->sendEmail(
            $this->managerEmail,
            'Request Proof for ' . $proofItem,
            'request-proof.html.twig',
            [
                'proofItem' => $proofItem,
                'proof' => $entity
            ]
        );

    }
}