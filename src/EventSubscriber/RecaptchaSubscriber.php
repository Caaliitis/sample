<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Service\RecaptchaValidator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RecaptchaSubscriber implements EventSubscriberInterface
{
    /** @var RecaptchaValidator */
    private $recaptchaValidator;

    private $routesToProtect = [
        '/api/companies',
        '/api/contacts',
        '/api/products',
        '/api/components',
        '/api/trademarks',
        '/api/patents',
    ];

    private $routesToExclude = [
        '/count_graph',
        '/total_items',
        '/analysis-order',
        '/name',
        '/no_auth',
        '/order',
        '/report',
    ];

    public function __construct(
        RecaptchaValidator $recaptchaValidator
    )
    {
        $this->recaptchaValidator = $recaptchaValidator;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['checkRecaptcha', EventPriorities::PRE_READ],
        ];
    }

    /**
     * @param RequestEvent $event
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function checkRecaptcha(RequestEvent $event)
    {
        $request = $event->getRequest();

        /** Protect only GET routes of main entities: collections and items. Skip secondary routes */
        if ($request->getMethod() !== Request::METHOD_GET ||
            $this->isRouteToBeSkipped($request->getPathInfo())
        ) {
            return;
        }

        $this->recaptchaValidator->validateAndLog($request);
    }

    /**
     * @param string $path
     * @return bool
     */
    private function isRouteToBeSkipped(string $path)
    {
        $included = false;
        $excluded = false;

        foreach ($this->routesToProtect as $routePart) {
            if (strpos($path, $routePart) !== false) {
                $included = true;
                break;
            }
        }

        foreach ($this->routesToExclude as $routePart) {
            if (strpos($path, $routePart) !== false) {
                $excluded = true;
                break;
            }
        }

        return !($included && !$excluded);
    }
}