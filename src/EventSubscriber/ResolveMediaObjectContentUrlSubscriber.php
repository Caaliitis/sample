<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\File\ContentUri;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Vich\UploaderBundle\Storage\StorageInterface;

final class ResolveMediaObjectContentUrlSubscriber implements EventSubscriberInterface
{
    private $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {

        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request))
            || (!property_exists($attributes['resource_class'], 'contentUrl') && !\is_subclass_of($attributes['resource_class'], ContentUri::class, true))) {
            return;
        }

        if ($request->getMethod() === Request::METHOD_DELETE) {
            return;
        }

        $mediaObjects = $controllerResult;

        if (!is_iterable($mediaObjects)) {
            $mediaObjects = [$mediaObjects];
        }


        foreach ($mediaObjects as $mediaObject) {

            if (property_exists($attributes['resource_class'], 'contentUrl')) {
                $mediaObject->contentUrl = $this->storage->resolveUri($mediaObject, 'file');
            }

            if (\is_subclass_of($attributes['resource_class'], ContentUri::class, true)) {

                if ($mediaObject === null) {
                    continue;
                }

                foreach ($mediaObject->getContentUris() as $mediaObjectEmbedded) {
                    $mediaObjectEmbedded->contentUrl = $this->storage->resolveUri($mediaObjectEmbedded, 'file');
                }
            }

        }
    }
}