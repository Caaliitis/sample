<?php

namespace App\EventSubscriber;


use App\Service\RecaptchaValidator;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class JWTCreatedListener
{

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var LoggerInterface
     */
    private $logger;
    private $recaptchaValidator;

    /**
     * @param RequestStack $requestStack
     * @param LoggerInterface $logger
     * @param RecaptchaValidator $recaptchaValidator
     */
    public function __construct(
        RequestStack $requestStack,
        LoggerInterface $logger,
        RecaptchaValidator $recaptchaValidator
    )
    {
        $this->requestStack = $requestStack;
        $this->logger = $logger;
        $this->recaptchaValidator = $recaptchaValidator;
    }


    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        if ($request) {
            $this->recaptchaValidator->validateAndLog($request);
        }

        $payload = $event->getData();
        $payload['id'] = $event->getUser()->getId();
        $payload['host'] = $request->headers->get('origin');
        $this->logger->alert('A host was added to payload: '.$request->headers->get('origin'));

        $event->setData($payload);

        $header = $event->getHeader();
        $header['cty'] = 'JWT';

        $event->setHeader($header);
    }
}