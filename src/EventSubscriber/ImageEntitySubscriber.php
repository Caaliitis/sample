<?php

namespace App\EventSubscriber;


use App\Entity\CompanyImages;
use App\Entity\ProductImages;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class ImageEntitySubscriber implements EventSubscriber {


	private $containerBag;

	public function __construct( ContainerBagInterface $containerBag ) {
		$this->containerBag = $containerBag;
	}

	public function getSubscribedEvents() {
		return [
			Events::postLoad,
		];
	}

	public function postLoad( LifecycleEventArgs $args ) {

		$entity = $args->getEntity();

		if ( $entity instanceof ProductImages ) {
			$entity->setAbsoluteUrl( $this->containerBag->get( 'google_public_base_url' ) );
			$entity->setCachePath( $this->containerBag->get( 'google_public_cache_folder_small' ) );
		}

		if ( $entity instanceof CompanyImages ) {
			$entity->setAbsoluteUrl( $this->containerBag->get( 'google_public_base_url' ) );
			$entity->setCachePath( $this->containerBag->get( 'google_public_cache_folder_square' ) );
		}

	}


}