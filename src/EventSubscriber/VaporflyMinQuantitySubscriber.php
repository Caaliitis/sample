<?php

namespace App\EventSubscriber;

use App\Entity\ProductFeatureValues;
use App\Entity\Vaporfly\VaporflyOrder;
use App\Entity\Vaporfly\VaporflyOrderItems;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class VaporflyMinQuantitySubscriber implements EventSubscriber
{

    public function getSubscribedEvents(): array
    {
        return ['prePersist'];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $order = $args->getEntity();

        if (!$order instanceof VaporflyOrder) {
            return;
        }

        $moq = 100;

        /** @var VaporflyOrderItems $item */
        foreach ($order->getItems() as $item) {
            foreach ($item->getProduct()->getProductFeatureValue() as $featureValue) {
                /** @var ProductFeatureValues $featureValue */
                if ($featureValue->getFeature()->getId() === 35) {
                    $moq = $featureValue->getValue() ? $featureValue->getValue() : 100;
                }
            }

            if ($moq > $item->getQuantity()) {
                throw new BadRequestHttpException(sprintf('The requested quantity less than min set value %d', $moq));
            }
        }


    }

}
