<?php


namespace App\Serializer;

use App\Entity\Products;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;


final class TypesAttributeNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'TYPES_ATTRIBUTE_NORMALIZER_ALREADY_CALLED';

    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function normalize($object, string $format = null, array $context = [])
    {

        $context[self::ALREADY_CALLED] = true;

        if ($this->userHasPermissionsToType() === true) {
            return $this->normalizer->normalize($object, $format, $context);
        }

        if (method_exists($object, 'getTypes') && method_exists($object, 'removeType')) {

            foreach ($object->getTypes() as $type) {
                if ($type->getOnlyForStaff() === true) {
                    $object->removeType($type);
                }
            }

        }

        return $this->normalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Products;
    }

    private function userHasPermissionsToType(): bool
    {
        $rolesArr = $this->tokenStorage->getToken()->getRoleNames();

        if (in_array('ROLE_STAFF', $rolesArr)) {
            return true;
        }

        return false;
    }

}