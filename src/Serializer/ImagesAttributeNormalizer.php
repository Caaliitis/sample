<?php


namespace App\Serializer;

use App\Entity\Products;
use App\Repository\TypesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;


final class ImagesAttributeNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'IMAGES_ATTRIBUTE_NORMALIZER_ALREADY_CALLED';

    private $tokenStorage;

    private $repository;

    private $vaporflyTypeId;

    public function __construct(TokenStorageInterface $tokenStorage, TypesRepository $repository, int $vaporflyTypeId)
    {
        $this->tokenStorage = $tokenStorage;
        $this->repository = $repository;
        $this->vaporflyTypeId = $vaporflyTypeId;
    }

    public function normalize($object, string $format = null, array $context = [])
    {

        $context[self::ALREADY_CALLED] = true;

        if (!$this->userHasPermissionsToType() || !$object instanceof Products) {
            return $this->normalizer->normalize($object, $format, $context);
        }

        $tradingType = $this->repository->find($this->vaporflyTypeId);

        //DefaultImage

        foreach ($object->getMedia() as $key => $media) {

            if ($media->getTypes()->contains($tradingType)) {

                $object->setDefaultImage($media);
                break;
            }
        }

        //Other Images
        $newMediaCollection = new ArrayCollection();

        //In case when at least one image contains trading type
        foreach ($object->getMedia() as $key => $media) {
            if ($media->getTypes()->contains($tradingType)) {
                $newMediaCollection->add($media);
            }
        }

        //Add to collection only default image because it's empty
        if (null !== $object->getDefaultImage() && $newMediaCollection->count() === 0) {
            $newMediaCollection->add($object->getDefaultImage());
        }

        $object->setMediaForNormalizing($newMediaCollection);

        return $this->normalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Products;
    }

    private function userHasPermissionsToType(): bool
    {
        $rolesArr = $this->tokenStorage->getToken()->getRoleNames();

        if (in_array('ROLE_SHOP', $rolesArr)) {
            return true;
        }

        return false;
    }

}