<?php


namespace App\DoctrineExtensions\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;

final class Citext extends TextType
{
    const CITEXT = 'citext';

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::CITEXT;
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getDoctrineTypeMapping(self::CITEXT);
    }
}